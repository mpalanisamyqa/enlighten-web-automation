package com.enphaseenergy.enlighten.Helpers;

import com.enphaseenergy.enlighten.utils.Driver.DriverManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class JavascriptHelper {
    private WebDriver driver = DriverManager.getDriver();
    private Logger logger = LoggerHelper.getLogger(JavascriptHelper.class);
    ProgressHelpers progressHelpers = new ProgressHelpers();

    public Object executeScript(String script) {
        JavascriptExecutor exe = (JavascriptExecutor) driver;
        logger.info(script);
        return exe.executeScript(script);
    }

    public Object executeScript(String script, Object... args) {
        JavascriptExecutor exe = (JavascriptExecutor) driver;
        logger.info(script);
        return exe.executeScript(script, args);
    }

    public void scrollToElemet(WebElement element) {
        executeScript("window.scrollTo(arguments[0],arguments[1])",
                element.getLocation().x, element.getLocation().y);
        logger.info(element);
    }

    public void scrollToElemet(By locator) {
        scrollToElemet(driver.findElement(locator));
        logger.info(locator);
    }

    public void scrollToElemetAndClick(By locator) {
        WebElement element = driver.findElement(locator);
        scrollToElemet(element);
        element.click();
        logger.info(locator);
    }

    public void scrollToElemetAndClick(WebElement element) {
        scrollToElemet(element);
        element.click();
        logger.info(element);
    }

    public void scrollIntoView(WebElement element) {
        executeScript("arguments[0].scrollIntoView()", element);
        logger.info(element);
    }

    public void scrollIntoView(By locator) {
        scrollIntoView(driver.findElement(locator));
        logger.info(locator);
    }

    public void scrollIntoViewAndClick(By locator) {
        WebElement element = driver.findElement(locator);
        scrollIntoView(element);
        element.click();
        logger.info(locator);
    }

    public void scrollIntoViewAndClick(WebElement element) {
        scrollIntoView(element);
        element.click();
        logger.info(element);
    }

    public void clickOnElement(WebElement element){
        ((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
    }

    public void enterText(WebElement element, String inputText){
        progressHelpers.hardWait(2000);
        executeScript("arguments[0].setAttribute('value','"+inputText+"')",element);
    }
}
