package com.enphaseenergy.enlighten.pages;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.time.LocalDateTime;
import java.util.ArrayList;

/**
 * @author mnpalanisamy
 */
public class ActivationsPage implements BaseObjects{
    public ActivationsPage(WebDriver driver){
        PageFactory.initElements(driver,this);
    }

    /*Objects of the Page*/

    @FindBy(id = "site_name")
    private WebElement systemNameTextField;

    @FindBy(id = "site_site_type")
    private WebElement systemTypeDropdown;

    @FindBy(id = "site_ref_id")
    private WebElement systemReferenceTextField;

    @FindBy(id = "site_owner_first_name")
    private WebElement systemFirstNameField;

    @FindBy(id = "site_owner_last_name")
    private WebElement systemLastNameField;

    @FindBy(id = "site_owner_email")
    private WebElement systemEmailField;

    @FindBy(id = "site_owner_phone")
    private WebElement systemPhoneField;

    @FindBy(id = "address_country")
    private WebElement systemCountryDropdown;

    @FindBy(id = "address_address1")
    private WebElement systemAddress1Field;

    @FindBy(id = "address_address2")
    private WebElement systemAddress2Field;

    @FindBy(id = "address_city")
    private WebElement systemCityField;

    @FindBy(id = "address_state")
    private WebElement systemStateDropdown;

    @FindBy(id = "address_zip")
    private WebElement systemZipCodeField;

    @FindBy(id = "new_emus_serial_number_1")
    private WebElement systemEnvoyNoField;

    @FindBy(css = "input.emu_serial")
    private WebElement activatedSystemEnvoyNoField;

    @FindBy(id = "site_expected_pcu_count")
    private WebElement systemTotalMicroInverterCountField;

    @FindBy(id = "site_privacy_policy_assent")
    private WebElement systemIConfirmCheckbox;

    @FindBy(id = "submitButton")
    private WebElement systemSaveCTAButton;

    @FindBy(css = "div.progress span.percentage")
    private WebElement systemCompletePercentageValue;

    @FindBy(id = "order")
    private WebElement newActivationForm;

    @FindBy(id = "existing_owner_confirm_association")
    private WebElement associateThisActivationWithThisUserCheckbox;

    @FindBy(css = "div.user_exists_error p")
    private WebElement userExistsErrorMessage;

    @FindBy(css = "div.flash_message div.text")
    private WebElement flashMessage;

    @FindBy(id = "close_btn")
    private WebElement flashMessageCloseButton;

    @FindBy(id = "delete_system")
    private WebElement deleteThisSystemButton;

    @FindBy(id = "confirm_delete")
    private WebElement confirmDeleteMessage;

    @FindBy(xpath = "//span[@class='ui-button-text' and text()='Cancel']")
    private WebElement popupCancelButton;

    @FindBy(xpath = "//span[@class='ui-button-text' and text()='Delete System']")
    private WebElement popupDeleteSystemButton;

    @FindBy(css = "div#existing_owner div:nth-child(1)")
    private WebElement activatedOwnerNameAndEmail;

    @FindBy(css = "div#existing_owner div:nth-child(2)")
    private WebElement activatedOwnerPhone;

    @FindBy(id = "enablEdit")
    private WebElement editDetailsButton;


    /*Constant Strings*/
    public static final String SUCCESSFULACTIVATIONMESSAGE= "Activation created successfully.";
    public static final String SUCCESSFULREMOVALMESSAGE= "Activation successfully removed.";
    public static final String SUCCESSFULUPDATEACTIVATIONMESSAGE= "Activation saved successfully.";


    /*Page Methods*/

    public void verifyNewActivationPage(){
        progressHelpers.waitForElementToDisplay(newActivationForm);
        Assert.assertTrue(newActivationForm.isDisplayed());
    }

    /**
     * @param sheetName
     * @param rowIndex
     */
    public void createNewSystem(String sheetName, String rowIndex){
        enterSystemName(testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"System Name"));
        enterInstallerReference(testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Installer Reference"));
        selectSiteType(testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"SystemType"));
        enterFirstName(testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"FirstName"));
        enterLastName(testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"LastName"));
        enterEmail(testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Email"));
        enterPhone(testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Phone"));
        selectCountry(testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Country"));
        enterAddress1(testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Street Address1"));
        enterAddress2(testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Street Address2"));
        enterCity(testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"City"));
        selectState(testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"State"));
        enterZipcode(testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Zipcode"));
        enterEnvoy(testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Envoy"));
        enterPcuCount(testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"PcuCount"));
        selectConfirmationCheckbox(true);
        clickOnSaveButton();
        userExistsErrorMessageHandler("new");

    }

    //create randomsystem name
    public void enterSystemName(){
        String automationSystemName = "ENLQA_Aut"+LocalDateTime.now();
        systemNameTextField.sendKeys(automationSystemName);
    }

    /**
     * @param systemName
     */
    public void enterSystemName(String systemName){
        systemNameTextField.clear();
        systemNameTextField.sendKeys(systemName);
    }

    /**
     * @param referenceName
     */
    public void enterInstallerReference(String referenceName){
        systemReferenceTextField.clear();
        systemReferenceTextField.sendKeys(referenceName);
    }

    /**
     * @param typeName
     */
    public void selectSiteType(String typeName){
        dropDownHelper.SelectUsingVisibleValue(systemTypeDropdown,typeName);
    }

    /**
     * @param firstName
     */
    public void enterFirstName(String firstName){
        systemFirstNameField.click();
        systemFirstNameField.clear();
        systemFirstNameField.sendKeys(firstName);
    }

    /**
     * @param lastName
     */
    public void enterLastName(String lastName){
        systemLastNameField.clear();
        systemLastNameField.sendKeys(lastName);
    }

    /**
     * @param email
     */
    public void enterEmail(String email){
        systemEmailField.clear();
        systemEmailField.sendKeys(email);
    }

    /**
     * @param phone
     */
    public void enterPhone(String phone){
        systemPhoneField.clear();
        systemPhoneField.sendKeys(phone);
    }

    /**
     * @param countryName
     */
    public void selectCountry(String countryName){
        dropDownHelper.SelectUsingVisibleValue(systemCountryDropdown,countryName);
    }

    /**
     * @param address1
     */
    public void enterAddress1(String address1){
        systemAddress1Field.clear();
        systemAddress1Field.sendKeys(address1);
    }

    /**
     * @param address2
     */
    public void enterAddress2(String address2){
        systemAddress2Field.clear();
        systemAddress2Field.sendKeys(address2);
    }

    /**
     * @param city
     */
    public void enterCity(String city){
        systemCityField.clear();
        systemCityField.sendKeys(city);
    }

    /**
     * @param stateName
     */
    public void selectState(String stateName){
        dropDownHelper.SelectUsingVisibleValue(systemStateDropdown,stateName);
    }

    /**
     * @param zipcode
     */
    public void enterZipcode(String zipcode){
        systemZipCodeField.clear();
        systemZipCodeField.sendKeys(zipcode);
    }

    /**
     * @param envoyNumber
     */
    public void enterEnvoy(String envoyNumber){
        systemEnvoyNoField.clear();
        systemEnvoyNoField.sendKeys(envoyNumber);
    }

    /**
     * @param envoyNumber
     */
    public void updateEnvoy(String envoyNumber){
        activatedSystemEnvoyNoField.clear();
        activatedSystemEnvoyNoField.sendKeys(envoyNumber);
    }

    /**
     * @param pcuCount
     */
    public void enterPcuCount(String pcuCount){
        systemTotalMicroInverterCountField.clear();
        systemTotalMicroInverterCountField.sendKeys(pcuCount);
    }

    /**
     * @param selectValue
     */
    public void selectConfirmationCheckbox(boolean selectValue){
        if(selectValue){
            //Check the existing state of checkbox and perform
            checkBoxAndRadioButtonHelper.selectCheckBox(systemIConfirmCheckbox);
        } else{
            checkBoxAndRadioButtonHelper.unSelectCheckBox(systemIConfirmCheckbox);
        }
    }

    public void clickOnSaveButton(){
        systemSaveCTAButton.click();
    }

    public void userExistsErrorMessageHandler(String forWhichActivation){
        try{
            if(forWhichActivation.equalsIgnoreCase("new")){
                if(userExistsErrorMessage.isDisplayed()){
                    associateThisActivationWithThisUserCheckbox.click();
                    selectConfirmationCheckbox(true);
                    clickOnSaveButton();
            }
        }else if (forWhichActivation.equalsIgnoreCase("update")){
                if(userExistsErrorMessage.isDisplayed()){
                    associateThisActivationWithThisUserCheckbox.click();
                    clickOnSaveButton();
                }
            }
        }catch (NoSuchElementException e){
            //Do nothing and Add log
        }
    }

    public void verifySuccessfullActivationMessage(){
        progressHelpers.waitForPageLoad();
        Assert.assertEquals(flashMessage.getText(),SUCCESSFULACTIVATIONMESSAGE);
    }

    public void deleteActivation(){
        deleteThisSystemButton.click();
        progressHelpers.waitForElementToDisplay(confirmDeleteMessage);
        popupDeleteSystemButton.click();
    }

    public void verifySuccessfullRemovalMessage(){
//        progressHelpers.waitForPageLoad();
        Assert.assertEquals(flashMessage.getText(),SUCCESSFULREMOVALMESSAGE);
    }

    /**
     * @param sheetName
     * @param rowIndex
     */
    public void verifyActivatedSystemDetails(String sheetName, String rowIndex){

        progressHelpers.waitForElementToDisplay(newActivationForm);
        Assert.assertEquals(systemNameTextField.getAttribute("value"),testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"System Name"));
        Assert.assertEquals(systemReferenceTextField.getAttribute("value"),testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Installer Reference"));
        Assert.assertEquals(dropDownHelper.getSelectedValue(systemTypeDropdown),testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"SystemType"));
        System.out.println("exp percent"+testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Completed Percentage"));
        System.out.println("percentage"+systemCompletePercentageValue.getText());
        Assert.assertEquals(systemCompletePercentageValue.getText(),testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Completed Percentage"));
        Assert.assertEquals(getActivatedOwnerFirstName(),testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"FirstName"));
        Assert.assertEquals(getActivatedOwnerLastName(),testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"LastName"));
        Assert.assertEquals(getActivatedOwnerEmail(),testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Email"));
        Assert.assertEquals(getActivatedOwnerPhone(),testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Phone"));
        Assert.assertEquals(dropDownHelper.getSelectedValue(systemCountryDropdown),testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Country"));
        Assert.assertEquals(systemAddress1Field.getAttribute("value"),testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Street Address1"));
        Assert.assertEquals(systemAddress2Field.getAttribute("value"),testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Street Address2"));
        Assert.assertEquals(systemCityField.getAttribute("value"),testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"City"));
        Assert.assertEquals(dropDownHelper.getSelectedValue(systemStateDropdown),testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"State"));
        Assert.assertEquals(systemZipCodeField.getAttribute("value"),testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Zipcode"));
        Assert.assertEquals(activatedSystemEnvoyNoField.getAttribute("value"),testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Envoy"));
        Assert.assertEquals(systemTotalMicroInverterCountField.getAttribute("value"),testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"PcuCount"));

    }

    public String getActivatedOwnerFirstName(){
        String ownerDetails = activatedOwnerNameAndEmail.getText();
        return ownerDetails.split("\\s")[0];
    }

    public String getActivatedOwnerLastName(){
        String ownerDetails = activatedOwnerNameAndEmail.getText();
        return ownerDetails.split("\\s")[1];
    }

    public String getActivatedOwnerEmail(){
        String ownerDetails = activatedOwnerNameAndEmail.getText();
        String email = ownerDetails.split("\\s")[2].substring(1,ownerDetails.split("\\s")[2].length()-1);
        return email;
    }

    public String getActivatedOwnerPhone(){
        String ownerDetails = activatedOwnerPhone.getText();
        return ownerDetails.trim();
    }

    public void clickOnEditDetailButton(){
        editDetailsButton.click();
    }

    /**
     * @param sheetName
     * @param rowIndex
     */
    public void updateActivatedSystem(String sheetName, String rowIndex){
        enterSystemName(testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"System Name"));
        enterInstallerReference(testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Installer Reference"));
        selectSiteType(testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"SystemType"));
        clickOnEditDetailButton();
        progressHelpers.waitForElementToDisplay(systemFirstNameField);
        enterFirstName(testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"FirstName"));
        enterLastName(testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"LastName"));
        enterEmail(testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Email"));
        enterPhone(testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Phone"));
        selectCountry(testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Country"));
        enterAddress1(testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Street Address1"));
        enterAddress2(testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Street Address2"));
        enterCity(testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"City"));
        selectState(testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"State"));
        enterZipcode(testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Zipcode"));
        updateEnvoy(testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Envoy"));
        enterPcuCount(testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"PcuCount"));
        clickOnSaveButton();
        userExistsErrorMessageHandler("update");
    }

    public void verifySuccessfullUpdateActivationMessage(){
//        progressHelpers.waitForPageLoad();
        Assert.assertEquals(flashMessage.getText(),SUCCESSFULUPDATEACTIVATIONMESSAGE);
    }


}
