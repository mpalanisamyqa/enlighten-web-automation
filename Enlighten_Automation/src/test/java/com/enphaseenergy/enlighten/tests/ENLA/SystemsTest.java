package com.enphaseenergy.enlighten.tests.ENLA;

import com.enphaseenergy.enlighten.tests.BaseTest;
import com.enphaseenergy.enlighten.utils.Report.ExtentTestManager;
import com.relevantcodes.extentreports.LogStatus;
import org.testng.annotations.Test;

import java.lang.reflect.Method;

/**
 * @author mnpalanisamy
 */
public class SystemsTest extends BaseTest {

    @Test(groups = {"sanity","prod_sanity"})
    public void ENLA1868_verifyAllFieldsCanBeSorted(Method method) {
        ExtentTestManager.startTest(method.getName(), "Verify All Fields can be sorted with Admin Role");
        ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId" + Thread.currentThread().getId());
        getSignInPageObject().verifySignInPage();
        getSignInPageObject().loginAs("EEADMIN_ADMIN_AUT_1");
        getDashboardPageObject().verifyDashboardPage();
        getDashboardPageObject().clickOnMenu("Admin");
        getAdminPageObject().verifyAdminPage();
        getAdminPageObject().navigateToLeftMenu("Systems");
        getAdminSystemPageObject().verifySystemTableSortFunctionality();

    }

    @Test(groups = {"sanity","prod_sanity"})
    public void ENLA1869_verifyAllTheFiltersBehaviourInSystemTable(Method method) {
        ExtentTestManager.startTest(method.getName(), "Verify All The Filters in Table with Admin Role");
        ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId" + Thread.currentThread().getId());
        getSignInPageObject().verifySignInPage();
        getSignInPageObject().loginAs("EEADMIN_ADMIN_AUT_1");
        getDashboardPageObject().verifyDashboardPage();
        getDashboardPageObject().clickOnMenu("Admin");
        getAdminPageObject().verifyAdminPage();
        getAdminPageObject().navigateToLeftMenu("Systems");
        getAdminSystemPageObject().verifyAllTheFiltersInTable("Systems","1");

    }

    @Test(groups = {"sanity","prod_sanity"})
    public void ENLA1870_verifyUserSeeAllSystems(Method method) {
        ExtentTestManager.startTest(method.getName(), "Verify All Systems are displayed with Admin Role");
        ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId" + Thread.currentThread().getId());
        getSignInPageObject().verifySignInPage();
        getSignInPageObject().loginAs("EEADMIN_ADMIN_AUT_1");
        getDashboardPageObject().verifyDashboardPage();
        getDashboardPageObject().clickOnMenu("Admin");
        getAdminPageObject().verifyAdminPage();
        getAdminPageObject().navigateToLeftMenu("Systems");
        getAdminSystemPageObject().verifyAllSystemsAreDisplayed();
    }

    @Test(groups = {"sanity","prod_sanity"})
    public void ENLA1899_verifyEventsOfASystemIsDisplayed(Method method) {
        ExtentTestManager.startTest(method.getName(), "Verify Events of a System with Admin Role");
        ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId" + Thread.currentThread().getId());
        getSignInPageObject().verifySignInPage();
        getSignInPageObject().loginAs("EEADMIN_ADMIN_AUT_1");
        getDashboardPageObject().verifyDashboardPage();
        getDashboardPageObject().clickOnMenu("Admin");
        getAdminPageObject().verifyAdminPage();
        getAdminPageObject().navigateToLeftMenu("Systems");
        getAdminSystemPageObject().searchSystem("Systems","1");
        getAdminSystemPageObject().clickOnSystemMenuItem("Events");
        getAdminEventPageObject().verifyEventsAreDispalyed();
    }

    @Test(groups = {"sanity","prod_sanity"})
    public void ENLA1900_verifySystemEnvoyLogs(Method method) {
        ExtentTestManager.startTest(method.getName(), "Verify System Envoy Logs with Admin Role");
        ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId" + Thread.currentThread().getId());
        getSignInPageObject().verifySignInPage();
        getSignInPageObject().loginAs("EEADMIN_ADMIN_AUT_1");
        getDashboardPageObject().verifyDashboardPage();
        getDashboardPageObject().clickOnMenu("Admin");
        getAdminPageObject().verifyAdminPage();
        getAdminPageObject().navigateToLeftMenu("Systems");
        getAdminSystemPageObject().searchSystem("Systems","1");
        getAdminSystemPageObject().clickOnSystemMenuItem("Devices");
        getAdminDevicesPageObject().clickOnEnvoyLink("Log");
        getAdminEnvoyPageObject().verifyEnvoyLogTableData();

    }

    @Test(groups = {"sanity","prod_sanity"})
    public void ENLA1901_verifySystemEnvoyTasks(Method method) {
        ExtentTestManager.startTest(method.getName(), "Verify System Envoy Tasks with Admin Role");
        ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId" + Thread.currentThread().getId());
        getSignInPageObject().verifySignInPage();
        getSignInPageObject().loginAs("EEADMIN_ADMIN_AUT_1");
        getDashboardPageObject().verifyDashboardPage();
        getDashboardPageObject().clickOnMenu("Admin");
        getAdminPageObject().verifyAdminPage();
        getAdminPageObject().navigateToLeftMenu("Systems");
        getAdminSystemPageObject().searchSystem("Systems","1");
        getAdminSystemPageObject().clickOnSystemMenuItem("Devices");
        getAdminDevicesPageObject().clickOnEnvoyLink("Tasks");
        getAdminEnvoyPageObject().verifyTasksTableData();

    }

    @Test(groups = {"sanity","prod_sanity"})
    public void ENLA1902_verifySystemEnvoyReports(Method method) {
        ExtentTestManager.startTest(method.getName(), "Verify Enlighten System Envoy Performance Report with Admin Role");
        ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId" + Thread.currentThread().getId());
        getSignInPageObject().verifySignInPage();
        getSignInPageObject().loginAs("EEADMIN_ADMIN_AUT_1");
        getDashboardPageObject().verifyDashboardPage();
        getDashboardPageObject().clickOnMenu("Admin");
        getAdminPageObject().verifyAdminPage();
        getAdminPageObject().navigateToLeftMenu("Systems");
        getAdminSystemPageObject().searchSystem("Systems","1");
        getAdminSystemPageObject().clickOnSystemMenuItem("Devices");
        getAdminDevicesPageObject().clickOnEnvoyLink("Reports");
        getAdminEnvoyPageObject().verifyLatestEnvoyReport();

    }

    @Test(groups = {"sanity","prod_sanity"})
    public void ENLA1903_verifyEnlightenManagerView(Method method) {
        ExtentTestManager.startTest(method.getName(), "Verify Enlighten Manager View with Admin Role");
        ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId" + Thread.currentThread().getId());
        getSignInPageObject().verifySignInPage();
        getSignInPageObject().loginAs("EEADMIN_ADMIN_AUT_1");
        getDashboardPageObject().verifyDashboardPage();
        getDashboardPageObject().clickOnMenu("Admin");
        getAdminPageObject().verifyAdminPage();
        getAdminPageObject().navigateToLeftMenu("Systems");
        getAdminSystemPageObject().searchSystem("Systems","1");
        getAdminSystemPageObject().clickOnSystemMenuItem("Enlighten Manager");
        getAdminSystemPageObject().verifyEnlightenManagerViewOfSystem("Systems","1");
    }

    @Test(groups = {"sanity","prod_sanity"})
    public void ENLA1904_verifyMyEnlightenView(Method method) {
        ExtentTestManager.startTest(method.getName(), "Verify MyEnlighten View with Admin Role");
        ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId" + Thread.currentThread().getId());
        getSignInPageObject().verifySignInPage();
        getSignInPageObject().loginAs("EEADMIN_ADMIN_AUT_1");
        getDashboardPageObject().verifyDashboardPage();
        getDashboardPageObject().clickOnMenu("Admin");
        getAdminPageObject().verifyAdminPage();
        getAdminPageObject().navigateToLeftMenu("Systems");
        getAdminSystemPageObject().searchSystem("Systems","1");
        getAdminSystemPageObject().clickOnSystemMenuItem("MyEnlighten");
        getAdminSystemPageObject().verifyMyEnlightenViewOfSystem("Systems","1");
    }

    @Test(groups = {"sanity","prod_sanity"})
    public void ENLA1905_verifyEnlightenMobileView(Method method) {
        ExtentTestManager.startTest(method.getName(), "Verify Enlighten Mobile View with Admin Role");
        ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId" + Thread.currentThread().getId());
        getSignInPageObject().verifySignInPage();
        getSignInPageObject().loginAs("EEADMIN_ADMIN_AUT_1");
        getDashboardPageObject().verifyDashboardPage();
        getDashboardPageObject().clickOnMenu("Admin");
        getAdminPageObject().verifyAdminPage();
        getAdminPageObject().navigateToLeftMenu("Systems");
        getAdminSystemPageObject().searchSystem("Systems","1");
        getAdminSystemPageObject().clickOnSystemMenuItem("Enlighten Mobile");
        getAdminSystemPageObject().verifyEnlightenMobileViewOfSystem();
    }

    @Test(groups = {"sanity","prod_sanity"})
    public void ENLA1906_verifySystemDevicesPage(Method method) {
        ExtentTestManager.startTest(method.getName(), "Verify Enlighten System Devices page with Admin Role");
        ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId" + Thread.currentThread().getId());
        getSignInPageObject().verifySignInPage();
        getSignInPageObject().loginAs("EEADMIN_ADMIN_AUT_1");
        getDashboardPageObject().verifyDashboardPage();
        getDashboardPageObject().clickOnMenu("Admin");
        getAdminPageObject().verifyAdminPage();
        getAdminPageObject().navigateToLeftMenu("Systems");
        getAdminSystemPageObject().searchSystem("Systems","1");
        getAdminSystemPageObject().clickOnSystemMenuItem("Devices");
        getAdminDevicesPageObject().verifyAdminDevicesPage();
    }

    @Test(groups = {"sanity","prod_sanity"})
    public void ENLA1907_verifyEventsPageOfASystemIsDisplayed(Method method) {
        ExtentTestManager.startTest(method.getName(), "Verify Events page with Admin Role");
        ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId" + Thread.currentThread().getId());
        getSignInPageObject().verifySignInPage();
        getSignInPageObject().loginAs("EEADMIN_ADMIN_AUT_1");
        getDashboardPageObject().verifyDashboardPage();
        getDashboardPageObject().clickOnMenu("Admin");
        getAdminPageObject().verifyAdminPage();
        getAdminPageObject().navigateToLeftMenu("Systems");
        getAdminSystemPageObject().searchSystem("Systems","1");
        getAdminSystemPageObject().clickOnSystemMenuItem("Events");
        getAdminEventPageObject().verifyEventsPage();
    }

    @Test(groups = {"sanity","prod_sanity"})
    public void ENLA1908_verifySystemAccesPage(Method method) {
        ExtentTestManager.startTest(method.getName(), "Verify System Access page with Admin Role");
        ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId" + Thread.currentThread().getId());
        getSignInPageObject().verifySignInPage();
        getSignInPageObject().loginAs("EEADMIN_ADMIN_AUT_1");
        getDashboardPageObject().verifyDashboardPage();
        getDashboardPageObject().clickOnMenu("Admin");
        getAdminPageObject().verifyAdminPage();
        getAdminPageObject().navigateToLeftMenu("Systems");
        getAdminSystemPageObject().searchSystem("Systems","1");
        getAdminSystemPageObject().clickOnSystemMenuItem("Access");
        getAdminSystemAccessPageObject().verifyAccessPageElements("Systems","1");
    }

    @Test(groups = {"sanity","prod_sanity"})
    public void ENLA1909_verifyEEAdminPageOfASystemIsDisplayed(Method method) {
        ExtentTestManager.startTest(method.getName(), "Verify EEAdmin Page with Admin Role");
        ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId" + Thread.currentThread().getId());
        getSignInPageObject().verifySignInPage();
        getSignInPageObject().loginAs("EEADMIN_ADMIN_AUT_1");
        getDashboardPageObject().verifyDashboardPage();
        getDashboardPageObject().clickOnMenu("Admin");
        getAdminPageObject().verifyAdminPage();
        getAdminPageObject().navigateToLeftMenu("Systems");
        getAdminSystemPageObject().searchSystem("Systems","1");
        getAdminSystemPageObject().clickOnSystemMenuItem("EEAdmin");
        getEEAdminPageObject().verifyEEAdminPageElements("Systems","1");
    }

    @Test(groups = {"sanity","prod_sanity"})
    public void ENLA1910_verifySystemRollupDataView(Method method) {
        ExtentTestManager.startTest(method.getName(), "Verify Enlighten Mobile View with Admin Role");
        ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId" + Thread.currentThread().getId());
        getSignInPageObject().verifySignInPage();
        getSignInPageObject().loginAs("EEADMIN_ADMIN_AUT_1");
        getDashboardPageObject().verifyDashboardPage();
        getDashboardPageObject().clickOnMenu("Admin");
        getAdminPageObject().verifyAdminPage();
        getAdminPageObject().navigateToLeftMenu("Systems");
        getAdminSystemPageObject().searchSystem("Systems","1");
        getAdminSystemPageObject().clickOnSystemDataTool("Show Site Rollups");
        getAdminRollupsPageObject().verifyYesterdayRollupData();

    }







    }
