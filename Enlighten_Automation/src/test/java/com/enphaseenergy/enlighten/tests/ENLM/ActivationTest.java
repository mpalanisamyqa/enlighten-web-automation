package com.enphaseenergy.enlighten.tests.ENLM;


import com.enphaseenergy.enlighten.tests.BaseTest;
import com.enphaseenergy.enlighten.utils.Report.ExtentTestManager;
import com.relevantcodes.extentreports.LogStatus;
import org.testng.annotations.Test;

import java.lang.reflect.Method;


/**
 * @author mnpalanisamy
 */
public class ActivationTest extends BaseTest {


    @Test(groups = {"sanity","qa2"})
    public void verifyCreateActivation(Method method){
        ExtentTestManager.startTest(method.getName(),"Verify Create New Activation with Installer Role");
        ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId"+ Thread.currentThread().getId());
        getSignInPageObject().verifySignInPage();
        getSignInPageObject().loginAs("INSTALLER_AUT_1");
        getDashboardPageObject().verifyDashboardPage();
        getDashboardPageObject().clickOnMenu("Systems");
        getSystemPageObject().verifySystemsPage();
        getSystemPageObject().clickOnTab("Activations");
        getSystemPageObject().clickOnAddANewSystemLink();
        getActivationPageObject().verifyNewActivationPage();
        getActivationPageObject().createNewSystem("Activation","1");
        getActivationPageObject().verifySuccessfullActivationMessage();
    }

    @Test(groups = {"sanity","qa2"}, dependsOnMethods = {"verifyCreateActivation"})
    public void verifyReadActivation(Method method){
        ExtentTestManager.startTest(method.getName(),"Verify Read New Activation with Installer Role");
        ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId"+ Thread.currentThread().getId());
        getSignInPageObject().verifySignInPage();
        getSignInPageObject().loginAs("INSTALLER_AUT_1");
        getDashboardPageObject().verifyDashboardPage();
        getDashboardPageObject().clickOnMenu("Systems");
        getSystemPageObject().verifySystemsPage();
        getSystemPageObject().clickOnTab("Activations");
        getSystemPageObject().verifyActivationTableIsDisplayed();
        getSystemPageObject().searchActivations("Activation","1");
        getActivationPageObject().verifyActivatedSystemDetails("Activation","1");

    }

    @Test(groups = {"sanity","qa2"}, dependsOnMethods = {"verifyReadActivation"})
    public void verifyUpdateActivation(Method method){
        ExtentTestManager.startTest(method.getName(),"Verify Update New Activation with Installer Role");
        ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId"+ Thread.currentThread().getId());
        getSignInPageObject().verifySignInPage();
        getSignInPageObject().loginAs("INSTALLER_AUT_1");
        getDashboardPageObject().verifyDashboardPage();
        getDashboardPageObject().clickOnMenu("Systems");
        getSystemPageObject().verifySystemsPage();
        getSystemPageObject().clickOnTab("Activations");
        getSystemPageObject().verifyActivationTableIsDisplayed();
        getSystemPageObject().searchActivations("Activation","1");
        getActivationPageObject().updateActivatedSystem("Activation","2");
        getActivationPageObject().verifySuccessfullUpdateActivationMessage();
        getActivationPageObject().verifyActivatedSystemDetails("Activation","2");
    }

    @Test(groups = {"sanity","qa2"}, dependsOnMethods = {"verifyUpdateActivation"})
    public void verifyDeleteActivation(Method method){
        ExtentTestManager.startTest(method.getName(),"Verify Delete New Activation with Installer Role");
        ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId"+ Thread.currentThread().getId());
        getSignInPageObject().verifySignInPage();
        getSignInPageObject().loginAs("INSTALLER_AUT_1");
        getDashboardPageObject().verifyDashboardPage();
        getDashboardPageObject().clickOnMenu("Systems");
        getSystemPageObject().verifySystemsPage();
        getSystemPageObject().clickOnTab("Activations");
        getSystemPageObject().verifyActivationTableIsDisplayed();
        getSystemPageObject().searchActivations("Activation","2");
        getActivationPageObject().deleteActivation();
        getActivationPageObject().verifySuccessfullRemovalMessage();
    }
}
