package com.enphaseenergy.enlighten.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import java.util.List;

/**
 * @author mnpalanisamy
 */
public class AdminSystemAccessPage implements BaseObjects {
    public AdminSystemAccessPage(WebDriver driver){
        PageFactory.initElements(driver,this);
    }

    /*Page Objects*/
    @FindBy(id = "system_roles_contents")
    private WebElement systemRolesAccordion;

    @FindBy(id = "system_privacy_contents")
    private WebElement privacySettingsAccordion;

    @FindBy(id="update_privacy")
    private WebElement updatePrivacyForm;

    @FindBy(id = "company_srch")
    private WebElement companyNameSearchTextField;

    @FindBy(id = "user_srch")
    private WebElement userNameOrEmailSearchTextField;

    @FindBy(id = "site_installer_id")
    private WebElement systemRolesInstallerDropdown;

    @FindBy(id = "site_maintainer_id")
    private WebElement systemRolesMaintainerDropdown;

    @FindBy(id = "site_owner_id")
    private WebElement systemRolesSiteOwnerDropdown;

    @FindBy(id = "site_partner_id")
    private WebElement systemRolesPartnerDropdown;

    @FindBy(css = "div#company_access a[href*='/admin'] span")
    private List<WebElement> accessPageCompaniesList;

    @FindBy(css = "div#user_access a[href*='/admin'] span")
    private List<WebElement> accessPageUsersList;

    /*Page Methods*/

    /**
     * @param sheetName
     * @param rowIndex
     */
    public void verifyCompaniesUnderSystemAccess(String sheetName, String rowIndex){
        String expectedInstaller = testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Installer Name");
        String expectedPartner = testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Partner Name");
        assertHelper.assertElementInList(expectedInstaller,accessPageCompaniesList);
        assertHelper.assertElementInList(expectedPartner,accessPageCompaniesList);
    }

    /**
     * @param sheetName
     * @param rowIndex
     */
    public void verifyUsersUnderSystemAccess(String sheetName, String rowIndex){
        String expectedOwner = testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Owner");
        assertHelper.assertElementInList(expectedOwner,accessPageUsersList);

    }

    /**
     * @param sheetName
     * @param rowIndex
     */
    public void verifyInstallerInSystemRoles(String sheetName, String rowIndex){
        String expectedInstaller = testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Installer Name");
        String acutalInstaller = dropDownHelper.getSelectedValue(systemRolesInstallerDropdown);
        assertHelper.compareTwoValues(acutalInstaller,expectedInstaller);

    }

    /**
     * @param sheetName
     * @param rowIndex
     */
    public void verifySystemMaintainerInSystemRoles(String sheetName, String rowIndex){
        String expectedMaintainer = testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Maintainer Name");
        String acutalMaintainer = dropDownHelper.getSelectedValue(systemRolesMaintainerDropdown);
        assertHelper.compareTwoValues(acutalMaintainer,expectedMaintainer);
    }

    /**
     * @param sheetName
     * @param rowIndex
     */
    public void verifyOwnerInSystemRoles(String sheetName, String rowIndex){
        String expectedOwner = testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Owner");
        String acutalOwner = dropDownHelper.getSelectedValue(systemRolesSiteOwnerDropdown);
        assertHelper.compareTwoValues(acutalOwner,expectedOwner);
    }

    /**
     * @param sheetName
     * @param rowIndex
     */
    public void verifyPartnerInSystemRoles(String sheetName, String rowIndex){
        String expectedPartner = testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Partner Name");
        String acutalPartner = dropDownHelper.getSelectedValue(systemRolesPartnerDropdown);
        assertHelper.compareTwoValues(acutalPartner,expectedPartner);
    }

    /**
     * @param sheetName
     * @param rowIndex
     */
    public void verifyAccessPageElements(String sheetName, String rowIndex){
        verifyInstallerInSystemRoles(sheetName,rowIndex);
        verifySystemMaintainerInSystemRoles(sheetName, rowIndex);
        verifyOwnerInSystemRoles(sheetName, rowIndex);
        verifyPartnerInSystemRoles(sheetName, rowIndex);
        verifyCompaniesUnderSystemAccess(sheetName, rowIndex);
        verifyUsersUnderSystemAccess(sheetName, rowIndex);
    }
}
