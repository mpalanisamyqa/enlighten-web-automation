package com.enphaseenergy.enlighten.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

/**
 * @author mnpalanisamy
 */
public class CompanyInformationPage implements BaseObjects{
    public CompanyInformationPage(WebDriver driver){
        PageFactory.initElements(driver,this);
    }

    /*Objects of the Page*/

    @FindBy(id = "company_name")
    private WebElement companyNameField;

    @FindBy(id = "address_country")
    private WebElement countryDropdown;

    @FindBy(id = "address_address1")
    private WebElement streetAddress1Field;

    @FindBy(id = "address_address2")
    private WebElement streetAddress2Field;

    @FindBy(id = "address_city")
    private WebElement cityField;

    @FindBy(id = "address_state")
    private WebElement stateDropdown;

    @FindBy(id = "address_zip")
    private WebElement zipcodeField;

    @FindBy(id = "company_url")
    private WebElement websiteURLField;

    @FindBy(id = "company_support_phone")
    private WebElement customerSupportPhoneNumberField;

    @FindBy(id = "company_support_email")
    private WebElement customerSupportEmailField;

    @FindBy(id = "contacts_same_as_above")
    private WebElement contactsSameAsAboveCheckbox;

    @FindBy(id = "company_mongo_phone")
    private WebElement otherContactsPhoneNumberField;

    @FindBy(id = "company_mongo_email")
    private WebElement otherContactsEmailField;

    @FindBy(id = "company_mongo_service_type_roofing")
    private WebElement roofingCheckbox;

    @FindBy(id = "company_mongo_service_type_solar")
    private WebElement solarCheckbox;

    @FindBy(id = "company_mongo_service_type_storage")
    private WebElement storageCheckbox;

    @FindBy(id = "company_mongo_year_founded")
    private WebElement yearFoundedDropDown;

    @FindBy(id = "company_mongo_yelp_account_link")
    private WebElement yelpAccountField;

    @FindBy(id = "company_mongo_typical_installations_per_year")
    private WebElement typicalInstallationPerYearField;

    @FindBy(id = "company_mongo_typical_installations_per_month")
    private WebElement typicalInstallationMWPerYearField;

    @FindBy(id = "company_mongo_number_of_sale_employees")
    private WebElement employeesInSalesField;

    @FindBy(id = "company_mongo_number_of_installation_employees")
    private WebElement employeesInInstallationsField;

    @FindBy(id = "company_mongo_labor_warranty")
    private WebElement laborWarrantyOfferedDropdown;

    @FindBy(id = "company_form_submit")
    private WebElement updateAccountButton;

    @FindBy(id = "delete")
    private WebElement companyLogoDeleteButton;

    @FindBy(css = "div#saved_marketing_description p")
    private WebElement savedCompanyDescription;

    @FindBy(id = "edit_description_link")
    private WebElement companyDescriptionEditButton;

    @FindBy(id = "marketing_description_help_trigger")
    private WebElement companyDescriptionHelpButton;

    @FindBy(id = "company_marketing_description_help")
    private WebElement companyDescriptionHelpText;

    @FindBy(id = "company_marketing_description")
    private WebElement companyDescriptionEditText;

    @FindBy(xpath = "//span[text()='Close']")
    private WebElement editPopUpCloseButton;

    @FindBy(xpath = "//span[text()='Save Changes']/..")
    private WebElement editPopUpSaveChangesButton;

    @FindBy(id = "edit_custom_welcome_message_link")
    private WebElement welcomeMessageEditButton;

    @FindBy(id = "custom_welcome_message_help_trigger")
    private WebElement welcomeMessageHelpButton;

    @FindBy(id = "company_custom_welcome_message_help")
    private WebElement welcomeMessageHelpText;

    @FindBy(css = "a[target='preview_system']")
    private WebElement previewCompanyDescriptionLink;

    @FindBy(css = "a[target='preview_welcome_email']")
    private WebElement previewWelcomeMessageLink;

    @FindBy(css = "div#saved_custom_welcome_message p")
    private WebElement savedWelcomeMessage;

    @FindBy(id = "ui-dialog-title-company_marketing_description_modal")
    private WebElement editCompanyInformationDialogBox;

    @FindBy(css = "div.flash_message div.text")
    private WebElement flashMessage;

    @FindBy(id = "ui-dialog-title-company_custom_welcome_message_modal")
    private WebElement editWelcomeMessageDialogBox;

    @FindBy(id = "company_custom_welcome_message")
    private WebElement welcomeMessageEditText;




    /*Constant Strings*/

    private static final String EDITDESCRIPTIONSUCCESSFULLYSAVEDMESSAGE = "Company information saved successfully.";
    private static final String COMPANYINFORMATIONSUCESSFULLYSAVEDMESSAGE = "Company saved successfully.";
    private static final String EDITWELCOMEMESSAGESUCCESSFULLYSAVEDMESSAGE = "Customer welcome email message saved successfully.";

    /*Page Methods*/

    /**
     * @param sheetName
     * @param rowIndex
     */
    public void readCompanyInformation(String sheetName, String rowIndex){
//        progressHelpers.waitForPageLoad();
        progressHelpers.waitForElementToDisplay(companyNameField);
        Assert.assertEquals(companyNameField.getAttribute("value"),testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Company Name"));
        Assert.assertEquals(dropDownHelper.getSelectedValue(countryDropdown),testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Country"));
        Assert.assertEquals(streetAddress1Field.getAttribute("value"),testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Street Address"));
        Assert.assertEquals(streetAddress2Field.getAttribute("value"),testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Street Address2"));
        Assert.assertEquals(cityField.getAttribute("value"),testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"City"));
        Assert.assertEquals(dropDownHelper.getSelectedValue(stateDropdown),testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"State"));
        Assert.assertEquals(zipcodeField.getAttribute("value"),testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Zipcode"));
        Assert.assertEquals(websiteURLField.getAttribute("value"),testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Website URL"));
        Assert.assertEquals(customerSupportPhoneNumberField.getAttribute("value"),testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Customer Support Phone"));
        Assert.assertEquals(customerSupportEmailField.getAttribute("value"),testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Customer Support Email"));
        Assert.assertEquals(otherContactsPhoneNumberField.getAttribute("value"),testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Other Contacts Phone"));
        Assert.assertEquals(otherContactsEmailField.getAttribute("value"),testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Other Contacts Email"));
        Assert.assertEquals(Boolean.valueOf(checkBoxAndRadioButtonHelper.isIselected(roofingCheckbox)),Boolean.valueOf(testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Roofing Service")));
        Assert.assertEquals(Boolean.valueOf(checkBoxAndRadioButtonHelper.isIselected(solarCheckbox)),Boolean.valueOf(testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Solar Service")));
        Assert.assertEquals(Boolean.valueOf(checkBoxAndRadioButtonHelper.isIselected(storageCheckbox)),Boolean.valueOf(testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Storage Service")));
        Assert.assertEquals(dropDownHelper.getSelectedValue(yearFoundedDropDown),testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Year Founded"));
        Assert.assertEquals(yelpAccountField.getAttribute("value"),testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Yelp Account Link"));
        Assert.assertEquals(typicalInstallationPerYearField.getAttribute("value"),testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Installations Per Year"));
        Assert.assertEquals(typicalInstallationMWPerYearField.getAttribute("value"),testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Installations MW Per Year"));
        Assert.assertEquals(employeesInSalesField.getAttribute("value"),testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Employees In Sales"));
        Assert.assertEquals(employeesInInstallationsField.getAttribute("value"),testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Employees Doing Installations"));
        Assert.assertEquals(dropDownHelper.getSelectedValue(laborWarrantyOfferedDropdown),testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Labor Warranty Offered"));
//        Assert.assertEquals(savedCompanyDescription.getText().trim(),testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Company Description"));
//        Assert.assertEquals(savedWelcomeMessage.getText().trim(),testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Welcome Message"));
    }

    /**
     * @param sheetName
     * @param rowIndex
     */
    public void updateCompanyInformation(String sheetName, String rowIndex){
//        progressHelpers.waitForPageLoad();
        progressHelpers.waitForElementToDisplay(companyNameField);
        enterCompanyName(testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Company Name"));
        selectCountry(testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Country"));
        enterStreetAddress(testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Street Address"));
        enterStreetAddress2(testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Street Address2"));
        enterCity(testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"City"));
        selectState(testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"State"));
        enterZipcode(testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Zipcode"));
        enterWebsiteURL(testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Website URL"));
        enterCustomerSupportPhone(testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Customer Support Phone"));
        enterCustomerSupportEmail(testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Customer Support Email"));
        enterOtherContactsPhone(testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Other Contacts Phone"));
        enterOtherContactsEmail(testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Other Contacts Email"));
        selectServiceType("Roofing",Boolean.valueOf(testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Roofing Service")));
        selectServiceType("Solar",Boolean.valueOf(testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Solar Service")));
        selectServiceType("Storage",Boolean.valueOf(testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Storage Service")));
        selectYearFounded(testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Year Founded"));
        enterYelpAccountLink(testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Yelp Account Link"));
        enterTypicalInstallationsPerYear(testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Installations Per Year"));
        enterTypicalInstallationsMWPerYear(testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Installations MW Per Year"));
        enterNumberOfEmployeesInSales(testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Employees In Sales"));
        enterNumberOfEmployeesInInstallations(testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Employees Doing Installations"));
        selectLaborWarranty(testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Labor Warranty Offered"));
//        Below scripts are need to be revisited
//        enterCompanyDescrition(testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Company Description"));
//        verifyCompanyDescriptionSavedSuccessfullyMessage();
//        clickOnEditPOPUPCloseButton();
//        progressHelpers.waitForElementToInvisible(editPopUpSaveChangesButton);
        //Edit welcome message is to be done
        /*
        progressHelpers.hardWait(3000);
        enterCompanyWelcomeMessage(testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Welcome Message"));
        verifyWelcomeMessageSavedSuccessfullyMessage();
        clickOnEditPOPUPCloseButton();
        progressHelpers.waitForElementToInvisible(editPopUpSaveChangesButton);
        */
        progressHelpers.waitForElementToBeClickable(updateAccountButton);
        clickOnUpdateAccount();
    }

    /**
     * @param companyName
     */
    public void enterCompanyName(String companyName){
        generalHelper.enterTextInTextField(companyNameField,companyName);
    }

    /**
     * @param countryName
     */
    public void selectCountry(String countryName){
        dropDownHelper.SelectUsingVisibleValue(countryDropdown,countryName);
    }

    /**
     * @param streetAddress
     */
    public void enterStreetAddress(String streetAddress){
        generalHelper.enterTextInTextField(streetAddress1Field,streetAddress);
    }

    /**
     * @param streetAddress2
     */
    public void enterStreetAddress2(String streetAddress2){
        generalHelper.enterTextInTextField(streetAddress2Field,streetAddress2);
    }

    /**
     * @param cityName
     */
    public void enterCity(String cityName){
        generalHelper.enterTextInTextField(cityField,cityName);
    }

    /**
     * @param state
     */
    public void selectState(String state){
        dropDownHelper.SelectUsingVisibleValue(stateDropdown,state);
    }

    /**
     * @param zipcode
     */
    public void enterZipcode(String zipcode){
        generalHelper.enterTextInTextField(zipcodeField,zipcode);
    }

    /**
     * @param websiteURL
     */
    public void enterWebsiteURL(String websiteURL){
        generalHelper.enterTextInTextField(websiteURLField,websiteURL);
    }

    /**
     * @param phone
     */
    public void enterCustomerSupportPhone(String phone){
        generalHelper.enterTextInTextField(customerSupportPhoneNumberField,phone);
    }

    /**
     * @param email
     */
    public void enterCustomerSupportEmail(String email){
        generalHelper.enterTextInTextField(customerSupportEmailField,email);
    }

    /**
     * @param phone
     */
    public void enterOtherContactsPhone(String phone){
        generalHelper.enterTextInTextField(otherContactsPhoneNumberField,phone);
    }

    /**
     * @param email
     */
    public void enterOtherContactsEmail(String email){
        generalHelper.enterTextInTextField(otherContactsEmailField,email);
    }

    /**
     * @param serviceType
     * @param serviceValue
     */
    public void selectServiceType(String serviceType, boolean serviceValue){
        switch (serviceType.toLowerCase()){
            case "roofing":
                if (serviceValue) {
                    checkBoxAndRadioButtonHelper.selectCheckBox(roofingCheckbox);
                } else {
                    checkBoxAndRadioButtonHelper.unSelectCheckBox(roofingCheckbox);
                }
                break;
            case "solar":
                if (serviceValue) {
                    checkBoxAndRadioButtonHelper.selectCheckBox(solarCheckbox);
                } else {
                    checkBoxAndRadioButtonHelper.unSelectCheckBox(solarCheckbox);
                }
                break;
            case "storage":
                if (serviceValue) {
                    checkBoxAndRadioButtonHelper.selectCheckBox(storageCheckbox);
                } else {
                    checkBoxAndRadioButtonHelper.unSelectCheckBox(storageCheckbox);
                }
                break;
        }
    }

    /**
     * @param yearFounded
     */
    public void selectYearFounded(String yearFounded){
        dropDownHelper.SelectUsingVisibleValue(yearFoundedDropDown,yearFounded);
    }

    /**
     * @param yelpAccountLink
     */
    public void enterYelpAccountLink(String yelpAccountLink){
        generalHelper.enterTextInTextField(yelpAccountField,yelpAccountLink);
    }

    /**
     * @param numberOfInstallations
     */
    public void enterTypicalInstallationsPerYear(String numberOfInstallations){
        generalHelper.enterTextInTextField(typicalInstallationPerYearField,numberOfInstallations);
    }

    /**
     * @param numberOfMWPerYear
     */
    public void enterTypicalInstallationsMWPerYear(String numberOfMWPerYear){
        generalHelper.enterTextInTextField(typicalInstallationMWPerYearField,numberOfMWPerYear);
    }

    /**
     * @param noOfEmployees
     */
    public void enterNumberOfEmployeesInSales(String noOfEmployees){
        generalHelper.enterTextInTextField(employeesInSalesField,noOfEmployees);
    }

    /**
     * @param noOfInstallations
     */
    public void enterNumberOfEmployeesInInstallations(String noOfInstallations){
        generalHelper.enterTextInTextField(employeesInInstallationsField,noOfInstallations);
    }

    /**
     * @param laborWarranty
     */
    public void selectLaborWarranty(String laborWarranty){
        dropDownHelper.SelectUsingVisibleValue(laborWarrantyOfferedDropdown,laborWarranty);
    }

    /**
     * @param companyDescription
     */
    public void enterCompanyDescrition(String companyDescription){
        clickOnCompanyDescriptionEditButton();
//        progressHelpers.waitForPageLoad();
        progressHelpers.waitForElementToDisplay(editCompanyInformationDialogBox);
        javascriptHelper.enterText(companyDescriptionEditText,companyDescription);
        progressHelpers.waitForElementToBeClickable(editPopUpSaveChangesButton);
        progressHelpers.hardWait(2000);
        javascriptHelper.clickOnElement(editPopUpSaveChangesButton);
        progressHelpers.hardWait(2000);
    }

    public void verifyCompanyDescriptionSavedSuccessfullyMessage(){
        progressHelpers.waitForElementToDisplay(flashMessage);
        Assert.assertEquals(flashMessage.getText(),EDITDESCRIPTIONSUCCESSFULLYSAVEDMESSAGE);
    }

    public void clickOnCompanyDescriptionEditButton(){
        companyDescriptionEditButton.click();
    }

    public void clickOnEditPOPUPCloseButton(){
        javascriptHelper.clickOnElement(editPopUpCloseButton);
    }

    /**
     * @param welcomeMessage
     */
    public void enterCompanyWelcomeMessage(String welcomeMessage){
        clickOnWelcomeMessageEditButton();
        progressHelpers.waitForElementToDisplay(editWelcomeMessageDialogBox);
        javascriptHelper.enterText(welcomeMessageEditText,welcomeMessage);
        progressHelpers.waitForElementToBeClickable(editPopUpSaveChangesButton);
        progressHelpers.hardWait(2000);
        javascriptHelper.clickOnElement(editPopUpSaveChangesButton);
        progressHelpers.hardWait(2000);
    }

    public void clickOnWelcomeMessageEditButton(){
        welcomeMessageEditButton.click();
    }

    public void verifyWelcomeMessageSavedSuccessfullyMessage(){
        progressHelpers.waitForElementToDisplay(flashMessage);
        Assert.assertEquals(flashMessage.getText(),EDITWELCOMEMESSAGESUCCESSFULLYSAVEDMESSAGE);
    }

    public void clickOnUpdateAccount(){
        updateAccountButton.click();
    }

    public void verifyCompanyInformationSavedSuccessfully(){
        Assert.assertEquals(flashMessage.getText(),COMPANYINFORMATIONSUCESSFULLYSAVEDMESSAGE);
    }

}
