package com.enphaseenergy.enlighten.pages;

import com.enphaseenergy.enlighten.utils.Data.PropertyReader;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.util.*;

/**
 * @author mnpalanisamy
 */
public class AdminActivationPage implements BaseObjects{
    public AdminActivationPage(WebDriver driver){
        PageFactory.initElements(driver,this);
    }

    /*Page Objects*/

    @FindBy(id="site_activations_datatables")
    private WebElement activationTable;

    @FindBy(id = "site_activations_datatables_processing")
    private WebElement loadingProgressBar;

    @FindBy(name = "id")
    private WebElement activationIdSearchTextField;

    @FindBy(name = "name")
    private WebElement activationNameSearchTextField;

    @FindBy(name = "city")
    private WebElement activationCitySearchTextField;

    @FindBy(name = "state")
    private WebElement activationStateSearchTextField;

    @FindBy(name = "installer")
    private WebElement activationInstallerSearchTextField;

    @FindBy(name = "owner")
    private WebElement activationOwnerSearchTextField;

    @FindBy(name = "stage")
    private WebElement activationStageDropdown;

    @FindBy(id = "submit")
    private WebElement activationSearchButton;

    @FindBy(css = "table#site_activations_datatables tr td:nth-child(1)")
    private List<WebElement> activationTableIDs;

    @FindBy(css = "table#site_activations_datatables tr td:nth-child(2) div.name_cell a")
    private List<WebElement> activationTableNames;

    @FindBy(css = "table#site_activations_datatables tr td:nth-child(3)")
    private List<WebElement> activationTableCities;

    @FindBy(css = "table#site_activations_datatables tr td:nth-child(4)")
    private List<WebElement> activationTableStates;

    @FindBy(css = "table#site_activations_datatables tr td:nth-child(5)")
    private List<WebElement> activationTableInstallers;

    @FindBy(css = "table#site_activations_datatables tr td:nth-child(6)")
    private List<WebElement> activationTableOwners;

    @FindBy(css = "table#site_activations_datatables tr td:nth-child(7)")
    private List<WebElement> activationTableStages;

    @FindBy(name = "site_activations_datatables_length")
    private WebElement tableViewSize;

    @FindBy(id = "id")
    private WebElement activationTableHeaderID;

    @FindBy(id = "name")
    private WebElement activationTableHeaderName;

    @FindBy(id = "city")
    private WebElement activationTableHeaderCity;

    @FindBy(id = "state")
    private WebElement activationTableHeaderState;

    @FindBy(id = "installer")
    private WebElement activationTableHeaderInstaller;

    @FindBy(id = "owner")
    private WebElement activationTableHeaderOwner;

    @FindBy(id = "stage")
    private WebElement activationTableHeaderStage;

    /*Page Methods*/

    /**
     * @param sheetName
     * @param rowIndex
     */
    public void verifyAllTheFiltersInTable(String sheetName, String rowIndex){
        progressHelpers.waitForElementToInvisible(loadingProgressBar);
        verifyActivationTableFilter(activationIdSearchTextField,activationTableIDs,testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"ID"));
        verifyActivationTableFilter(activationNameSearchTextField,activationTableNames,testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Name"));
        verifyActivationTableFilter(activationCitySearchTextField,activationTableCities,testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"City"));
        verifyActivationTableFilter(activationStateSearchTextField,activationTableStates,testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"State"));
        verifyActivationTableFilter(activationInstallerSearchTextField,activationTableInstallers,testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Installer"));
        verifyActivationTableFilter(activationOwnerSearchTextField,activationTableOwners,testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Owner"));
        verifyActivationTableFilterForStages(activationStageDropdown,activationTableStages,testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Stage"));
    }

    /**
     * @param filterLocator
     * @param tableRows
     * @param filterText
     */
    public void verifyActivationTableFilter(WebElement filterLocator, List<WebElement> tableRows, String filterText){
        progressHelpers.waitForElementToBeClickable(filterLocator);
        if(filterLocator.getTagName().equalsIgnoreCase("Select")){
            dropDownHelper.SelectUsingVisibleValue(filterLocator,filterText);
        }else {
            generalHelper.enterTextInTextField(filterLocator, filterText);
        }
        clickOnSearchButton();
        progressHelpers.waitForElementToInvisible(loadingProgressBar);
        verifyTableData(tableRows,filterText);
        if(filterLocator.getTagName().equalsIgnoreCase("Select")){
            dropDownHelper.SelectUsingVisibleValue(filterLocator,"All");
        }else {
            generalHelper.clearTextField(filterLocator);
        }
        clickOnSearchButton();
    }

    public void clickOnSearchButton(){
        activationSearchButton.click();
    }

    /**
     * @param tableRows
     * @param expectedText
     */
    public void verifyTableData(List<WebElement> tableRows, String expectedText){
        for (WebElement rowItem: tableRows){
            if(rowItem.getText().contains("-")){
                List<String> multipleItems = Arrays.asList(rowItem.getText().split("[\\-]"));
                String actualItem = null;
                for(String eachItem:multipleItems){
                    if(eachItem.trim().equalsIgnoreCase(expectedText)){
                        actualItem = eachItem.trim();
                    }
                }
                Assert.assertEquals(actualItem,expectedText);
            }
            else {
                Assert.assertEquals(rowItem.getText(),expectedText);
            }
        }
    }

    /**
     * @param filterLocator
     * @param tableRows
     * @param filterText
     */
    public void verifyActivationTableFilterForStages(WebElement filterLocator,List<WebElement> tableRows, String filterText){
        progressHelpers.waitForElementToBeClickable(filterLocator);
        if(filterLocator.getTagName().equalsIgnoreCase("Select")){
            dropDownHelper.SelectUsingVisibleValue(filterLocator,filterText);
        }else {
            generalHelper.enterTextInTextField(filterLocator, filterText);
        }
        clickOnSearchButton();
        progressHelpers.waitForElementToInvisible(loadingProgressBar);
        verifyTableData(tableRows,filterText.split("[\\-]")[1].trim());
        if (filterLocator.getTagName().equalsIgnoreCase("Select")) {
            dropDownHelper.SelectUsingVisibleValue(filterLocator, "All Activations");
        } else {
            generalHelper.clearTextField(filterLocator);
        }
        clickOnSearchButton();
    }

    public void verifyFilterWithAllTheStages(){
        progressHelpers.waitForElementToDisplay(activationTable);
        progressHelpers.waitForElementToDisplay(tableViewSize);
        dropDownHelper.SelectUsingVisibleValue(tableViewSize,"100");
        progressHelpers.waitForElementToInvisible(loadingProgressBar);
        List<String> listOfStagesInDropdown = new ArrayList<String>(Arrays.asList("All Activations","In Progress (Stages 1-4)","1 - Started","2 - Connecting","3 - Verifying","4 - Ready","5 - Final"));
        List<String> listOfStages = new ArrayList<String>(Arrays.asList("1 - Started","2 - Connecting","3 - Verifying","4 - Ready","5 - Final"));

        for(String item:listOfStagesInDropdown){
            switch (item.toLowerCase()){
                case "all activations":
                    verifyActivationTableFilterForStages(activationStageDropdown,activationTableStages,item,listOfStages);
                    break;
                case "in progress (stages 1-4)":
                    verifyActivationTableFilterForStages(activationStageDropdown,activationTableStages,item,listOfStages);
                    break;
                    default:
                        verifyActivationTableFilterForStages(activationStageDropdown,activationTableStages,item);
                        break;

            }
        }
    }

    /**
     * @param filterLocator
     * @param tableRows
     * @param filterText
     * @param verifyTexts
     */
    public void verifyActivationTableFilterForStages(WebElement filterLocator,List<WebElement> tableRows, String filterText,List<String> verifyTexts){
        progressHelpers.waitForElementToBeClickable(filterLocator);
        if(filterLocator.getTagName().equalsIgnoreCase("Select")){
            dropDownHelper.SelectUsingVisibleValue(filterLocator,filterText);
        }else {
            generalHelper.enterTextInTextField(filterLocator, filterText);
        }
        clickOnSearchButton();
        progressHelpers.waitForElementToInvisible(loadingProgressBar);
        verifyTableData(tableRows,verifyTexts);
        if (filterLocator.getTagName().equalsIgnoreCase("Select")) {
            dropDownHelper.SelectUsingVisibleValue(filterLocator, "All Activations");
        } else {
            generalHelper.clearTextField(filterLocator);
        }
        clickOnSearchButton();
    }

    /**
     * @param tableRows
     * @param expectedText
     */
    public void verifyTableData(List<WebElement> tableRows, List<String> expectedText){
        Set<String> actualItems = new HashSet<>();
        for(WebElement item: tableRows){
            actualItems.add(item.getText());
        }
        for(String item:actualItems){
            assertHelper.assertTrueCondition(expectedText.contains(item));
        }
    }

    public void verifyActivationTableSortFunctionality(){
        //TBD Optimize the below code
        List[] allDatas = new List[]{activationTableCities, activationTableStates, activationTableInstallers, activationTableOwners, activationTableStages, activationTableNames};
        List<String> allHeaders = new ArrayList<>(Arrays.asList("City","State","Installer","Owner","Stage","Name"));
        verifySorting("descending",activationTableIDs);
        clickOnSystemTableHeader("ID");
        verifySorting("ascending",activationTableIDs);
        for(int i=0;i<allDatas.length;i++){
            clickOnSystemTableHeader(allHeaders.get(i));
            verifySorting("ascending",allDatas[i]);
            clickOnSystemTableHeader(allHeaders.get(i));
            verifySorting("descending",allDatas[i]);
        }

    }

    /**
     * @param sortOrder
     * @param filterLocator
     */
    public void verifySorting(String sortOrder, List<WebElement> filterLocator){
        progressHelpers.waitForElementToInvisible(loadingProgressBar,Integer.valueOf(PropertyReader.getConfigProperties(System.getProperty("ENV"),"hardwaitinseconds")));
        List<String> actualTableItems = new ArrayList<>();
        List<String> expectedTableItems = new ArrayList<>();
        for(WebElement unitElement:filterLocator){
            actualTableItems.add(unitElement.getText());
            expectedTableItems.add(unitElement.getText());
        }
        if(sortOrder.equalsIgnoreCase("ascending")){
            Arrays.parallelSort(actualTableItems.toArray(new String[actualTableItems.size()]));
            Assert.assertEquals(actualTableItems,expectedTableItems);

        }else if(sortOrder.equalsIgnoreCase("descending")){
            Arrays.parallelSort(actualTableItems.toArray(new String[actualTableItems.size()]),Collections.reverseOrder());
            Assert.assertEquals(actualTableItems,expectedTableItems);
        }

    }

    /**
     * @param headerName
     */
    public void clickOnSystemTableHeader(String headerName){
        progressHelpers.waitForElementToInvisible(loadingProgressBar,Integer.valueOf(PropertyReader.getConfigProperties(System.getProperty("ENV"),"hardwaitinseconds")));
        switch (headerName.toLowerCase()){
            case "id":
                activationTableHeaderID.click();
                break;
            case "name":
                activationTableHeaderName.click();
                break;
            case "city":
                activationTableHeaderCity.click();
                break;
            case "state":
                activationTableHeaderState.click();
                break;
            case "installer":
                activationTableHeaderInstaller.click();
                break;
            case "owner":
                activationTableHeaderOwner.click();
                break;
            case "stage":
                activationTableHeaderStage.click();
                break;
        }
    }
}
