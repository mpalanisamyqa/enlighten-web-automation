package com.enphaseenergy.enlighten.utils.Report;

import com.relevantcodes.extentreports.ExtentReports;

import java.io.File;

public class ExtentManager {
    private static ExtentReports extent;

    public synchronized static ExtentReports getReporter() {
        if (extent == null) {
            //Set HTML reporting file location
            extent = new ExtentReports(System.getProperty("user.dir") + File.separator +"target" + File.separator + "Enlighten"+System.getProperty("ENV").toUpperCase()+"AutomationReport.html");
        }
        return extent;
    }
}
