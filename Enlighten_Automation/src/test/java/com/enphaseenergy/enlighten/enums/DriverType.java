package com.enphaseenergy.enlighten.enums;

public enum DriverType {
    FIREFOX,
    CHROME,
    SAFARI,
    IE,
    HEADLESS
}
