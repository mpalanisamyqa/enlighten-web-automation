package com.enphaseenergy.enlighten.utils.MySQLConn;

import com.enphaseenergy.enlighten.Helpers.LoggerHelper;
import com.enphaseenergy.enlighten.pages.SystemsPage;
import com.enphaseenergy.enlighten.utils.Data.PropertyReader;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import org.apache.log4j.Logger;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *  Java Program to connect to remote database through SSH using port forwarding
 * @author mnpalanisamy
 */
public class MySqlConnOverSSH extends PropertyReader {

    private Logger logger = LoggerHelper.getLogger(MySqlConnOverSSH.class);
    int lport=5656;
    String rhost=getConfigProperties("QA2","enlighten_ssh_host");
    String host=getConfigProperties("QA2","enlighten_ssh_host");
    int rport=3306;
    String user= getConfigProperties("QA2","enlighten_qa2_sshuser");
//    String password="sshpassword";
    String dbuserName = getConfigProperties("QA2","enlighten_sql_db_username");
    String dbpassword = getConfigProperties("QA2","enlighten_sql_db_password");
    String url = getConfigProperties("QA2","enlighten_db_url");
    String driverName=getConfigProperties("QA2","drivername");
    String privateKey = System.getProperty("user.dir") + File.separator +"properties" + File.separator + getConfigProperties("QA2","enlighten_qa2_sshprivatekey");
    protected Connection conn = null;
    protected Session session= null;


    public void connectMySql(){
        try{
            //Set StrictHostKeyChecking property to no to avoid UnknownHostKey issue
            java.util.Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");
            JSch jsch = new JSch();


            jsch.addIdentity(privateKey);
            session=jsch.getSession(user, host);
//            session.setPassword(password);
            session.setConfig(config);
            session.connect();
            logger.info("Connected");
            System.out.println("conn"+session);
            int assinged_port=session.setPortForwardingL(lport, rhost, rport);
            logger.info("localhost:"+assinged_port+" -> "+rhost+":"+rport);
            logger.info("Port Forwarded");

            //mysql database connectivity
            Class.forName(driverName);
            conn = DriverManager.getConnection (url, dbuserName, dbpassword);
            logger.info ("Database connection established");
            logger.info("DONE");
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void closeConnection(){
        try {
            if(conn != null && !conn.isClosed()){
                logger.info("Closing Database Connection");
                conn.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if(session !=null && session.isConnected()){
            logger.info("Closing SSH Connection");
            session.disconnect();
        }
    }
}
