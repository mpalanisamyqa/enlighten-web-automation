package com.enphaseenergy.enlighten.enums;

public enum Environment {
    QA2,
    PROD
}
