# Enlighten Web Automation

Prerequisites:
1. Install JDK10+
2. Install Apache Maven 3.6.0+
3. Use any of your favourite IDEs to script

Before working on the scripts do the following steps:
1. clone the repo's master branch
2. jump to the project folder "enlighten-web-automation"
3. initiate git
4. start with git process

GIT Process to follow:
1. clone the master branch to local
2. create a local feature branch in the format of "feature/<nameofemployee>/<jiraticketnumber>
3. commit the changes to remove feature branch (use the same feature branch from steps two)
4. raise a PR again develop branch
5. merge the remote branch after review changes

How to Run:
Use the testng.xml to run on your local machine, remaining xmls will be used for CI/CD


