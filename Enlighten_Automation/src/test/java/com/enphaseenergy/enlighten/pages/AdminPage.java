package com.enphaseenergy.enlighten.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

/**
 * @author mnpalanisamy
 */
public class AdminPage implements BaseObjects{
    public AdminPage(WebDriver driver){
        PageFactory.initElements(driver,this);
    }

    /*Objects of the Page*/

    @FindBy(id = "nav_systems")
    private WebElement systemsNavMenu;

    @FindBy(id = "nav_activations")
    private WebElement activationsNavMenu;

    @FindBy(id = "nav_companies")
    private WebElement companiesNavMenu;

    @FindBy(id = "nav_users")
    private WebElement usersNavMenu;

    @FindBy(id = "nav_settings")
    private WebElement settingsNavMenu;

    @FindBy(id = "nav_reports")
    private WebElement reportsNavMenu;

    @FindBy(id = "nav_profiles")
    private WebElement gridprofilesNavMenu;

    @FindBy(id = "nav_vogel1")
    private WebElement vogeltrack1NavMenu;

    @FindBy(id = "nav_vogel2")
    private WebElement vogeltrack2NavMenu;

    @FindBy(id = "nav_features")
    private WebElement featuresNavMenu;

    @FindBy(id = "nav_rollups")
    private WebElement rollupsNavMenu;

    @FindBy(css = "div#subnav a[href='/eeadmin']")
    private WebElement adminHomeSubNavMenu;

    @FindBy(css = "div#subnav a[href='/eeadmin/reports']")
    private WebElement reportsSubNavMenu;

    @FindBy(css = "div#subnav a[href='/eeadmin/pbi_projects']")
    private WebElement pbiProjectsSubNavMenu;

    @FindBy(css = "div#subnav a[href='/eeadmin/enlighten_notices']")
    private WebElement enlightenNoticesSubNavMenu;

    @FindBy(css = "div#subnav a[href='/eeadmin/content_sources']")
    private WebElement contentSourcesSubNavMenu;

    @FindBy(css = "div#subnav a[href='/eeadmin/upgrade_templates']")
    private WebElement upgradeTemplatesSubNavMenu;

    @FindBy(css = "div#subnav a[href='/eeadmin/upgrades']")
    private WebElement batchUpgradesSubNavMenu;

    @FindBy(css = "div#subnav a[href='/eeadmin/rma_upgrades']")
    private WebElement iq7rmaUpgradesSubNavMenu;

    @FindBy(css = "div#subnav a[href='/eeadmin/ab_features']")
    private WebElement abFeaturesSubNavMenu;

    @FindBy(id = "welcome_message")
    private WebElement webappReleaseVersionText;

    /*Constant Strings*/

    /*Page Methods*/

    /**
     * @param option
     */
    public void navigateToLeftMenu(String option){
        progressHelpers.waitForElementToDisplay(webappReleaseVersionText);
        switch (option.toLowerCase()){
            case "systems":
                systemsNavMenu.click();
                break;
            case "activations":
                activationsNavMenu.click();
                break;
            case "companies":
                companiesNavMenu.click();
                break;
            case "users":
                usersNavMenu.click();
                break;
            case "settings":
                settingsNavMenu.click();
                break;
            case "reports":
                reportsNavMenu.click();
                break;
            case "gridprofiles":
                gridprofilesNavMenu.click();
                break;
            case "vogeltrack1":
                vogeltrack1NavMenu.click();
                break;
            case "vogeltrack2":
                vogeltrack2NavMenu.click();
                break;
            case "features":
                featuresNavMenu.click();
                break;
            case "rollups":
                rollupsNavMenu.click();
                break;
                default:
                    adminHomeSubNavMenu.click();
                    break;
        }
    }

    /**
     * @param option
     */
    public void navigateToLeftSubMenu(String option){
        progressHelpers.waitForElementToDisplay(webappReleaseVersionText);
        switch (option.toLowerCase()){
            case "adminhome":
                adminHomeSubNavMenu.click();
                break;
            case "reports":
                reportsSubNavMenu.click();
                break;
            case "pbiprojects":
                pbiProjectsSubNavMenu.click();
                break;
            case "enlightennotices":
                enlightenNoticesSubNavMenu.click();
                break;
            case "contentsources":
                contentSourcesSubNavMenu.click();
                break;
            case "upgradetemplates":
                upgradeTemplatesSubNavMenu.click();
                break;
            case "batchupgrades":
                batchUpgradesSubNavMenu.click();
                break;
            case "iq7rmaupgrades":
                iq7rmaUpgradesSubNavMenu.click();
                break;
            case "alphabetafeatures":
                abFeaturesSubNavMenu.click();
                break;
            default:
                adminHomeSubNavMenu.click();
                break;
        }
    }

    public void verifyAdminPage(){
        progressHelpers.waitForElementToDisplay(webappReleaseVersionText);
        Assert.assertTrue(webappReleaseVersionText.isDisplayed());
    }


}
