package com.enphaseenergy.enlighten.Helpers;

import com.enphaseenergy.enlighten.utils.Driver.DriverManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.LinkedList;
import java.util.Set;

public class BrowserHelper {

    WebDriver driver = DriverManager.getDriver();
    private Logger logger = LoggerHelper.getLogger(BrowserHelper.class);


    public void goBack() {
        driver.navigate().back();
        logger.info("");
    }

    public void goForward() {
        driver.navigate().forward();
        logger.info("");
    }

    public void refresh() {
        driver.navigate().refresh();
        logger.info("");
    }

    public Set<String> getWindowHandlens() {
        logger.info("");
        return driver.getWindowHandles();
    }

    public void SwitchToWindow(int index) {

        LinkedList<String> windowsId = new LinkedList<String>(
                getWindowHandlens());

        if (index < 0 || index > windowsId.size())
            throw new IllegalArgumentException("Invalid Index : " + index);

        driver.switchTo().window(windowsId.get(index));
        logger.info(index);
    }

    public void switchToParentWindow() {
        LinkedList<String> windowsId = new LinkedList<String>(
                getWindowHandlens());
        driver.switchTo().window(windowsId.get(0));
        logger.info("");
    }

    public void switchToParentWithChildClose() {
        switchToParentWindow();

        LinkedList<String> windowsId = new LinkedList<String>(
                getWindowHandlens());

        for (int i = 1; i < windowsId.size(); i++) {
            logger.info(windowsId.get(i));
            driver.switchTo().window(windowsId.get(i));
            driver.close();
        }

        switchToParentWindow();
    }

    public void switchToFrame(By locator) {
        driver.switchTo().frame(driver.findElement(locator));
        logger.info(locator);
    }

    public void switchToFrame(String nameOrId) {
        driver.switchTo().frame(nameOrId);
        logger.info(nameOrId);
    }

    public void switchToFrame(int id) {
        driver.switchTo().frame(id);
        logger.info(id);
    }



}
