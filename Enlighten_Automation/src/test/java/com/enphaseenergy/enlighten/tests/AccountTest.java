package com.enphaseenergy.enlighten.tests;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.enphaseenergy.enlighten.utils.Report.ExtentTestManager;
import com.relevantcodes.extentreports.LogStatus;

public class AccountTest extends BaseTest{
	/**@author kulanjirajanp
	 * 	
	 */
	 @Test(groups = {"sanity","prod_sanity","qa4_sanity","qa2_sanity"})
	public void ENLM517_verifyAccountDetailText(Method method) throws Throwable {
		ExtentTestManager.startTest(method.getName(),"verifyAccountDetailText");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId"+ Thread.currentThread().getId());
		getSignInPageObject().loginAs("INSTALLER_AUT_1");
		getDashboardPageObject().clickOnMenu("account");
		getAccountPageObject().verifyAccountDetailText();

	}
	  @Test(groups = {"sanity","prod_sanity","qa4_sanity","qa2_sanity"})
	public void ENLM522_verifyAdminRecord(Method method) throws Throwable {
		ExtentTestManager.startTest(method.getName(),"verifyAdminRecord");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId"+ Thread.currentThread().getId());
		getSignInPageObject().loginAs("INSTALLER_AUT_1");
		getDashboardPageObject().clickOnMenu("account");
		getAccountPageObject().clickOnviewEnlightenLink();
		getAccountPageObject().verifyEnlightenadministrator();

	}
	@Test(groups = {"sanity","prod_sanity","qa4_sanity","qa2_sanity"})
	public void ENLM495_verifyAccountDetailMail(Method method) throws Throwable {
		ExtentTestManager.startTest(method.getName(),"verifyAccountDetailMail");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId"+ Thread.currentThread().getId());
		getSignInPageObject().loginAs("INSTALLER_AUT_1");
		getDashboardPageObject().clickOnMenu("account");
		getAccountPageObject().verifyAccountDetailMail("ADMIN_AUT_1");

	}
	@Test(groups = {"sanity","prod_sanity","qa4_sanity","qa2_sanity"})
	public void ENLM496_verifyUpdateAccountDetail(Method method) throws Throwable {
		ExtentTestManager.startTest(method.getName(),"verifyUpdateAccountDetail");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId"+ Thread.currentThread().getId());
		getSignInPageObject().loginAs("INSTALLER_AUT_1");
		getDashboardPageObject().clickOnMenu("account");
		getAccountPageObject().verifyUpdateAccountDetail("9524683284");

	}



}
