package com.enphaseenergy.enlighten.pages;

import com.enphaseenergy.enlighten.Helpers.LoggerHelper;
import com.enphaseenergy.enlighten.utils.Data.PropertyReader;
import com.google.common.collect.Ordering;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.util.*;

/**
 * @author mnpalanisamy
 */
public class AdminSystemsPage implements BaseObjects {
    public AdminSystemsPage(WebDriver driver){
        PageFactory.initElements(driver,this);
    }

    private Logger logger = LoggerHelper.getLogger(AdminSystemsPage.class);

    /*Page Objects*/
    @FindBy(id = "a_sites_datatables")
    private WebElement systemTable;

    @FindBy(css = "tr#filter_header input[name=name]")
    private WebElement systemNameSearchTextField;

    @FindBy(id = "submit")
    private WebElement systemTableSearchButton;

    @FindBy(css = "table#a_sites_datatables div[id*='name_cell'] a[href*='/admin/sites']")
    private WebElement systemTableSearchItemName;

    @FindBy(css = "table#a_sites_datatables div[id*='name_cell'] a[href*='/admin/sites']")
    private List<WebElement> systemTableSearchItemNames;

    //This is nothing but system Name
    @FindBy(id = "page_title")
    private WebElement systemAdminPageTitle;

    @FindBy(css = "div#page_head + div a")
    private List<WebElement> systemMenuItems;

    @FindBy(id = "a_sites_datatables_processing")
    private WebElement loadingProgressBar;

    @FindBy(css = "div#a_sites_datatables_info + div span a.fg-button")
    private List<WebElement> pageNos;

    @FindBy(id = "a_sites_datatables_last")
    private WebElement systemTableLastPageButton;

    @FindBy(id = "id")
    private WebElement systemTableHeaderID;

    @FindBy(id = "name")
    private WebElement systemTableHeaderName;

    @FindBy(id = "group_id")
    private WebElement systemTableHeaderGroupID;

    @FindBy(id = "city")
    private WebElement systemTableHeaderCity;

    @FindBy(id = "state")
    private WebElement systemTableHeaderState;

    @FindBy(id = "installer")
    private WebElement systemTableHeaderInstaller;

    @FindBy(id = "owner")
    private WebElement systemTableHeaderOwner;

    @FindBy(id = "pcu_count")
    private WebElement systemTableHeaderPcuCount;

    @FindBy(id = "acb_count")
    private WebElement systemTableHeaderAcbCount;

    @FindBy(id = "encharge_count")
    private WebElement systemTableHeaderEnchargeCount;

    @FindBy(id = "enpower_count")
    private WebElement systemTableHeaderEnpowerCount;

    @FindBy(id = "nsr_count")
    private WebElement systemTableHeaderQRelaysCount;

    @FindBy(id = "emu_count")
    private WebElement systemTableHeaderEmuCount;

    @FindBy(id = "pcu_types")
    private WebElement systemTableHeaderDeviceType;

    @FindBy(id = "envoy_types")
    private WebElement systemTableHeaderEnvoyType;

    @FindBy(id = "connection_type")
    private WebElement systemTableHeaderConnection;

    @FindBy(id = "status")
    private WebElement systemTableHeaderStatus;

    @FindBy(id = "stage")
    private WebElement systemTableHeaderStage;

    @FindBy(css = "table#a_sites_datatables tr td:nth-child(1)")
    private List<WebElement> systemTableIDs;

    @FindBy(css = "table#a_sites_datatables div[id*='name_cell'] a[href*='/admin/sites']")
    private List<WebElement> systemTableNames;

    @FindBy(css = "table#a_sites_datatables tr td:nth-child(4)")
    private List<WebElement> systemTableCities;

    @FindBy(css = "table#a_sites_datatables tr td:nth-child(5)")
    private List<WebElement> systemTableStates;

    @FindBy(css = "table#a_sites_datatables tr td:nth-child(6) a")
    private List<WebElement> systemTableInstallers;

    @FindBy(css = "table#a_sites_datatables tr td:nth-child(7) a")
    private List<WebElement> systemTableOwners;

    @FindBy(css = "table#a_sites_datatables tr td:nth-child(8)")
    private List<WebElement> systemTablePcus;

    @FindBy(css = "table#a_sites_datatables tr td:nth-child(9)")
    private List<WebElement> systemTableACBs;

    @FindBy(css = "table#a_sites_datatables tr td:nth-child(10)")
    private List<WebElement> systemTableEncharges;

    @FindBy(css = "table#a_sites_datatables tr td:nth-child(11)")
    private List<WebElement> systemTableEnpowers;

    @FindBy(css = "table#a_sites_datatables tr td:nth-child(12)")
    private List<WebElement> systemTableQRelays;

    @FindBy(css = "table#a_sites_datatables tr td:nth-child(13)")
    private List<WebElement> systemTableEmus;

    @FindBy(css = "table#a_sites_datatables tr td:nth-child(14)")
    private List<WebElement> systemTableDeviceTypes;

    @FindBy(css = "table#a_sites_datatables tr td:nth-child(15)")
    private List<WebElement> systemTableEnvoyTypes;

    @FindBy(css = "table#a_sites_datatables tr td:nth-child(16)")
    private List<WebElement> systemTableConnections;

    @FindBy(css = "table#a_sites_datatables tr td:nth-child(17)")
    private List<WebElement> systemTableStatus;

    @FindBy(css = "table#a_sites_datatables tr td:nth-child(18)")
    private List<WebElement> systemTableStages;

    @FindBy(name = "id")
    private WebElement systemIDSearchTextField;

    @FindBy(id = "group_id")
    private WebElement systemGroupsDropdown;

    @FindBy(name = "city")
    private WebElement systemCitySearchTextField;

    @FindBy(name = "state")
    private WebElement systemStateSearchTextField;

    @FindBy(name = "installer")
    private WebElement systemInstallerSearchTextField;

    @FindBy(name = "owner")
    private WebElement systemOwnerSearchTextField;

    @FindBy(name = "pcu_count")
    private WebElement systemPcuCountSearchTextField;

    @FindBy(name = "acb_count")
    private WebElement systemAcbCountSearchTextField;

    @FindBy(name = "encharge_count")
    private WebElement systemEnchargeCountSearchTextField;

    @FindBy(name = "enpower_count")
    private WebElement systemEnpowerCountSearchTextField;

    @FindBy(name = "nsr_count")
    private WebElement systemQRelaySearchTextField;

    @FindBy(name = "emu_count")
    private WebElement systemEmuSearchTextField;

    @FindBy(name = "pcu_types")
    private WebElement systemDeviceTypeDropdown;

    @FindBy(name = "envoy_types")
    private WebElement systemEnvoyTypeDropdown;

    @FindBy(name = "connection_type")
    private WebElement systemConnectionDropdown;

    @FindBy(name = "status")
    private WebElement systemStatusDropdown;

    @FindBy(name = "stage")
    private WebElement systemStageDropdown;

    @FindBy(css = "div#data_tools a[href$='refresh_cache']")
    private WebElement systemDataToolRefreshCacheLink;

    @FindBy(css = "div#data_tools a[href$='rollup']")
    private WebElement systemDataToolShowSiteRollupsLink;

    @FindBy(css = "div#data_tools a[href$='analyze']")
    private WebElement systemDataToolAnalyzeMicroinvertersLink;

    @FindBy(css = "div#data_tools a[href$='sfdc_sync']")
    private WebElement systemDataToolSyncWithSFDCLink;

    @FindBy(css = "div#data_tools a[href$='enable_wiring_check']")
    private WebElement systemDataToolEnableWiringIssueDetectionLink;

    @FindBy(css = "div#page_title div.parent_level a")
    private WebElement enlightenManagerSystemName;

    @FindBy(id = "stats_column")
    private WebElement enlightenManagerStatsColumn;

    @FindBy(css = "div.title_head h2")
    private WebElement myEnlightenSystemName;

    @FindBy(xpath = "//span[@class='logo brand' and text()='MyEnlighten']")
    private WebElement myEnlightenLogo;

    @FindBy(xpath = "//div[@id='content']/descendant-or-self::div[text()='Monitor']")
    private WebElement enlightenMobileSplashScreen;

    @FindBy(css = "div#data_tools a")
    private List<WebElement> systemPageDataTools;

    @FindBy(css = "div#a_sites_datatables_wrapper button span")
    private WebElement changeColumnButton;



    /*Page Constants*/


    /*Page Methods*/
    public void clickOnSystemMenuItem(String menuItem){
        boolean flag = false;
        for(WebElement individualElement:systemMenuItems){
            if (individualElement.getText().equalsIgnoreCase(menuItem)){
                individualElement.click();
                flag = true;
                break;
            }
        }
        if(!flag){
            logger.info("Non of the Menu Selected and Menu Item Passed is "+menuItem);
        }
    }

    public void searchSystem(String sheetName, String rowIndex){
        progressHelpers.waitForElementToInvisible(loadingProgressBar);
        progressHelpers.waitForElementToDisplay(systemNameSearchTextField);
        enterSystemName(testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"System Name"));
        clickOnSearchButton();
        progressHelpers.waitForElementToInvisible(loadingProgressBar);
        systemTableSearchItemName.click();
    }

    public void enterSystemName(String systemName){
        generalHelper.enterTextInTextField(systemNameSearchTextField,systemName);
    }

    public void clickOnSearchButton(){
        systemTableSearchButton.click();
    }

    public void verifyAllSystemsAreDisplayed(){
        for(WebElement element:systemTableSearchItemNames){
            Assert.assertTrue(element.getText()!=null && element.getText()!="");
        }
        clickOnPageNationLastButton();
        progressHelpers.waitForElementToInvisible(loadingProgressBar);
        for(WebElement element:systemTableSearchItemNames){
            Assert.assertTrue(element.getText()!=null && element.getText()!="");
        }
    }

    public void clickOnPageNationLastButton(){
        systemTableLastPageButton.click();
    }

    public void searchOnDataTable(String searchText, WebElement searchLocator){
        progressHelpers.waitForElementToInvisible(loadingProgressBar);
        generalHelper.enterTextInTextField(searchLocator,searchText);
        systemTableSearchButton.click();
        progressHelpers.waitForElementToInvisible(loadingProgressBar);
    }

    public void verifySystemTableSortFunctionality(){
        //TBD Optimize the below code
        verifySorting("descending",systemTableIDs);
        clickOnSystemTableHeader("ID");
        verifySorting("ascending",systemTableIDs);
        clickOnSystemTableHeader("City");
        verifySorting("ascending",systemTableCities);
        clickOnSystemTableHeader("City");
        verifySorting("descending",systemTableCities);
        clickOnSystemTableHeader("State");
        verifySorting("ascending",systemTableStates);
        clickOnSystemTableHeader("State");
        verifySorting("descending",systemTableStates);
        clickOnSystemTableHeader("Pcu");
        verifySorting("ascending",systemTablePcus);
        clickOnSystemTableHeader("Pcu");
        verifySorting("descending",systemTablePcus);
        clickOnSystemTableHeader("Acb");
        verifySorting("ascending",systemTableACBs);
        clickOnSystemTableHeader("Acb");
        verifySorting("descending",systemTableACBs);
        clickOnSystemTableHeader("Encharge");
        verifySorting("ascending",systemTableEncharges);
        clickOnSystemTableHeader("Encharge");
        verifySorting("descending",systemTableEncharges);
        clickOnSystemTableHeader("Enpower");
        verifySorting("ascending",systemTableEnpowers);
        clickOnSystemTableHeader("Enpower");
        verifySorting("descending",systemTableEnpowers);
        clickOnSystemTableHeader("Qrelay");
        verifySorting("ascending",systemTableQRelays);
        clickOnSystemTableHeader("Qrelay");
        verifySorting("descending",systemTableQRelays);
        clickOnSystemTableHeader("Emu");
        verifySorting("ascending",systemTableEmus);
        clickOnSystemTableHeader("Emu");
        verifySorting("descending",systemTableEmus);
        clickOnSystemTableHeader("Connection");
        verifySorting("ascending",systemTableConnections);
        clickOnSystemTableHeader("Connection");
        verifySorting("descending",systemTableConnections);
        clickOnSystemTableHeader("Status");
        verifySorting("ascending",systemTableStatus);
        clickOnSystemTableHeader("Status");
        verifySorting("descending",systemTableStatus);
        searchOnDataTable("NAY",systemStateSearchTextField);
        clickOnSystemTableHeader("Name");
        verifySorting("ascending",systemTableNames);
        clickOnSystemTableHeader("Name");
        verifySorting("descending",systemTableNames);

    }

    /**
     * @param headerName
     */
    public void clickOnSystemTableHeader(String headerName){
        progressHelpers.waitForElementToInvisible(loadingProgressBar,Integer.valueOf(PropertyReader.getConfigProperties(System.getProperty("ENV"),"hardwaitinseconds")));
        switch (headerName.toLowerCase()){
            case "id":
                systemTableHeaderID.click();
                break;
            case "name":
                systemTableHeaderName.click();
                break;
            case "city":
                systemTableHeaderCity.click();
                break;
            case "state":
                systemTableHeaderState.click();
                break;
            case "pcu":
                systemTableHeaderPcuCount.click();
                break;
            case "acb":
                systemTableHeaderAcbCount.click();
                break;
            case "encharge":
                systemTableHeaderEnchargeCount.click();
                break;
            case "enpower":
                systemTableHeaderEnpowerCount.click();
                break;
            case "qrelay":
                systemTableHeaderQRelaysCount.click();
                break;
            case "emu":
                systemTableHeaderEmuCount.click();
                break;
            case "connection":
                systemTableHeaderConnection.click();
                break;
            case "status":
                systemTableHeaderStatus.click();
                break;

        }
    }

    /**
     * @param sortOrder
     * @param filterLocator
     */
    public void verifySorting(String sortOrder, List<WebElement> filterLocator){
        progressHelpers.waitForElementToInvisible(loadingProgressBar,Integer.valueOf(PropertyReader.getConfigProperties(System.getProperty("ENV"),"hardwaitinseconds")));
        List<String> actualTableItems = new ArrayList<>();
        List<String> expectedTableItems = new ArrayList<>();
        for(WebElement unitElement:filterLocator){
            actualTableItems.add(unitElement.getText());
            expectedTableItems.add(unitElement.getText());
        }
        if(sortOrder.equalsIgnoreCase("ascending")){
            Arrays.parallelSort(actualTableItems.toArray(new String[actualTableItems.size()]));
            Assert.assertEquals(actualTableItems,expectedTableItems);

        }else if(sortOrder.equalsIgnoreCase("descending")){
            Arrays.parallelSort(actualTableItems.toArray(new String[actualTableItems.size()]),Collections.reverseOrder());
            Assert.assertEquals(actualTableItems,expectedTableItems);
        }

    }

    /**
     * @param sheetName
     * @param rowIndex
     */
    public void verifyAllTheFiltersInTable(String sheetName, String rowIndex){
        verifySystemTableFilter(systemIDSearchTextField,systemTableIDs,testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"System ID"));
        verifySystemTableFilter(systemNameSearchTextField,systemTableNames,testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"System Name"));
        verifySystemTableFilter(systemCitySearchTextField,systemTableCities,testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"City"));
        verifySystemTableFilter(systemStateSearchTextField,systemTableStates,testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"State Filter"));
        verifySystemTableFilter(systemInstallerSearchTextField,systemTableInstallers,testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Installer Name"));
        verifySystemTableFilter(systemOwnerSearchTextField,systemTableOwners,testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Owner"));
        verifySystemTableFilter(systemPcuCountSearchTextField,systemTablePcus,testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Pcu"));
        verifySystemTableFilter(systemAcbCountSearchTextField,systemTableACBs,testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Acb"));
        verifySystemTableFilter(systemEnchargeCountSearchTextField,systemTableEncharges,testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Encharge"));
        verifySystemTableFilter(systemEnpowerCountSearchTextField,systemTableEnpowers,testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Enpower"));
        progressHelpers.hardWait(Integer.valueOf(PropertyReader.getConfigProperties(System.getProperty("ENV"),"hardwait")));
        javascriptHelper.scrollIntoView(systemStageDropdown);
        verifySystemTableFilter(systemQRelaySearchTextField,systemTableQRelays,testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Qrelay"));
        verifySystemTableFilter(systemEmuSearchTextField,systemTableEmus,testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Emu"));
        progressHelpers.hardWait(Integer.valueOf(PropertyReader.getConfigProperties(System.getProperty("ENV"),"hardwait")));
        javascriptHelper.scrollIntoView(systemStageDropdown);
        verifySystemTableFilter(systemDeviceTypeDropdown,systemTableDeviceTypes,testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Device Type"));
        verifySystemTableFilter(systemEnvoyTypeDropdown,systemTableEnvoyTypes,testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Envoy Type"));
        progressHelpers.hardWait(Integer.valueOf(PropertyReader.getConfigProperties(System.getProperty("ENV"),"hardwait")));
        javascriptHelper.scrollIntoView(systemStageDropdown);
        verifySystemTableFilter(systemConnectionDropdown,systemTableConnections,testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Connection"));
        verifySystemTableFilter(systemStatusDropdown,systemTableStatus,testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Status"));
        verifySystemTableFilterForStages(systemStageDropdown,systemTableStages,testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Stage"));


    }

    /**
     * @param filterLocator
     * @param tableRows
     * @param filterText
     */
    public void verifySystemTableFilter(WebElement filterLocator,List<WebElement> tableRows, String filterText){
        progressHelpers.waitForElementToBeClickable(filterLocator);
        if(filterLocator.getTagName().equalsIgnoreCase("Select")){
            dropDownHelper.SelectUsingVisibleValue(filterLocator,filterText);
        }else {
            generalHelper.enterTextInTextField(filterLocator, filterText);
        }
        clickOnSearchButton();
        progressHelpers.waitForElementToInvisible(loadingProgressBar);
        verifyTableData(tableRows,filterText);
        if(filterLocator.getTagName().equalsIgnoreCase("Select")){
            dropDownHelper.SelectUsingVisibleValue(filterLocator,"All");
        }else {
            generalHelper.clearTextField(filterLocator);
        }
        clickOnSearchButton();
    }

    /**
     * @param filterLocator
     * @param tableRows
     * @param filterText
     */
    public void verifySystemTableFilterForStages(WebElement filterLocator,List<WebElement> tableRows, String filterText){
        progressHelpers.waitForElementToBeClickable(filterLocator);
        if(filterLocator.getTagName().equalsIgnoreCase("Select")){
            dropDownHelper.SelectUsingVisibleValue(filterLocator,filterText);
        }else {
            generalHelper.enterTextInTextField(filterLocator, filterText);
        }
        clickOnSearchButton();
        progressHelpers.waitForElementToInvisible(loadingProgressBar);
        verifyTableData(tableRows,filterText.split("[\\-]")[1].trim());
        if(filterLocator.getTagName().equalsIgnoreCase("Select")){
            dropDownHelper.SelectUsingVisibleValue(filterLocator,"All");
        }else {
            generalHelper.clearTextField(filterLocator);
        }
        clickOnSearchButton();
    }

    /**
     * @param tableRows
     * @param expectedText
     */
    public void verifyTableData(List<WebElement> tableRows, String expectedText){
        for (WebElement rowItem: tableRows){
            if(rowItem.getText().contains(",")){
                List<String> multipleItems = Arrays.asList(rowItem.getText().split("[\\,]"));
                String actualItem = null;
                for(String eachItem:multipleItems){
                    if(eachItem.trim().equalsIgnoreCase(expectedText)){
                        actualItem = eachItem.trim();
                    }
                }
                Assert.assertEquals(actualItem,expectedText);
            }
            else {
                Assert.assertEquals(rowItem.getText(),expectedText);
            }
        }
    }

    /**
     * @param sheetName
     * @param rowIndex
     */
    public void verifyEnlightenManagerViewOfSystem(String sheetName,String rowIndex){
        progressHelpers.hardWait(Integer.valueOf(PropertyReader.getConfigProperties(System.getProperty("ENV"),"hardwait")));
        Assert.assertTrue(enlightenManagerStatsColumn.isDisplayed());
        Assert.assertEquals(enlightenManagerSystemName.getText(),testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"System Name"));
    }

    /**
     * @param sheetName
     * @param rowIndex
     */
    public void verifyMyEnlightenViewOfSystem(String sheetName,String rowIndex){
        progressHelpers.hardWait(Integer.valueOf(PropertyReader.getConfigProperties(System.getProperty("ENV"),"hardwait")));
        Assert.assertTrue(myEnlightenLogo.isDisplayed());
        Assert.assertEquals(myEnlightenSystemName.getText(),testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"System Name"));
    }

    public void verifyEnlightenMobileViewOfSystem(){
        progressHelpers.hardWait(Integer.valueOf(PropertyReader.getConfigProperties(System.getProperty("ENV"),"hardwait")));
        Assert.assertTrue(enlightenMobileSplashScreen.isDisplayed());
    }

    /**
     * @param toolName
     */
    public void clickOnSystemDataTool(String toolName){
        boolean flag = false;
        for(WebElement individualElement:systemPageDataTools){
            if (individualElement.getText().equalsIgnoreCase(toolName)){
                individualElement.click();
                flag = true;
                break;
            }
        }
        if(!flag){
            logger.info("Non of the Menu Selected and Menu Item Passed is "+toolName);
        }
    }

}
