package com.enphaseenergy.enlighten.Helpers;

import com.enphaseenergy.enlighten.utils.Data.PropertyReader;
import com.enphaseenergy.enlighten.utils.Driver.DriverManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;

import java.util.ArrayList;

public class GeneralHelper {
    WebDriver driver = DriverManager.getDriver();
    private Logger logger = LoggerHelper.getLogger(GeneralHelper.class);
    ProgressHelpers progressHelpers = new ProgressHelpers();

    /**
     * @param locator
     * @return
     */
    public WebElement getElement(By locator) {
        logger.info(locator);
        if (IsElementPresentQuick(locator))
            return driver.findElement(locator);

        try {
            throw new NoSuchElementException("Element Not Found : " + locator);
        } catch (RuntimeException re) {
            logger.error(re);
            throw re;
        }
    }

    /**
     * Check for element is present based on locator
     * If the element is present return the web element otherwise null
     * @param locator
     * @return WebElement or null
     */

    public WebElement getElementWithNull(By locator) {
        logger.info(locator);
        try {
            return driver.findElement(locator);
        } catch (NoSuchElementException e) {
            // Ignore
        }
        return null;
    }

    /**
     * @param locator
     * @return
     */
    public boolean IsElementPresentQuick(By locator) {
        boolean flag = driver.findElements(locator).size() >= 1;
        logger.info(flag);
        return flag;
    }


    /**
     * @param locator
     * @param text
     */
    public void enterTextInTextField(WebElement locator, String text){
        try {
            locator.click();
            locator.clear();
            locator.sendKeys(text);
        }catch (ElementClickInterceptedException e){
//            Actions actions = new Actions(driver);
            progressHelpers.hardWait(Integer.valueOf(PropertyReader.getConfigProperties("qa2","hardwait")));
            locator.sendKeys(Keys.ESCAPE);
//            actions.sendKeys(Keys.ESCAPE).perform();
            progressHelpers.hardWait(Integer.valueOf(PropertyReader.getConfigProperties("qa2","hardwait")));
            progressHelpers.waitForElementToBeClickable(locator);
            locator.click();
            locator.clear();
            locator.sendKeys(text);
        }
    }

    /**
     * To be done
     * @param actual
     * @param expected
     */
    public void assertActualExpected(ArrayList<String> actual,ArrayList<String> expected){
        Assert.assertEquals(actual, expected);
    }


    /**
     * @param locator
     */
    public void clearTextField(WebElement locator){
        locator.click();
        locator.clear();
    }

    /**
     * @param element
     */
    public void moveToElement(WebElement element, WebDriver driver){
        Actions actions = new Actions(driver);
        actions.moveToElement(element);
        actions.perform();
        actions.build();
    }
}
