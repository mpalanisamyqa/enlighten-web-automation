package com.enphaseenergy.enlighten.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class ProductionPage implements BaseObjects{
    public ProductionPage(WebDriver driver){
        PageFactory.initElements(driver,this);
    }

    /*Objects */
    @FindBy(xpath = "//a[@class='production-menu']")
    private WebElement productionTab;

    @FindBy(xpath = "//a[@class='module hours graph production']")
    private WebElement productionGraphSection;

    /*Methods*/
    public void clickOnProductionTab(){
//        progressHelpers.waitForElementToDisplay(productionTab);
//        buttonHelper.click(productionTab);
        javascriptHelper.clickOnElement(productionTab);
    }

    public void verifyProductionTabSelected(){
        Assert.assertTrue(productionGraphSection.isDisplayed());
    }
}
