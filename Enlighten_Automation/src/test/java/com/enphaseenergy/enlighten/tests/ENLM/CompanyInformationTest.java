package com.enphaseenergy.enlighten.tests.ENLM;

import com.enphaseenergy.enlighten.tests.BaseTest;
import com.enphaseenergy.enlighten.utils.Report.ExtentTestManager;
import com.relevantcodes.extentreports.LogStatus;
import org.testng.annotations.Test;

import java.lang.reflect.Method;

/**
 * @author mnpalanisamy
 */
public class CompanyInformationTest extends BaseTest {

    /*Below scripts are from BOC-2157*/
    @Test(groups = {"sanity","qa2"})
    public void verifyUpdateCompanyInformationToOriginalDetails(Method method){
        ExtentTestManager.startTest(method.getName(),"verify Update Company Information to Original details with Installer Role");
        ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId"+ Thread.currentThread().getId());
        getSignInPageObject().verifySignInPage();
        getSignInPageObject().loginAs("INSTALLER_AUT_1");
        getDashboardPageObject().verifyDashboardPage();
        getDashboardPageObject().clickOnMenu("Account");
        getAccountPageObject().selectTheLeftMenuOption("CompanyInformation");
        getCompanyInformationPageObject().updateCompanyInformation("CompanyInformation","1");
        getCompanyInformationPageObject().verifyCompanyInformationSavedSuccessfully();
    }

    @Test(groups = {"sanity","qa2"}, dependsOnMethods = "verifyUpdateCompanyInformationToOriginalDetails")
    public void verifyReadCompanyInformation(Method method){
        ExtentTestManager.startTest(method.getName(),"verify Read Company Information with Installer Role");
        ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId"+ Thread.currentThread().getId());
        getSignInPageObject().verifySignInPage();
        getSignInPageObject().loginAs("INSTALLER_AUT_1");
        getDashboardPageObject().verifyDashboardPage();
        getDashboardPageObject().clickOnMenu("Account");
        getAccountPageObject().selectTheLeftMenuOption("CompanyInformation");
        getCompanyInformationPageObject().readCompanyInformation("CompanyInformation","1");
    }

    @Test(groups = {"sanity","qa2"}, dependsOnMethods = "verifyReadCompanyInformation")
    public void verifyUpdateCompanyInformation(Method method){
        ExtentTestManager.startTest(method.getName(),"verify Update Company Information with Installer Role");
        ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId"+ Thread.currentThread().getId());
        getSignInPageObject().verifySignInPage();
        getSignInPageObject().loginAs("INSTALLER_AUT_1");
        getDashboardPageObject().verifyDashboardPage();
        getDashboardPageObject().clickOnMenu("Account");
        getAccountPageObject().selectTheLeftMenuOption("CompanyInformation");
        getCompanyInformationPageObject().updateCompanyInformation("CompanyInformation","2");
        getCompanyInformationPageObject().verifyCompanyInformationSavedSuccessfully();
        getCompanyInformationPageObject().readCompanyInformation("CompanyInformation","2");
    }


}
