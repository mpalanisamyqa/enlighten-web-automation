package com.enphaseenergy.enlighten.Helpers;

import com.enphaseenergy.enlighten.utils.Data.PropertyReader;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class CalendarHelper {

    Logger logger = LoggerHelper.getLogger(CalendarHelper.class);

    private final String enlightenDateTimeFormat = "E, MMM dd, yyyy";
    private final String enlightenMonthAndYearFormatInPicker = "MMMM yyyy";

    /*Method to get all the dates are displayed in the date picker*/
    public List<String> getAllDatesFromCalendar(WebElement tableElement){
        List<WebElement> listOfWebElements = tableElement.findElements(By.cssSelector("td"));
        List<String> listOfElements = new ArrayList<>();
        for(WebElement s : listOfWebElements){
            listOfElements.add(s.getText());
        }
        return listOfElements;
    }


    /*Method to get all the enabled dates are displayed in the date picker*/
    public List<String> getAllEnabledDatesFromCalendar(WebElement tableElement){
        List<WebElement> listOfWebElements = tableElement.findElements(By.cssSelector("td[class=day]"));
        List<String> listOfElements = new ArrayList<>();
        for(WebElement s : listOfWebElements){
            listOfElements.add(s.getText());
        }
        listOfElements.add(tableElement.findElement(By.cssSelector("td[class='active day']")).getText());
        return listOfElements;
    }

    /*Method to get all the disabled dates are displayed in the date picker*/
    public List<String> getAllDisabledDatesFromCalendar(List<WebElement> datePickerDisableDates){

        List<WebElement> listOfWebElements = datePickerDisableDates;
        List<String> listOfElements = new ArrayList<>();
        for(WebElement s : listOfWebElements){
            listOfElements.add(s.getText());
        }
        return listOfElements;
    }

    /*Method to get all the disabled dates using site timezone*/
    public List<String> getAllDisabledDatesFromCalendar(String siteName){
        String timeZone = PropertyReader.getSiteProperties(siteName,"timezone");
        LocalDate currentDate = getDateFromTimeZone(timeZone);
        int currentDay = currentDate.getDayOfMonth();
        int lenghtOfMonth = currentDate.lengthOfMonth();
        List<String> listOfElements = new ArrayList<>();
        for(int i=currentDay+1; i<=lenghtOfMonth;i++){
            listOfElements.add(String.valueOf(i));
        }
        return listOfElements;
    }

    /*Convert the string into Dateformat*/
    public LocalDate getDateFromString(String date){
        DateTimeFormatter format = DateTimeFormatter.ofPattern(enlightenDateTimeFormat);
        LocalDate dateString= LocalDate.parse(date,format);
        return dateString;
    }

    /**
     * @param date
     * @param datePattern
     * @return
     */
    public LocalDate getDateFromString(String date,String datePattern){
        DateTimeFormatter format = DateTimeFormatter.ofPattern(datePattern);
        LocalDate dateString= LocalDate.parse(date,format);
        return dateString;
    }

    /**
     * @param datePattern
     * @return
     */
    public LocalDate getTodayDate(String datePattern){
        DateTimeFormatter format = DateTimeFormatter.ofPattern(datePattern);
        LocalDate date = LocalDate.now();
        String dateString = date.format(format);
        LocalDate localDate = LocalDate.parse(dateString, DateTimeFormatter.ofPattern(datePattern));
        return localDate;
    }

    public int getIntegerValueOfMonth(String monthAndYear){
        DateTimeFormatter format = DateTimeFormatter.ofPattern(enlightenMonthAndYearFormatInPicker);
        YearMonth dateString = YearMonth.parse(monthAndYear,format);
        int monthValue = dateString.getMonthValue();
        return monthValue;
    }

    public LocalDate getDateFromTimeZone(String timeZone){
        DateTimeFormatter format = DateTimeFormatter.ofPattern(enlightenDateTimeFormat);
        LocalDate localDate = LocalDate.now(ZoneId.of(timeZone));
        LocalDate dateString = LocalDate.parse(localDate.format(format),format);
        return dateString;
    }
}


