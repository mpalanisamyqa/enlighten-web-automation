package com.enphaseenergy.enlighten.Helpers;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author mnpalanisamy
 */
public class AssertHelper {
    Logger logger = LoggerHelper.getLogger(CalendarHelper.class);

    /**
     * @param expectedValue
     * @param actualListOfItems
     */
    public void assertElementInList(String expectedValue, List<WebElement> actualListOfItems){
        String expectedOwner = expectedValue;
        Set<String> allUsers = new HashSet<>();
        for (WebElement individualItem:actualListOfItems){
            allUsers.add(individualItem.getText());
        }
        Assert.assertTrue(allUsers.contains(expectedOwner));
    }

    /**
     * @param actualValue
     * @param expectedValue
     *
     */
    public void compareTwoValues(Object actualValue, Object expectedValue){
        try{
            Assert.assertEquals(actualValue,expectedValue);
        }catch (Throwable throwable){
            logger.info(actualValue+" is not matching with "+expectedValue);
        }
    }

    /**
     * @param actualValue
     * @param expectedValue
     *
     */
    public void compareTwoValues(Object[] actualValue, Object[] expectedValue){
        try{

            Assert.assertEquals(actualValue,expectedValue);
        }catch (Throwable throwable){
            logger.info(actualValue+" is not matching with "+expectedValue);
        }
    }

    /**
     * @param objectList
     */
    public void assertListOfElementsExistsInPage(List<WebElement> objectList){
        for(WebElement individualElement: objectList){
            try{
                Assert.assertTrue(individualElement.isDisplayed());
            }catch (Throwable throwable){
                logger.info("Expected element "+individualElement+" is not displayed");
            }
        }
    }

    /**
     * @param objectList
     */
    public void assertListOfElementsIsNotEmpty(List<WebElement> objectList){
        for(WebElement item:objectList){
            Assert.assertTrue((item.getText()!=""&&item.getText()!=null));
        }
    }


    
    public void assertElementsExistsInPage(WebElement element){
            try{
                Assert.assertTrue(element.isDisplayed());
            }catch (Throwable throwable){
                logger.info("Expected element "+element+" is not displayed");
            }
       
    }
    
    
}
