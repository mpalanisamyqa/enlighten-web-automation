package com.enphaseenergy.enlighten.Helpers;

import com.enphaseenergy.enlighten.utils.Data.TestDataReader;

import java.io.File;
import java.util.HashMap;
import java.util.List;

public class TestDataHelper extends TestDataReader {

    public String getDataFromDataSheet(String sheetName, String rowIndex, String columnName) {
        List<HashMap<String, String>> datamap;
        datamap = getData(System.getProperty("user.dir") + File.separator + "src/test/resources/test_data/" + System.getProperty("ENV") +File.separator+ "inputdata.xlsx", sheetName);
        int index = Integer.parseInt(rowIndex) - 1;
        String value = datamap.get(index).get(columnName);
        return value;
    }

}
