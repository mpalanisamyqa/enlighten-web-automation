package com.enphaseenergy.enlighten.utils.Data;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import org.ini4j.Ini;
import org.ini4j.Profile.Section;

public class PropertyReader {

    /**
     * @param env
     * @param fileName
     * @param sectype
     * @return
     * @throws IOException
     */
    public static Section iniLoader(String env, String fileName, String sectype) throws IOException {
        Ini ini = new Ini();
        FileInputStream iniConfigFile = null;
        iniConfigFile = new FileInputStream(
                System.getProperty("user.dir") + File.separator +"properties" +File.separator+ env + File.separator + fileName + ".properties");
        ini.load(iniConfigFile);
        Section section = ini.get(sectype);
        return section;
    }

    /**
     * @param fileName
     * @param sectype
     * @return
     * @throws IOException
     */
    public static Section iniLoader(String fileName, String sectype) throws IOException {
        Ini ini = new Ini();
        FileInputStream iniConfigFile = null;
        iniConfigFile = new FileInputStream(
                System.getProperty("user.dir") + File.separator +"properties" + File.separator + fileName + ".properties");
        ini.load(iniConfigFile);
        Section section = ini.get(sectype);
        return section;
    }


    public static String getUserProperties(String secntype, String val) {
        String value = null;
        try {
            Section section = iniLoader(System.getProperty("ENV"),"user",secntype.toUpperCase());
            value = section.get(val);
        } catch (Exception e) {
            System.out.println(e);
        }
        return value;
    }

    public static String getConfigProperties(String secntype, String val) {
        String value = null;
        try {
            Section section = iniLoader("config",secntype.toUpperCase());
            value = section.get(val);
        } catch (Exception e) {
            System.out.println(e);
        }
        return value;
    }

    public static String getSiteProperties(String secntype, String val) {
        String value = null;
        try {
            Section section = iniLoader( "site",secntype.toUpperCase());
            value = section.get(val);
        } catch (Exception e) {
            System.out.println(e);
        }
        return value;
    }
    public static String getownershipsiteProperties(String secntype, String val) {
        String value = null;
        try {
            Section section = iniLoader( "ownershipsite",secntype.toUpperCase());
            value = section.get(val);
        } catch (Exception e) {
            System.out.println(e);
        }
        return value;
    }
   
}
