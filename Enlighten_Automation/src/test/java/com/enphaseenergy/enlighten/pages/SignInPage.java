package com.enphaseenergy.enlighten.pages;


import com.enphaseenergy.enlighten.utils.Data.PropertyReader;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class SignInPage {

    public SignInPage(WebDriver driver){
        PageFactory.initElements(driver,this);
    }

    /*Object of SignIn Page*/

    @FindBy (how = How.ID, using = "user_email")
    private WebElement emailField;

    @FindBy (how = How.ID, using = "user_password")
    private WebElement passwordField;

    @FindBy(how = How.ID, using = "submit")
    private WebElement signInButton;

    /*Page Methods*/

    public void verifySignInPage(){
        Assert.assertTrue(emailField.isDisplayed());
        Assert.assertTrue(passwordField.isDisplayed());
        Assert.assertTrue(signInButton.isDisplayed());
    }

    public void enterUserCredential(String username, String password){
        emailField.sendKeys(username);
        passwordField.sendKeys(password);
    }

    public void clickOnSiginInButton(){
        signInButton.click();
    }

    public void loginAs(String userType){
        String email = PropertyReader.getUserProperties(userType,"email");
        String password = PropertyReader.getUserProperties(userType,"password");
        enterUserCredential(email,password);
        clickOnSiginInButton();

    }
}
