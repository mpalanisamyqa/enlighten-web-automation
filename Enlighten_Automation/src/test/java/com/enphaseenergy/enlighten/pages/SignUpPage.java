package com.enphaseenergy.enlighten.pages;


import java.net.HttpURLConnection;
import java.net.URL;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;



public class SignUpPage  implements BaseObjects  {

	/**@author kulanjirajanp
	 *	
	 */
	public SignUpPage(WebDriver driver){
		PageFactory.initElements(driver,this);
	}

	@FindBy(xpath = "//div[@id='new_user_form']/h1")
	private WebElement newUserRegistrationText;

	@FindBy(id = "user_first_name")
	private WebElement firstNameTextBox;

	@FindBy(xpath = "//input[@id='user_first_name']/..//label")
	private WebElement firstNameText;

	@FindBy(xpath = "//input[@id='user_first_name']/..//em")
	private WebElement firstNameMandatoryStart;

	@FindBy(id = "user_last_name")
	private WebElement lastNameTextBox;

	@FindBy(xpath = "//input[@id='user_last_name']/..//label")
	private WebElement lastNameText;

	@FindBy(xpath = "//input[@id='user_last_name']/..//em")
	private WebElement lastNameMandatoryStart;

	@FindBy(id ="user_country")
	private WebElement userCountryDropdown;

	@FindBy(xpath = "//select[@id='user_country']/..//label")
	private WebElement countryText;

	@FindBy(xpath = "//select[@id='user_country']/..//em")
	private WebElement countryMandatoryStart;

	
	@FindBy(id = "user_email")
	private WebElement emailTextBox;

	@FindBy(xpath = "//input[@id='user_email']/..//label[@for='user_email']")
	private WebElement emailText;

	@FindBy(xpath = "//input[@id='user_email']/..//em")
	private WebElement emailMandatoryStart;



	@FindBy(id = "user_phone")
	private WebElement phoneTextBox;

	@FindBy(xpath = "//input[@id='user_phone']/..//label[@for='user_phone']")
	private WebElement phoneText;

	@FindBy(xpath = "//input[@id='user_phone']/..//em")
	private WebElement phoneMandatoryStart;

	@FindBy(xpath = "//input[@id='user_self_installer_false']")
	private WebElement professionalRegisterCheckbox;

	@FindBy(xpath = "//input[@id='user_self_installer_false']/following-sibling::label[@for='user_self_installer_false']")
	private WebElement professionalRegisterText;


	/////////Error Message///////

	@FindBy(xpath = "//label[@id='user_first_name-error']")
	private WebElement firstNameErrorMsg;

	@FindBy(id = "user_last_name-error")
	private WebElement lastNameErrorMsg;

	@FindBy(id = "user_email-error")
	private WebElement EmailErrorMsg;

	@FindBy(id = "user_phone-error")
	private WebElement phoneErrorMsg;

	@FindBy(id = "privacy_policy_assent-error")
	private WebElement privacy_policyErrorMsg;

	////Company////////

	@FindBy(id = "company_name-error")
	private WebElement companyNameErrorMsg;

	@FindBy(id = "company_address_attributes_address1-error")
	private WebElement address1ErrorMsg;

	@FindBy(id = "company_address_attributes_city-error")
	private WebElement cityErrorMsg;

	@FindBy(id = "company_address_attributes_zip-error")
	private WebElement ZipErrorMsg;

	@FindBy(id = "company_support_email-error")
	private WebElement support_emailErrorMsg;

	@FindBy(id = "company_support_phone-error")
	private WebElement support_phoneErrorMsg; 

	//	 ===================company===================

	@FindBy(id = "company_name")
	private WebElement companyTextBox;

	@FindBy(xpath = "//input[@id='company_name']/..//label[@for='company_name']")
	private WebElement companyText;

	@FindBy(xpath = "//input[@id='company_name']/..//em")
	private WebElement companyMandatoryStart;

	@FindBy(id = "company_address_attributes_address1")
	private WebElement streetAddress1TextBox;

	@FindBy(xpath = "//input[@id='company_address_attributes_address1']/..//label[@for='company_address_attributes_address1']")
	private WebElement streetAddress1Text;

	@FindBy(xpath = "//input[@id='company_address_attributes_address1']/..//em")
	private WebElement streetAddress1MandatoryStart;

	@FindBy(id = "company_address_attributes_address2")
	private WebElement streetAddress2TextBox;

	@FindBy(xpath = "//input[@id='company_address_attributes_address2']/..//label[@for='company_address_attributes_address2']")
	private WebElement streetAddress2Text;

	@FindBy(xpath = "//input[@id='company_address_attributes_address2']/..//label")
	private WebElement streetAddress2MandatoryStart;

	@FindBy(id = "company_address_attributes_city")
	private WebElement cityTextBox;

	@FindBy(xpath = "//input[@id='company_address_attributes_city']/..//label[@for='company_address_attributes_city']")
	private WebElement cityText;

	@FindBy(xpath = "//input[@id='company_address_attributes_city']/..//em")
	private WebElement cityMandatoryStart;

	@FindBy(id = "company_address_attributes_country")
	private WebElement companyCountryDrodown;

	@FindBy(xpath = "//select[@id='company_address_attributes_country']/..//label[@for='company_address_attributes_country']")
	private WebElement companyCountryText;

	@FindBy(xpath = "//select[@id='company_address_attributes_country']/..//em")
	private WebElement companyCountryMandatoryStart;

	@FindBy(id = "company_address_attributes_state")
	private WebElement companyStateDrodown;

	@FindBy(xpath = "//select[@id='company_address_attributes_state']/..//label[@for='company_address_attributes_state']")
	private WebElement companystateText;

	@FindBy(xpath = "//select[@id='company_address_attributes_state']/..//em")
	private WebElement companystateMandatoryStart;

	@FindBy(id = "company_address_attributes_zip")
	private WebElement companyZipTextBox;

	@FindBy(xpath = "//input[@id='company_address_attributes_zip']/..//label[@for='company_address_attributes_zip']")
	private WebElement companyZipText;

	@FindBy(xpath = "//input[@id='company_address_attributes_zip']/..//em")
	private WebElement companyZipMandatoryStart;

	@FindBy(id = "company_support_email")
	private WebElement companySupportEmailTextBox;

	@FindBy(xpath = "//input[@id='company_support_email']/..//label[@for='company_support_email']")
	private WebElement companySupportEmailText;

	@FindBy(xpath = "//input[@id='company_support_email']/..//em")
	private WebElement companySupportEmailMandatoryStart;

	@FindBy(id = "company_support_phone")
	private WebElement companySupportPhoneTextBox;

	@FindBy(xpath = "//input[@id='company_support_phone']/..//label[@for='company_support_phone']")
	private WebElement companySupportPhoneText;

	@FindBy(xpath = "//input[@id='company_support_phone']/..//em")
	private WebElement companySupportPhoneMandatoryStart;

	@FindBy(id = "company_url")
	private WebElement companyUrlTextBox;

	@FindBy(xpath = "//input[@id='company_url']/..//label[@for='company_url']")
	private WebElement companyUrlText;

	@FindBy(xpath = "//input[@id='company_url']/..//em")
	private WebElement companyUrlMandatoryStart;

	@FindBy(id = "company_logo_img")
	private WebElement companyLogo;

	@FindBy(xpath = "//input[@id='company_logo_img']/..//label[@for='company_url']")
	private WebElement companyLogoText;


	@FindBy(xpath = "//input[@id='company_logo_img']/following-sibling::p")
	private WebElement companyLogoFormat;

	@FindBy(xpath = "//input[@id='user_self_installer_true']")
	private WebElement self_installerCheckbox;


	@FindBy(xpath = "//input[@id='user_self_installer_true']/following-sibling::label[@for='user_self_installer_true']")
	private WebElement self_installerText;

	@FindBy(xpath = "//input[@id='user_self_installer_na']")
	private WebElement otherCheckbox;

	@FindBy(xpath = "//input[@id='user_self_installer_na']/following-sibling::label[@for='user_self_installer_na']")
	private WebElement otherText;

	@FindBy(xpath = "//input[@id='privacy_policy_assent']")
	private WebElement privacy_policyCheckbox;

	@FindBy(xpath = "//input[@id='privacy_policy_assent']/../label")
	private WebElement agreeText;

	@FindBy(xpath = "//input[@id='privacy_policy_assent']/..//a[@href='http://enphase.com/privacy']")
	private WebElement privacyLink;

	@FindBy(xpath = "//input[@id='privacy_policy_assent']/..//a[@href='http://enphase.com/terms']")
	private WebElement termOfServiceLink;

	@FindBy(id= "rc-anchor-container")
	private WebElement recaptcha;

	@FindBy(xpath = "//iframe[@role='presentation']")
	private WebElement recaptchaFrame;

	@FindBy(id = "save")
	private WebElement submitbutton;

	@FindBy(xpath = "//a[@href=\"/users/new\"]")
	private WebElement	 signInButton;

	@FindBy(xpath = "//div[contains(text(),'reCAPTCHA verification failed, please try again.')]")
	private WebElement reCaptcherrorMsg;
	
	@FindBy(name = "company[logo_img]")
	private WebElement linkUpload;
	
	
	
	private static final String SIGNUPNEWUSERREGISTRATION= "New User Registration";
	private static final String SIGNUPFIRSTNAME= "First Name";
	private static final String SIGNUPLASTNAME= "Last Name";
	private static final String SIGNUPCOUNTRY1= "Country";	
	private static final String SIGNUPEMAIL= "Email";
	private static final String SIGNUPPHONE= "Phone";
	
	
	private static final String SIGNUPCOMPANYNAME= "Company Name";
	private static final String SIGNUPSTREETADDRESS= "Street Address";
	private static final String SIGNUPSTREETADDRESS2= "Street Address 2";
	private static final String SIGNUPCITY= "City";	
	private static final String SIGNUPCOUNTRY= "Country";
	private static final String SIGNUPSTATEPROVINCE= "State/Province";
	private static final String SIGNUPZIPPOSTALCODE= "Zip/Postal Code";
	private static final String SIGNUPCUSTOMERSUPPORTEMAIL= "Customer Support Email";
	private static final String SIGNUPCUSTOMERSUPPORTPHONE= "Customer Support Phone";
	private static final String SIGNUPWEBSITEURL= "Website URL";
	private static final String SIGNUPCOMPANYLOGO= "Company Logo";
	private static final String SIGNUPJPEG= "JPEG";
	
	private static final String TEXTOTHER= "Other";
	private static final String SELFINSTALLERTEXT= "I am a do-it-yourselfer installing Enphase products on my own home or business";
	private static final String PROFESSIONALREGISTERTEXT="I am a solar professional registering on behalf of my company";
	
	private static final String SIGNUPERRORMSG= "This field is required.";
	private static final String SIGNUPTERMERRORMSG="If you do not agree to the terms of Enphase’s Privacy Policy and Terms of Service you may not register for Enlighten.";
	

	public void clickSignbtn() {
		signInButton.click();
	}

	public  static void verifyIfLinkBroken(WebElement element) throws Throwable
	{

		String str = element.getAttribute("href");
		try{
			Assert.assertTrue(isLinkBroken(new URL(str)).equalsIgnoreCase("ok"),"");

		}catch(Exception ex)
		{
			System.out.println(("Exception occured: "+ex.getMessage()));
		}
	}

	public static String isLinkBroken(URL url) throws Throwable{
		String response = "";
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		try {
			connection.connect();
			response = connection.getResponseMessage();
			connection.disconnect();
			return response;
		}
		catch(Exception exp) {
			return exp.getMessage();
		}
	}
	
	
	public void selectHomeOwner() {
		assertHelper.assertElementsExistsInPage(otherCheckbox);
		assertHelper.compareTwoValues(otherText.getText(),TEXTOTHER);
		checkBoxAndRadioButtonHelper.selectCheckBox(otherCheckbox);

	}
	public void  selectSelfInstaller() {
		assertHelper.assertElementsExistsInPage(self_installerCheckbox);
		assertHelper.compareTwoValues(self_installerText.getText(),SELFINSTALLERTEXT);
		checkBoxAndRadioButtonHelper.selectCheckBox(self_installerCheckbox);
	}

	public void selectSolarProfessional() {
		assertHelper.assertElementsExistsInPage(professionalRegisterCheckbox); 
		assertHelper.compareTwoValues(professionalRegisterText.getText(),PROFESSIONALREGISTERTEXT); 
		checkBoxAndRadioButtonHelper.selectCheckBox(professionalRegisterCheckbox);
	}

	public void signupHomeOwnerSelfInstaller( String SheetName ,String rowindex){
		generalHelper.enterTextInTextField(firstNameTextBox ,testDataHelper.getDataFromDataSheet(SheetName, rowindex, "firstName"));
		generalHelper.enterTextInTextField(lastNameTextBox , testDataHelper.getDataFromDataSheet(SheetName, rowindex, "lastName"));
		generalHelper.enterTextInTextField(emailTextBox ,testDataHelper.getDataFromDataSheet(SheetName, rowindex, "email"));
		generalHelper.enterTextInTextField(phoneTextBox,testDataHelper.getDataFromDataSheet(SheetName, rowindex, "phone"));        
		dropDownHelper.SelectUsingVisibleValue(userCountryDropdown, testDataHelper.getDataFromDataSheet(SheetName, rowindex, "country"));
		checkBoxAndRadioButtonHelper.selectCheckBox(privacy_policyCheckbox);
		submitbutton.click();
		Assert.assertTrue(reCaptcherrorMsg.getText().contains("reCAPTCHA verification failed, please try again."),reCaptcherrorMsg.getText()+"Text not found");
	}
	public void signupSolarProfessional(String filepath,String SheetName ,String rowindex) {
		generalHelper.enterTextInTextField(firstNameTextBox,testDataHelper.getDataFromDataSheet(SheetName, rowindex, "firstName"));
		generalHelper.enterTextInTextField(lastNameTextBox,testDataHelper.getDataFromDataSheet(SheetName, rowindex, "lastName"));
		dropDownHelper.SelectUsingVisibleValue(userCountryDropdown, testDataHelper.getDataFromDataSheet(SheetName, rowindex, "country"));
		generalHelper.enterTextInTextField(emailTextBox,testDataHelper.getDataFromDataSheet(SheetName, rowindex, "email"));
		generalHelper.enterTextInTextField(phoneTextBox,testDataHelper.getDataFromDataSheet(SheetName, rowindex, "phone"));
		generalHelper.enterTextInTextField(companyTextBox,testDataHelper.getDataFromDataSheet(SheetName, rowindex, "companyName"));
		generalHelper.enterTextInTextField(streetAddress1TextBox ,testDataHelper.getDataFromDataSheet(SheetName, rowindex, "address1"));
		generalHelper.enterTextInTextField(streetAddress2TextBox,testDataHelper.getDataFromDataSheet(SheetName, rowindex, "address2"));
		generalHelper.enterTextInTextField(cityTextBox ,testDataHelper.getDataFromDataSheet(SheetName, rowindex, "companyCity"));
		dropDownHelper.SelectUsingVisibleValue(companyCountryDrodown, testDataHelper.getDataFromDataSheet(SheetName, rowindex, "companycountry"));
		generalHelper.enterTextInTextField(companyZipTextBox ,testDataHelper.getDataFromDataSheet(SheetName, rowindex, "zipCode"));
		dropDownHelper.SelectUsingVisibleValue(companyStateDrodown,testDataHelper.getDataFromDataSheet(SheetName, rowindex, "state"));
		generalHelper.enterTextInTextField(companySupportEmailTextBox,testDataHelper.getDataFromDataSheet(SheetName, rowindex, "customersupportemail"));
		generalHelper.enterTextInTextField(companySupportPhoneTextBox,testDataHelper.getDataFromDataSheet(SheetName, rowindex, "customersupportphone"));
		generalHelper.enterTextInTextField(companyUrlTextBox,testDataHelper.getDataFromDataSheet(SheetName, rowindex, "websiteurl"));
		checkBoxAndRadioButtonHelper.selectCheckBox(privacy_policyCheckbox);
		linkUpload.sendKeys(filepath);
		submitbutton.click();
		Assert.assertTrue(reCaptcherrorMsg.getText().contains("reCAPTCHA verification failed, please try again."),reCaptcherrorMsg.getText()+"Text not found");     
	}	
	public  void VerifyHomeInstallerSignUpPage() throws Throwable { 
		assertHelper.compareTwoValues(newUserRegistrationText.getText(),SIGNUPNEWUSERREGISTRATION);
		assertHelper.compareTwoValues(firstNameText.getText(),SIGNUPFIRSTNAME);
		assertHelper.compareTwoValues(lastNameText.getText(),SIGNUPLASTNAME);
		assertHelper.compareTwoValues(countryText.getText(),SIGNUPCOUNTRY1);
		assertHelper.compareTwoValues(emailText.getText(),SIGNUPEMAIL);
		assertHelper.compareTwoValues(phoneText.getText(),SIGNUPPHONE);
		assertHelper.assertElementsExistsInPage(privacy_policyCheckbox);
		assertHelper.assertElementsExistsInPage(firstNameMandatoryStart);
		assertHelper.assertElementsExistsInPage(lastNameMandatoryStart);
		assertHelper.assertElementsExistsInPage(countryMandatoryStart);
		assertHelper.assertElementsExistsInPage(emailMandatoryStart);
		assertHelper.assertElementsExistsInPage(phoneMandatoryStart);
		

	}	 
		
	public void verifyProfessionalRegisterpage() throws Throwable  
	{
		VerifyHomeInstallerSignUpPage();
		assertHelper.compareTwoValues(companyText.getText(),SIGNUPCOMPANYNAME);
		assertHelper.compareTwoValues(streetAddress1Text.getText(),SIGNUPSTREETADDRESS);
		assertHelper.compareTwoValues(streetAddress2Text.getText(),SIGNUPSTREETADDRESS2);
		assertHelper.compareTwoValues(cityText.getText(),SIGNUPCITY);
		assertHelper.compareTwoValues(companyCountryText.getText(),SIGNUPCOUNTRY);
		assertHelper.compareTwoValues(companystateText.getText(),SIGNUPSTATEPROVINCE);
		assertHelper.compareTwoValues(companyZipText.getText(),SIGNUPZIPPOSTALCODE);
		assertHelper.compareTwoValues(companySupportEmailText.getText(),SIGNUPCUSTOMERSUPPORTEMAIL);
		assertHelper.compareTwoValues(companySupportPhoneText.getText(),SIGNUPCUSTOMERSUPPORTPHONE);
		assertHelper.compareTwoValues(companyUrlText.getText(),SIGNUPWEBSITEURL);
		assertHelper.compareTwoValues(companyLogoText.getText(),SIGNUPCOMPANYLOGO);
		assertHelper.compareTwoValues(companyLogoFormat.getText(),SIGNUPJPEG);
		assertHelper.assertElementsExistsInPage(companyMandatoryStart);
		assertHelper.assertElementsExistsInPage(streetAddress1MandatoryStart);
		assertHelper.assertElementsExistsInPage(cityMandatoryStart);
		assertHelper.assertElementsExistsInPage(companyCountryMandatoryStart); 
		assertHelper.assertElementsExistsInPage(companystateMandatoryStart);
		assertHelper.assertElementsExistsInPage(companyZipMandatoryStart);
		assertHelper.assertElementsExistsInPage(companySupportEmailMandatoryStart);
		assertHelper.assertElementsExistsInPage(companySupportPhoneMandatoryStart);
		assertHelper.assertElementsExistsInPage(companyMandatoryStart); 
		assertHelper.assertElementsExistsInPage(companyMandatoryStart);
		assertHelper.assertElementsExistsInPage(companyMandatoryStart);
		assertHelper.assertElementsExistsInPage(companyMandatoryStart);
		assertHelper.assertElementsExistsInPage(companyMandatoryStart);
		assertHelper.assertElementsExistsInPage(companyMandatoryStart);
			 
	}
	public void Verify_Home_Installer_ErrorMessage()
	{
		submitbutton.click();
		assertHelper.compareTwoValues( firstNameErrorMsg.getText(),SIGNUPERRORMSG);
		assertHelper.compareTwoValues( lastNameErrorMsg.getText(),SIGNUPERRORMSG);
		assertHelper.compareTwoValues( EmailErrorMsg.getText(),SIGNUPERRORMSG);
		assertHelper.compareTwoValues(phoneErrorMsg.getText(),SIGNUPERRORMSG);
		assertHelper.compareTwoValues( privacy_policyErrorMsg.getText(),SIGNUPTERMERRORMSG);	 
	}
	public void verifyProfessionalRegisterErrorMessage() 
	{
		Verify_Home_Installer_ErrorMessage();
		assertHelper.compareTwoValues(companyNameErrorMsg.getText(),SIGNUPERRORMSG);	 	 
		assertHelper.compareTwoValues( address1ErrorMsg.getText(),SIGNUPERRORMSG); 
		assertHelper.compareTwoValues(cityErrorMsg.getText(),SIGNUPERRORMSG);
		assertHelper.compareTwoValues( ZipErrorMsg.getText(),SIGNUPERRORMSG);
		assertHelper.compareTwoValues(support_emailErrorMsg.getText(),SIGNUPERRORMSG);
		assertHelper.compareTwoValues( support_phoneErrorMsg.getText(),SIGNUPERRORMSG);
	}

}






