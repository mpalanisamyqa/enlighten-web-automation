package com.enphaseenergy.enlighten.tests;

import com.enphaseenergy.enlighten.utils.Report.ExtentTestManager;
import com.relevantcodes.extentreports.LogStatus;
import org.testng.annotations.*;

import java.lang.reflect.Method;

public class SmokeTestCases extends BaseTest{

    @Test(priority = 1, groups = {"smoke"})
    public void verifyLoginAsHomeOwner(Method method){
        ExtentTestManager.startTest(method.getName(),"Verify Home Owner Login");
        ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId"+ Thread.currentThread().getId());
        getSignInPageObject().verifySignInPage();
        getSignInPageObject().loginAs("HomeOwner");
        getOverviewPageObject().verifyMyEnlightenLandingPage();
    }

    @Test(priority = 2, groups = {"smoke"})
    public void verifyTopNavigationBar(Method method){
        ExtentTestManager.startTest(method.getName(),"Verify Top Navigation Bar");
        ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId"+ Thread.currentThread().getId());
        getSignInPageObject().verifySignInPage();
        getSignInPageObject().loginAs("HomeOwner");
        getOverviewPageObject().verifyTopNavigationBar("HomeOwner","BORDEAUX");
    }

    @Test(priority = 3, groups = {"smoke"})
    public void verifyOptionsUnderUserName(Method method){
        ExtentTestManager.startTest(method.getName(),"Verify Top Options under username");
        ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId"+ Thread.currentThread().getId());
        getSignInPageObject().verifySignInPage();
        getSignInPageObject().loginAs("HomeOwner");
        getOverviewPageObject().clickOnUserNameButton();
        getOverviewPageObject().verifyAllElementsUnderUserNameDropDown("HomeOwner");
    }

    @Test(priority = 4, groups = {"smoke"})
    public void verifySocialLinks(Method method){
        ExtentTestManager.startTest(method.getName(),"Verify Social Links");
        ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId"+ Thread.currentThread().getId());
        getSignInPageObject().verifySignInPage();
        getSignInPageObject().loginAs("HomeOwner");
        getOverviewPageObject().verifySocialLinks();
    }

    @Test(priority = 5, groups = {"smoke"})
    public void verifySystemName(Method method){
        ExtentTestManager.startTest(method.getName(),"Verify System Name");
        ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId"+ Thread.currentThread().getId());
        getSignInPageObject().verifySignInPage();
        getSignInPageObject().loginAs("HomeOwner");
        getOverviewPageObject().verifySystemName("HomeOwner");
    }

    @Test(priority = 6, groups = {"smoke"})
    public void verifySystemStatusAndUpdatedDate(Method method){
        ExtentTestManager.startTest(method.getName(),"Verify System Status and Updated Date");
        ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId"+ Thread.currentThread().getId());
        getSignInPageObject().verifySignInPage();
        getSignInPageObject().loginAs("HomeOwner");
        getOverviewPageObject().verifySystemStatus("BORDEAUX");
        getOverviewPageObject().verifySystemLastUpdatedDetails();
    }

    @Test(priority = 7, groups = {"smoke"})
    public void verifyUserCanChoosePreviousDatesUsingCalendar(Method method){
        ExtentTestManager.startTest(method.getName(),"Verify user can pick previous dates");
        ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId"+ Thread.currentThread().getId());
        getSignInPageObject().verifySignInPage();
        getSignInPageObject().loginAs("HomeOwner");
        getOverviewPageObject().verifySelecteDateIsDisplayed("Tue, Jun 18, 2019");
    }

}
