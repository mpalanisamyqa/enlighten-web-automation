package com.enphaseenergy.enlighten.pages;

import com.enphaseenergy.enlighten.utils.Data.PropertyReader;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.util.List;

/**
 * @author mnpalanisamy 
 */
public class AdminEventsPage implements BaseObjects{
    public AdminEventsPage(WebDriver driver){
        PageFactory.initElements(driver,this);
    }

    /*Page Objects*/
    @FindBy(css = "h1.page_title")
    private WebElement pageTitle;

    @FindBy(id = "events_table_datatables")
    private WebElement eventsDataTable;

    @FindBy(css = "table#events_table_datatables tbody tr")
    private List<WebElement> eventsTableRows;

    @FindBy(id = "a_sites_datatables_processing")
    private WebElement loadingProgressBar;

    /*Page Contants*/
    private static final String EVENTSPAGETITLE = "Events";

    /*Page Methods*/

    public void verifyEventsPage(){
        progressHelpers.waitForElementToDisplay(pageTitle);
        Assert.assertTrue(eventsDataTable.isDisplayed());
        Assert.assertEquals(pageTitle.getText(),EVENTSPAGETITLE);
    }

    //Need to connect to MySQL to verify it accuracy
    public void verifyEventsAreDispalyed(){
        progressHelpers.hardWait(Integer.valueOf(PropertyReader.getConfigProperties(System.getProperty("ENV"),"hardwait")));
        progressHelpers.waitForElementToDisplay(eventsDataTable);
        Assert.assertEquals(eventsTableRows.size(),10);
    }
}
