package com.enphaseenergy.enlighten.Helpers;

import com.enphaseenergy.enlighten.utils.MySQLConn.MySqlConnOverSSH;

import java.sql.SQLException;
import java.sql.Statement;

/**
 * @author mnpalanisamy
 */
public class MySqlHelper extends MySqlConnOverSSH {

    /**
     * Pass the query to execute eg: "select * from companies"
     * @param query
     */
    public void executeSatement(String query){
        try {
            Statement stmt = conn.createStatement();
            stmt.executeQuery(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void connectToMySql(){
        connectMySql();
    }

    public void disconnectFromMySql(){
        closeConnection();
    }
}
