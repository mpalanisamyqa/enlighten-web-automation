package com.enphaseenergy.enlighten.pages;

import java.net.HttpURLConnection;
import java.net.URL;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.enphaseenergy.enlighten.utils.Data.PropertyReader;

public class OwnershipPage implements BaseObjects{
	
	 public OwnershipPage(WebDriver driver){
	        PageFactory.initElements(driver,this);
	    }
 /////////////page1/////////
	@FindBy(xpath = "//a[@href='/support']")
		private WebElement supportTab; 
	  
	@FindBy(xpath = "//a[@href='/ownership_transfer']")
		private WebElement ownershipTransferbtn;
	 
	 
/////////////page2/////////
	
	@FindBy(xpath = "//div[@class='col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2']/h1")
	private WebElement ownershipText;
	
	@FindBy(xpath = "//div[@id='progress-complete']")
	private WebElement progressLine;
	
	@FindBy(xpath = "//div[@class='flash_message flash_success']//div[@class='text']")
	private WebElement successFetchMsg;
	
	@FindBy(xpath = "//div[@class='flash_message flash_success']//div[@class='icon']")
	private WebElement successIcon;
	
	@FindBy(xpath = "//div[@class='flash_message flash_success']//div[@class='clear']")
	private WebElement clearIcon;
	
	@FindBy(xpath = "(//div[@class='col-md-6 col-xs-12']/legend)[1]")
	private WebElement yourdetailText;
	
	@FindBy(xpath = "(//div[@class='col-md-6 col-xs-12']/legend)[2]")
	private WebElement previous_ownerDetailText;
	
	@FindBy(xpath = "(//div[@class='highlight'])[1]/div[1]")
	private WebElement ownerName;
	
	@FindBy(xpath = "(//div[@class='highlight'])[1]/div[2]")
	private WebElement ownerEmail;
	
	@FindBy(xpath = "(//div[@class='highlight'])[1]/div[3]")
	private WebElement ownerPhone;
		
	@FindBy(xpath = "//label[@for='ownership_transfer_envoy_serial_number']")
	private WebElement envoySerialNo;
	
	@FindBy(id = "ownership_transfer_envoy_serial_number")
	private WebElement envoySerialNoTextBox;
		
	@FindBy(xpath = "//label[@for='ownership_transfer_site_id']")
	private WebElement envoySystemIdText;

	@FindBy(id = "ownership_transfer_site_id")
	private WebElement envoySystemIdTextBox;

	@FindBy(xpath = "//label[@for='ownership_transfer_zip']")
	private WebElement ZipText;
	
	@FindBy(id = "ownership_transfer_site_zip")
	private WebElement zipTextBox;
	
	@FindBy(id = "rc-anchor-container")
	private WebElement recaptcha;
	
	@FindBy(id = "fetch_system_details")
	private WebElement fetchbtn;
	
	@FindBy(xpath = "(//div[@class='text'])[1]")
	private WebElement errorMessage;
	
	@FindBy(xpath = "(//div[@class='clear'])[1]")
	private WebElement closeIcon;
	
	@FindBy(xpath = "(//div[@class='icon'])[1]")
	private WebElement righticon;
		
	@FindBy(xpath = "//div[@id='prev-owner-details']/legend")
	private WebElement if_availabeleText;
	
	@FindBy(id = "ownership_transfer_owner_first_name")
	private WebElement firstNameTextBox;
	
	@FindBy(xpath = "//input[@id='ownership_transfer_owner_first_name']/..//label")
	private WebElement firstNameText;
	
	@FindBy(id = "ownership_transfer_owner_last_name")
	private WebElement lastNameTextBox;
	
	@FindBy(xpath = "//input[@id='ownership_transfer_owner_last_name']/..//label")
	private WebElement lastNameText;
	
	@FindBy(id = "step0Next")
	private WebElement nextBtn;
		
	/////////////////page3///////////
	
	@FindBy(xpath = "//fieldset[@id='address-fields']/legend")
	private WebElement addressInfoText;
	
	@FindBy(id = "ownership_transfer_address_attributes_address1")
	private WebElement address1TextBox;
	
	@FindBy(xpath = "//input[@id='ownership_transfer_address_attributes_address1']/..//label")
	private WebElement address1Text;
	
	@FindBy(xpath = "//input[@id='ownership_transfer_address_attributes_address1']/..//em")
	private WebElement address1MandatoryStar;

	@FindBy(id = "ownership_transfer_address_attributes_address2")
	private WebElement address2TextBox;
	
	@FindBy(xpath = "//input[@id='ownership_transfer_address_attributes_address2']/..//label")
	private WebElement address2Text;
	
	@FindBy(id = "ownership_transfer_address_attributes_city")
	private WebElement cityTextBox;
		
	@FindBy(xpath = "//input[@id='ownership_transfer_address_attributes_city']/..//label")
	private WebElement cityText;
	
	@FindBy(xpath = "//input[@id='ownership_transfer_address_attributes_city']/..//em")
	private WebElement cityMadatoryStar;

	@FindBy(id = "ownership_transfer_address_attributes_country")
	private WebElement countryDrpdown;
	
	@FindBy(xpath = "//label[@for='ownership_transfer_address_attributes_country']")
	private WebElement countryText;
	
	@FindBy(xpath = "//label[@for='ownership_transfer_address_attributes_country']/../em")
	private WebElement countrymandatorystar;
	
	@FindBy(id = "ownership_transfer_address_attributes_state")
	private WebElement stateDropdown;
	
	@FindBy(xpath = "//label[@for='ownership_transfer_address_attributes_state']")
	private WebElement stateText;
	
	@FindBy(xpath = "//label[@for='ownership_transfer_address_attributes_state']/../em")
	private WebElement stateMandatoryStar;
	
	@FindBy(id = "ownership_transfer_address_attributes_zip")
	private WebElement zipCodeTextBox;
		
	@FindBy(xpath = "//input[@id='ownership_transfer_address_attributes_zip']/..")
	private WebElement zipCodeText;
	
	@FindBy(xpath = "//input[@id='ownership_transfer_address_attributes_zip']/..//em")
	private WebElement zipMandatoryStar;
		
	
	/////////error mesg////////
	
	@FindBy(id = "ownership_transfer_address_attributes_address1-error")
	private WebElement addressErrorMessage;
	
	
	@FindBy(id = "ownership_transfer_address_attributes_city-error")
	private WebElement cityErrorMessage;
	
	
	@FindBy(id = "ownership_transfer_address_attributes_country-error")
	private WebElement countryErrorMessage;
	
	
	@FindBy(id = "ownership_transfer_address_attributes_state-error")
	private WebElement stateErrorMessage;
	
	
	@FindBy(id = "ownership_transfer_address_attributes_zip-error")
	private WebElement zipErrorMessage;
	

	@FindBy(id = "step1Prev")
	private WebElement backBtn1;
	
	@FindBy(id = "step1Next")
	private WebElement nextButton1;
		
//////////////page4
	
	@FindBy(id = "payment-fields")
	private WebElement paymentText;
	
	@FindBy(xpath = "//p[@class=\"message transfer-fee-message\"]")
	private WebElement feesText;
	
	@FindBy(xpath = "//a[@href='https://enphase.com/en-us/warranties']")
	private WebElement linkTermCondition;
	
	
	
	@FindBy(id = "validateCardNumber")
	private WebElement cardNoTextBox;
	
	@FindBy(xpath = "//input[@id='validateCardNumber']/../label")
	private WebElement cardNoText;
	
	@FindBy(id = "expiryMonth")
	private WebElement monthTextBox;
	
	@FindBy(id = "validateExpiry")
	private WebElement yearTextBox;
	

	@FindBy(xpath = "//input[@id='expiryMonth']/../label")
	private WebElement expirationdateText;
	
	
	@FindBy(id = "validateCVC")
	private WebElement securityCodeTextBox;
	
	
	@FindBy(xpath = "//input[@id='validateCVC']/../label")
	private WebElement securityCodeText;
	
	@FindBy(xpath = "//a[@href='https://stripe.com/docs/security/stripe']")
	private WebElement hereLink;
	
	
	@FindBy(xpath = "//a[@href='https://stripe.com/docs/security/stripe']/..")
	private WebElement cardDescription;
	

	@FindBy(id = "step2Prev")
	private WebElement backBtn2;
	
	@FindBy(name = "commit")
	private WebElement paybtn;
	
	
	@FindBy(id = "validateCardNumber-error")
	private WebElement carNOErrorMsg;


	@FindBy(id = "expiryMonth-error")
	private WebElement expiryMonthErrorMsg;


	@FindBy(id = "validateExpiry-error")
	private WebElement validateExpiryErrorMsg;

	@FindBy(id = "validateCVC-error")
	private WebElement validateCVCErrorMsg;
	
	//payment success page/////
	@FindBy(xpath = "//div[@class='modern_notice']")
	private WebElement paymentSuccessMsg;

	@FindBy(xpath = "//p[contains(text(),'Thank you for joining the Enphase community! We’re excited to welcome you to the world of solar.')]")

	private WebElement joiningMsg;


	@FindBy(xpath = "//p[contains(text(),'Your Case Number is : ')]")
	private WebElement cardNoMsg;


	@FindBy(xpath = "//a[@href='https://enphase.com/en-us/homeowners/welcome']")
	private WebElement welcomeToEnphaseLiink;



	@FindBy(xpath = "//h4[contains(text(),'North America')]/following-sibling::p[1]")
	private WebElement northAmericaSupport;


	@FindBy(xpath = "//h5[contains(text(),'Netherlands')]/following-sibling::p[1]")
	private WebElement NetherlandsSupport;

	@FindBy(xpath = "//h5[contains(text(),'France')]/following-sibling::p[1]")
	private WebElement FranceGeneral;

	@FindBy(xpath = "//h5[contains(text(),'France')]/following-sibling::p[2]")
	private WebElement FranceSupport;


	@FindBy(xpath = "//h5[contains(text(),'Sydney, Australia')]/following-sibling::p[1]")
	private WebElement Sydney_AustraliaGeneral;


	@FindBy(xpath = "//h5[contains(text(),'Sydney, Australia')]/following-sibling::p[2]")
	private WebElement Sydney_AustraliaTechnicalSupport;



	@FindBy(xpath = "//h5[contains(text(),'Melbourne, Australia')]/following-sibling::p[1]")
	private WebElement Melbourne_AustraliaGeneral;


	@FindBy(xpath = "//h5[contains(text(),'Melbourne, Australia')]/following-sibling::p[2]")
	private WebElement Melbourne_AustraliaTechnicalSupport;


	@FindBy(xpath = "//h5[contains(text(),'New Zealand')]/following-sibling::p[1]")
	private WebElement New_ZealandTechnicalSupport;
	
	
	@FindBy(xpath = "//div[contains(text(),'We can not proceed with this system id. Already have a request for the system id :')]")
	private WebElement AlreadyDonePaymentMsg;
	
	@FindBy(xpath = "//a[contains(@href,'/ownership_transfers/')]")
	private WebElement paymentDoneLink;
	
	
	//FetchOwnerShip//  
	private static final String OWNERSHIPTEXT_ = "Transfer of Ownership";
	private static final String OWNERSHIPYOURDETAILTEXT= "Your Details";
	private static final String OWNERSHIPPREVIOUSOWNERDETAILTEXT= "Please help us find your Previous Owner’s Details";
	private static final String OWNERSHIPENVOYSYSTEMNOTEXT = "Enlighten System ID";
	private static final String OWNERSHIPENVOYSERIALNOTEXT = "Envoy Serial Number";
	private static final String OWNERSHIPZIPTEXT = "Zip";
	private static final String OWNERSHIPsuccessFetchMsg= "We have detected and fetched the system you are looking for. Please click next to continue.";
	
	private static final String OWNERSHIPFetchErrorMsg= "Please provide Envoy Serial Number or Enlighten System ID to fetch System Details";
	
	//previous owner Detail//
	private static final String OWNERSHIPif_availabeleText= "Previous Owner’s Details (if available)";
	private static final String OWNERSHIPfirstNameText= "First Name";
	private static final String OWNERSHIPlastNameText= "Last Name";

	//Address Information//
	private static final String OWNERSHIPaddressInfoText= "Please provide your address information";
	private static final String OWNERSHIPaddress1Text= "Street Address";
	private static final String OWNERSHIPaddress2Text= "Street Address";
	private static final String OWNERSHIPcityText= "City";
	private static final String OWNERSHIPcountryText= "Country";
	private static final String OWNERSHIPstateText= "State/Province";
	private static final String OWNERSHIPzipCodeText= "Zip/Postal Code";

	//Payment//
	private static final String OWNERSHIPpaymentText= "Payment";
	private static final String OWNERSHIPfeesText= "The transfer of warranty will be initiated upon payment of an administrative fees";
	private static final String OWNERSHIPcardNoText= "Credit Card Number";
	private static final String OWNERSHIPexpirationdateText= "Expiration Date";
	private static final String OWNERSHIPsecurityCodeText= "Security Code";
	private static final String OWNERSHIPcardDescription= "Your card will be charged and you will be sent an email receipt when you click the Pay button at the bottom of this form";
	private static final String OWNERSHIPERRORMSG= "This field is required.";
	private static final String OWNERSHIPcarNOErrorMsg= "Please enter a valid credit card number.";
	private static final String OWNERSHIPexpiryMonthErrorMsg= "Please enter a valid number.";
	private static final String OWNERSHIPvalidateExpiryErrorMsg= "Please enter a valid expiration date.";
	private static final String OWNERSHIPvalidateCVCErrorMsg= "Please enter your card's 3 or 4 digit security code.";

	//payment Success/
	private static final String OWNERSHIPpaymentSuccessMsg= "Request for Transfer of Ownership submitted successfully";
	private static final String OWNERSHIPjoiningMsg= "Thank you for joining the Enphase community! We’re excited to welcome you to the world of solar.";
	private static final String OWNERSHIPcardNoMsg= "Your Case Number is :";

	
	private static final String OWNERSHIPAlreadyDonePaymentMsg= "We can not proceed with this system id. Already have a request for the system id";


	public  static void verifyIfLinkBroken(WebElement element) throws Throwable
	{

		String str = element.getAttribute("href");
		try{
			Assert.assertTrue(isLinkBroken(new URL(str)).equalsIgnoreCase("ok"),"");

		}catch(Exception ex)
		{
			System.out.println(("Exception occured: "+ex.getMessage()));
		}
	}

	public static String isLinkBroken(URL url) throws Throwable{
		String response = "";
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		try {
			connection.connect();
			response = connection.getResponseMessage();
			connection.disconnect();
			return response;
		}
		catch(Exception exp) {
			return exp.getMessage();
		}
	}

	public void clickOnSupportTab() {
		supportTab.click();
	}

	public void clickOnOwnershipTransferButton() {
		ownershipTransferbtn.click();
	}

	public void FetchOwnerShipDetailUsingEnvoySerialNo(String site) {
		String envoyNo = PropertyReader.getownershipsiteProperties(site, "envoyserialno");
	    String zipcode = PropertyReader.getownershipsiteProperties(site, "zipcode");

		envoySerialNoTextBox.sendKeys(envoyNo);
		zipTextBox.sendKeys(zipcode);
		fetchbtn.click();
		Assert.assertTrue(successFetchMsg.getText().contains(OWNERSHIPsuccessFetchMsg),successFetchMsg.getText()+"Text not found");
		Assert.assertTrue(progressLine.isDisplayed());
		Assert.assertTrue(successIcon.isDisplayed());
		Assert.assertTrue(clearIcon.isDisplayed());
		Assert.assertTrue(ownerName.isDisplayed());
		Assert.assertTrue(ownerEmail.isDisplayed());
		Assert.assertTrue(ownerPhone.isDisplayed());

	}
		
	public void FetchOwnershipDetailUsingEnlightenSystemId(String site) {

		String systemId = PropertyReader.getownershipsiteProperties(site, "systemid");
		String Zipcode = PropertyReader.getownershipsiteProperties(site, "zipcode");
		envoySystemIdTextBox.sendKeys(systemId);
		zipTextBox.sendKeys(Zipcode);
		fetchbtn.click();
		Assert.assertTrue(successFetchMsg.getText().contains(OWNERSHIPsuccessFetchMsg),successFetchMsg.getText()+"Text not found");
		Assert.assertTrue(progressLine.isDisplayed());
		Assert.assertTrue(successIcon.isDisplayed());
		Assert.assertTrue(clearIcon.isDisplayed());
		Assert.assertTrue(ownerName.isDisplayed());
		Assert.assertTrue(ownerEmail.isDisplayed());
		Assert.assertTrue(ownerPhone.isDisplayed());
	}
	
	public void verifyFetchOwnerDetailAllText() {
		
		Assert.assertTrue( ownershipText.getText().contains(OWNERSHIPTEXT_),ownershipText.getText()+"Text not found");
		Assert.assertTrue( yourdetailText.getText().contains(OWNERSHIPYOURDETAILTEXT),yourdetailText.getText()+"Text not found");
		Assert.assertTrue( previous_ownerDetailText.getText().contains(OWNERSHIPPREVIOUSOWNERDETAILTEXT),previous_ownerDetailText.getText()+"Text not found");
		Assert.assertTrue(envoySerialNo.getText().contains(OWNERSHIPENVOYSERIALNOTEXT),envoySerialNo.getText()+"Text not found");
		Assert.assertTrue(envoySystemIdText.getText().contains(OWNERSHIPENVOYSYSTEMNOTEXT ),envoySystemIdText.getText()+"Text not found");
		Assert.assertTrue(ZipText.getText().contains(OWNERSHIPZIPTEXT),ZipText.getText()+"Text not found");	 
	}
	 
	public void VerifyErrorMsgFetchOwnerDetail() { 
		fetchbtn.click();
		Assert.assertTrue(errorMessage.getText().contains(OWNERSHIPFetchErrorMsg),errorMessage.getText()+"Text not found");
		Assert.assertTrue(closeIcon.isDisplayed());
		Assert.assertTrue(righticon.isDisplayed());
		
	}
	
	public void verifyValidPreviousOwnerDetail() throws Throwable {
		Assert.assertTrue(if_availabeleText.getText().contains(OWNERSHIPif_availabeleText),if_availabeleText.getText()+"Text not found");
		Assert.assertTrue(firstNameText.getText().contains(OWNERSHIPfirstNameText),firstNameText.getText()+"Text not found");
		firstNameTextBox.sendKeys("");
		Assert.assertTrue(lastNameText.getText().contains(OWNERSHIPlastNameText),lastNameText.getText()+"Text not found");
		lastNameTextBox.sendKeys("");
		nextBtn.click(); 
	}
	
	public void verifyInValidPreviousOwnerDetail() throws Throwable 
	{
		Assert.assertTrue(if_availabeleText.getText().contains(OWNERSHIPif_availabeleText),if_availabeleText.getText()+"Text not found");
		Assert.assertTrue(firstNameText.getText().contains(OWNERSHIPfirstNameText),firstNameText.getText()+"Text not found");
		firstNameTextBox.clear();
		Assert.assertTrue(lastNameText.getText().contains(OWNERSHIPlastNameText),lastNameText.getText()+"Text not found");
		lastNameTextBox.clear();
		nextBtn.click(); 
	}
	 
	public void verifyValidManualAddressInformation() {
		Assert.assertTrue(addressInfoText.getText().contains(OWNERSHIPaddressInfoText),addressInfoText.getText()+"Text not found");
		Assert.assertTrue(address1Text.getText().contains(OWNERSHIPaddress1Text),address1Text.getText()+"Text not found");
		Assert.assertTrue(address1MandatoryStar.isDisplayed());
		address1TextBox.sendKeys("");
		Assert.assertTrue(address2Text.getText().contains(OWNERSHIPaddress2Text),address2Text.getText()+"Text not found");
		address2TextBox.sendKeys("");
		Assert.assertTrue(cityText.getText().contains(OWNERSHIPcityText),cityText.getText()+"Text not found");
		Assert.assertTrue(cityMadatoryStar.isDisplayed());
		cityTextBox.sendKeys("");
		Assert.assertTrue(countryText.getText().contains(OWNERSHIPcountryText),countryText.getText()+"Text not found");
		Assert.assertTrue(countrymandatorystar.isDisplayed()); 
		dropDownHelper.SelectUsingVisibleValue(countryDrpdown, "");
		Assert.assertTrue(stateText.getText().contains(OWNERSHIPstateText),stateText.getText()+"Text not found");
		Assert.assertTrue(stateMandatoryStar.isDisplayed()); 
		dropDownHelper.SelectUsingVisibleValue(stateDropdown, "");
		Assert.assertTrue(zipCodeText.getText().contains(OWNERSHIPzipCodeText),zipCodeText.getText()+"Text not found");
		Assert.assertTrue(zipMandatoryStar.isDisplayed()); 
		zipCodeTextBox.sendKeys("");
		nextButton1.click(); 
	}

	
	public void verifyValidDefaultAddressInformation() {
		Assert.assertTrue(addressInfoText.getText().contains(OWNERSHIPaddressInfoText),addressInfoText.getText()+"Text not found");
		Assert.assertTrue(address1Text.getText().contains(OWNERSHIPaddress1Text),address1Text.getText()+"Text not found");
		Assert.assertTrue(address1MandatoryStar.isDisplayed());
		Assert.assertTrue(address2Text.getText().contains(OWNERSHIPaddress2Text),address2Text.getText()+"Text not found");
		Assert.assertTrue(cityText.getText().contains(OWNERSHIPcityText),cityText.getText()+"Text not found");
		Assert.assertTrue(cityMadatoryStar.isDisplayed());
		Assert.assertTrue(countryText.getText().contains(OWNERSHIPcountryText),countryText.getText()+"Text not found");
		Assert.assertTrue(countrymandatorystar.isDisplayed()); 
		Assert.assertTrue(stateText.getText().contains(OWNERSHIPstateText),stateText.getText()+"Text not found");
		Assert.assertTrue(stateMandatoryStar.isDisplayed()); 
		Assert.assertTrue(zipCodeText.getText().contains(OWNERSHIPzipCodeText),zipCodeText.getText()+"Text not found");
		Assert.assertTrue(zipMandatoryStar.isDisplayed()); 
		nextButton1.click(); 
	}
	
	public void verifyInValidAddressInformation() {
		Assert.assertTrue(addressInfoText.getText().contains(OWNERSHIPaddressInfoText),addressInfoText.getText()+"Text not found");
		Assert.assertTrue(address1Text.getText().contains(OWNERSHIPaddress1Text),address1Text.getText()+"Text not found");
		Assert.assertTrue(address1MandatoryStar.isDisplayed());
		address1TextBox.clear();
		Assert.assertTrue(address2Text.getText().contains(OWNERSHIPaddress2Text),address2Text.getText()+"Text not found");
		address2TextBox.clear();
		Assert.assertTrue(cityText.getText().contains(OWNERSHIPcityText),cityText.getText()+"Text not found");
		Assert.assertTrue(cityMadatoryStar.isDisplayed());
		cityTextBox.clear();
		Assert.assertTrue(countryText.getText().contains(OWNERSHIPcountryText),countryText.getText()+"Text not found");
		Assert.assertTrue(countrymandatorystar.isDisplayed()); 
		Select  se1=new Select(countryDrpdown);
		se1.selectByIndex(0);
		Assert.assertTrue(stateText.getText().contains(OWNERSHIPstateText),stateText.getText()+"Text not found");
		Assert.assertTrue(stateMandatoryStar.isDisplayed()); 
		Select  se2=new Select(stateDropdown);
		se2.selectByIndex(0);
		Assert.assertTrue(zipCodeText.getText().contains(OWNERSHIPzipCodeText),zipCodeText.getText()+"Text not found");
		Assert.assertTrue(zipMandatoryStar.isDisplayed()); 
		zipCodeTextBox.clear();
		nextButton1.click(); 	
		Assert.assertTrue(addressErrorMessage.getText().contains(OWNERSHIPERRORMSG),addressErrorMessage.getText()+"Text not found");
		Assert.assertTrue(cityErrorMessage.getText().contains(OWNERSHIPERRORMSG),cityErrorMessage.getText()+"Text not found");
		Assert.assertTrue(countryErrorMessage.getText().contains(OWNERSHIPERRORMSG),countryErrorMessage.getText()+"Text not found");
		Assert.assertTrue(stateErrorMessage.getText().contains(OWNERSHIPERRORMSG),stateErrorMessage.getText()+"Text not found");
		Assert.assertTrue(zipErrorMessage.getText().contains(OWNERSHIPERRORMSG),zipErrorMessage.getText()+"Text not found");
	}
	
	public void verifyValidPaymentDetail() throws Throwable {
		Assert.assertTrue(paymentText.getText().contains(OWNERSHIPpaymentText),paymentText.getText()+"Text not found");	
		Assert.assertTrue(feesText.getText().contains(OWNERSHIPfeesText),feesText.getText()+"Text not found");
		verifyIfLinkBroken(linkTermCondition);
		Assert.assertTrue(cardNoText.getText().contains(OWNERSHIPcardNoText),cardNoText.getText()+"Text not found");
		cardNoTextBox.sendKeys("4242424242424242");
		Assert.assertTrue( expirationdateText.getText().contains(OWNERSHIPexpirationdateText), expirationdateText.getText()+"Text not found");
		monthTextBox.sendKeys("02");
		yearTextBox.sendKeys("2020");
		Assert.assertTrue( securityCodeText.getText().contains(OWNERSHIPsecurityCodeText), securityCodeText.getText()+"Text not found");
		securityCodeTextBox.sendKeys("123");
		feesVerification();
		verifyIfLinkBroken(hereLink);
		paybtn.click();	
		Assert.assertTrue( paymentSuccessMsg.getText().contains(OWNERSHIPpaymentSuccessMsg), paymentSuccessMsg.getText()+"Text not found");	
		Assert.assertTrue( joiningMsg.getText().contains(OWNERSHIPjoiningMsg), joiningMsg.getText()+"Text not found");
		Assert.assertTrue( cardNoMsg.getText().contains(OWNERSHIPcardNoMsg), cardNoMsg.getText()+"Text not found");
		verifyIfLinkBroken(welcomeToEnphaseLiink);
		Assert.assertTrue(northAmericaSupport.isDisplayed());
		Assert.assertTrue(NetherlandsSupport.isDisplayed());
		Assert.assertTrue(FranceGeneral.isDisplayed());
		Assert.assertTrue(FranceSupport.isDisplayed());
		Assert.assertTrue(Sydney_AustraliaGeneral.isDisplayed());
		Assert.assertTrue(Sydney_AustraliaTechnicalSupport.isDisplayed());
		Assert.assertTrue(Melbourne_AustraliaGeneral.isDisplayed());
		Assert.assertTrue(Melbourne_AustraliaTechnicalSupport.isDisplayed());
		Assert.assertTrue(New_ZealandTechnicalSupport.isDisplayed());
	}
	

	public void verifyInValidPaymentDetail() throws Throwable {  
		Assert.assertTrue(paymentText.getText().contains(OWNERSHIPpaymentText),paymentText.getText()+"Text not found");
		Assert.assertTrue(feesText.getText().contains(OWNERSHIPfeesText),feesText.getText()+"Text not found");

		verifyIfLinkBroken(linkTermCondition);
		Assert.assertTrue(cardNoText.getText().contains(OWNERSHIPcardNoText),cardNoText.getText()+"Text not found");
		cardNoTextBox.sendKeys("8181818181818181");
		Assert.assertTrue( expirationdateText.getText().contains(OWNERSHIPexpirationdateText), expirationdateText.getText()+"Text not found");
		monthTextBox.sendKeys("a1");
		yearTextBox.sendKeys("ab12");
		Assert.assertTrue( securityCodeText.getText().contains(OWNERSHIPsecurityCodeText), securityCodeText.getText()+"Text not found");
		securityCodeTextBox.sendKeys("eew");
		verifyIfLinkBroken(hereLink);
		paybtn.click();
		Assert.assertTrue( carNOErrorMsg.getText().contains(OWNERSHIPcarNOErrorMsg), carNOErrorMsg.getText()+"Text not found");  

	}   
	
	public void verifyAlreadyDonePayment(String site) throws Throwable {
		
		String systemId = PropertyReader.getownershipsiteProperties(site, "systemid");
		String Zipcode = PropertyReader.getownershipsiteProperties(site, "zipcode");
		envoySystemIdTextBox.sendKeys(systemId);
		zipTextBox.sendKeys(Zipcode);
		fetchbtn.click();
		Assert.assertTrue( AlreadyDonePaymentMsg.getText().contains(OWNERSHIPAlreadyDonePaymentMsg), AlreadyDonePaymentMsg.getText()+"Text not found");  
		Assert.assertTrue(closeIcon.isDisplayed());
		Assert.assertTrue(righticon.isDisplayed());
		paymentDoneLink.click();
		//already done payment
		browserHelper.SwitchToWindow(0);
		Assert.assertTrue( paymentSuccessMsg.getText().contains(OWNERSHIPpaymentSuccessMsg), paymentSuccessMsg.getText()+"Text not found");	
		verifyIfLinkBroken(welcomeToEnphaseLiink);
		Assert.assertTrue(northAmericaSupport.isDisplayed());
		Assert.assertTrue(NetherlandsSupport.isDisplayed());
		Assert.assertTrue(FranceGeneral.isDisplayed());
		Assert.assertTrue(FranceSupport.isDisplayed());
		Assert.assertTrue(Sydney_AustraliaGeneral.isDisplayed());
		Assert.assertTrue(Sydney_AustraliaTechnicalSupport.isDisplayed());
		Assert.assertTrue(Melbourne_AustraliaGeneral.isDisplayed());
		Assert.assertTrue(Melbourne_AustraliaTechnicalSupport.isDisplayed());
		Assert.assertTrue(New_ZealandTechnicalSupport.isDisplayed());
		browserHelper.switchToParentWindow();	    
	}
	
	public void feesVerification() {
		
		if(dropDownHelper.getSelectedValue(countryDrpdown).equals("United States"))
		{
			Assert.assertTrue((feesText.getText().contains("399") && feesText.getText().contains("USD")));
		}
		else
		if(dropDownHelper.getSelectedValue(countryDrpdown).equals("Australia") || dropDownHelper.getSelectedValue(countryDrpdown).equals("New Zealand") )
		{
			Assert.assertTrue((feesText.getText().contains("199") && feesText.getText().contains("AUD")));
		}
		else
		if(dropDownHelper.getSelectedValue(countryDrpdown).equals("France") || dropDownHelper.getSelectedValue(countryDrpdown).equals("Austria") ||
				dropDownHelper.getSelectedValue(countryDrpdown).equals("Belgium") || dropDownHelper.getSelectedValue(countryDrpdown).equals("Czech Republic") ||
				dropDownHelper.getSelectedValue(countryDrpdown).equals("Denmark") || dropDownHelper.getSelectedValue(countryDrpdown).equals("Germany") || 
				dropDownHelper.getSelectedValue(countryDrpdown).equals("Hungary") || dropDownHelper.getSelectedValue(countryDrpdown).equals("Ireland") || 
				dropDownHelper.getSelectedValue(countryDrpdown).equals("Italy") || dropDownHelper.getSelectedValue(countryDrpdown).equals("Luxembourg") || 
				dropDownHelper.getSelectedValue(countryDrpdown).equals("Malta") || dropDownHelper.getSelectedValue(countryDrpdown).equals("Netherlands") ||
				dropDownHelper.getSelectedValue(countryDrpdown).equals("Poland") || dropDownHelper.getSelectedValue(countryDrpdown).equals("Slovenia")||
				dropDownHelper.getSelectedValue(countryDrpdown).equals("Spain") || dropDownHelper.getSelectedValue(countryDrpdown).equals("Sweden") ||
				dropDownHelper.getSelectedValue(countryDrpdown).equals("Switzerland"))
		{
			Assert.assertTrue((feesText.getText().contains("249") && feesText.getText().contains("EUR")));	
		}
		else
		if(dropDownHelper.getSelectedValue(countryDrpdown).equals("United Kingdom"))
		{
			Assert.assertTrue((feesText.getText().contains("249") && feesText.getText().contains("GBP")));			
		}
	}
	
	 
}


 
 
 
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	


