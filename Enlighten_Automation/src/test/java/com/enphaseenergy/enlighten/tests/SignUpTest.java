package com.enphaseenergy.enlighten.tests;

import org.testng.annotations.Test;

import java.io.File;
import java.lang.reflect.Method;
import com.enphaseenergy.enlighten.utils.Report.ExtentTestManager;
import com.relevantcodes.extentreports.LogStatus;

public class SignUpTest extends BaseTest{

	/**@author kulanjirajanp
	 * 	
	 */
	  @Test(groups = {"sanity","prod_sanity","qa4_sanity","qa2_sanity"})
	public void ENLM1311_VerifySignupforEnlightenSolarProfessionalSignUp_Page(Method method) throws Throwable{
		ExtentTestManager.startTest(method.getName(),"Verify Admin signUp");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId"+ Thread.currentThread().getId());
		getSignUpPageObject().clickSignbtn();
		getSignUpPageObject().selectSolarProfessional();
		getSignUpPageObject().signupSolarProfessional(System.getProperty("user.dir") + File.separator + "src/test/resources/test_data/" + "inputdata.xlsx","NewUserRegisterSolarProfessionl","1");
	}
	  @Test(groups = {"sanity","prod_sanity","qa4_sanity","qa2_sanity"})
	public void ENLM1306_VerifySignupforEnlightendoityourselferSignUpPage(Method method) throws Throwable{
		ExtentTestManager.startTest(method.getName(),"Verify installer signUp");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId"+ Thread.currentThread().getId());
		getSignUpPageObject().clickSignbtn();
		getSignUpPageObject().selectSelfInstaller();
		getSignUpPageObject().signupHomeOwnerSelfInstaller("NewUserRegisterHomeOwner","1");
	}
	  @Test(groups = {"sanity","prod_sanity","qa4_sanity","qa2_sanity"})
	public void ENLM1314_verifySignupforHomeownerSignUpPage(Method method) throws Throwable{
		ExtentTestManager.startTest(method.getName(),"Verify HomeOwner signUp");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId"+ Thread.currentThread().getId());
		getSignUpPageObject().clickSignbtn();
		getSignUpPageObject().selectHomeOwner();
		getSignUpPageObject().signupHomeOwnerSelfInstaller("NewUserRegisterHomeOwner","1");
	}
	  @Test(groups = {"sanity","prod_sanity","qa4_sanity","qa2_sanity"})
	public void ENLM1308_verifyAdminErrorMessage(Method method) throws Throwable{
		ExtentTestManager.startTest(method.getName(),"Verify Admin Error Message");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId"+ Thread.currentThread().getId());
		getSignUpPageObject().clickSignbtn();
		getSignUpPageObject().selectSolarProfessional();
		getSignUpPageObject().verifyProfessionalRegisterErrorMessage();
	}
	  @Test(groups = {"sanity","prod_sanity","qa4_sanity","qa2_sanity"})
	public void ENLM1312_verifyInstallerErrorMessage(Method method) throws Throwable{
		ExtentTestManager.startTest(method.getName(),"Verify Installer Error Message");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId"+ Thread.currentThread().getId());
		getSignUpPageObject().clickSignbtn();
		getSignUpPageObject().selectSelfInstaller();
		getSignUpPageObject().Verify_Home_Installer_ErrorMessage();
	}
	  @Test(groups = {"sanity","prod_sanity","qa4_sanity","qa2_sanity"})
	public void ENLM1313_verifyHomeOwnerErrorMessage(Method method) throws Throwable{
		ExtentTestManager.startTest(method.getName(),"Verify HomeOwner Error Message");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId"+ Thread.currentThread().getId());
		getSignUpPageObject().clickSignbtn();
		getSignUpPageObject().selectHomeOwner();
		getSignUpPageObject().Verify_Home_Installer_ErrorMessage();
	}
	  @Test(groups = {"sanity","prod_sanity","qa4_sanity","qa2_sanity"})
	public void ENLM1313_verifyAdminSignUpPage(Method method) throws Throwable{
		ExtentTestManager.startTest(method.getName(),"verify Admin SignUpPage");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId"+ Thread.currentThread().getId());
		getSignUpPageObject().clickSignbtn();
		getSignUpPageObject().selectSolarProfessional();
		getSignUpPageObject().verifyProfessionalRegisterpage();
	}
	  @Test(groups = {"sanity","prod_sanity","qa4_sanity","qa2_sanity"})
	public void ENLM1315_verifyInstallerSignUpPage(Method method) throws Throwable{
		ExtentTestManager.startTest(method.getName(),"verify Installer SignUpPage");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId"+ Thread.currentThread().getId());
		getSignUpPageObject().clickSignbtn();
		getSignUpPageObject().selectSelfInstaller();
		getSignUpPageObject().VerifyHomeInstallerSignUpPage();
	}
	  @Test(groups = {"sanity","prod_sanity","qa4_sanity","qa2_sanity"})
	public void ENLM1316_verifyHomeOwnerSignUpPage(Method method) throws Throwable{
		ExtentTestManager.startTest(method.getName(),"verify HomeOwner SignUpPage");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId"+ Thread.currentThread().getId());
		getSignUpPageObject().clickSignbtn();
		getSignUpPageObject().selectHomeOwner();
		getSignUpPageObject().VerifyHomeInstallerSignUpPage();
	} 
}
