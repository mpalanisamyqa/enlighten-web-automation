package com.enphaseenergy.enlighten.pages;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.enphaseenergy.enlighten.Helpers.CalendarHelper;
import com.enphaseenergy.enlighten.Helpers.LoggerHelper;


public class SupportPage implements BaseObjects {
	Logger logger = LoggerHelper.getLogger(CalendarHelper.class);
	/**@author kulanjirajanp
	 * @throws Throwable 
	 * 	
	 */
	public SupportPage(WebDriver driver){
		PageFactory.initElements(driver,this);
	}



	@FindBy(xpath = "//a[@href='http://go.enphase.com/gettingstarted']")
	private WebElement linkgettingStarted;


	@FindBy(xpath = "//a[@href='http://go.enphase.com/gettingstarted']/following-sibling::p[1]")
	private WebElement textgettingstarted;


	@FindBy(xpath = "(//a[@href='https://enphase.com/en-us/support/solar-professionals/software?modal-content=support-regions?m=mgr'])[1]")
	private WebElement linkHelp1;

	@FindBy(xpath = "(//a[@href='https://enphase.com/en-us/support/solar-professionals/software?modal-content=support-regions?m=mgr'])[2]")
	private WebElement linkHelp2;

	@FindBy(xpath = "//a[@href='http://go.enphase.com/gettingstarted']/following-sibling::p[2]")
	private WebElement texthelp;


	@FindBy(xpath = "//a[@href='/ownership_transfer']")
	private WebElement ownershipTransferbtn;


	@FindBy(xpath = "//a[@href='http://go.enphase.com/gettingstarted']/following-sibling::p[3]")
	private WebElement textownership;



	@FindBy(xpath = "//h3[text()='Email Support']")
	private WebElement emailsupportText;

	@FindBy(id = "subject")
	private WebElement subjectTextBox;

	@FindBy(xpath = "//label[@for='msg_subject']")
	private WebElement subjectText;

	@FindBy(id = "description")
	private WebElement   msgTextBox; 

	@FindBy(xpath = "//label[@for='description']")
	private WebElement msgText;

	@FindBy(id = "submit")
	private WebElement btnSend;

	@FindBy(xpath = "//label[@for='subject']")
	private WebElement subjectErrorMsg;

	@FindBy(xpath = "//label[@for='description']")
	private WebElement descriptionErrorMsg;

	@FindBy(xpath = "//div[text()='Thank you for your message!']")
	private WebElement successMsg;




	@FindBy(xpath = "//h3[text()='Phone Support']")
	private WebElement textSupport;

	@FindBy(xpath = "//p[text()='Have your Site ID ready when you call:']")
	private WebElement textSiteId;

	@FindBy(xpath = "//label[text()='Site Name:']")
	private WebElement textSiteName;

	@FindBy(id = "site_search")
	private WebElement searchSiteName;

	@FindBys({@FindBy(xpath = "//ul[@class='ui-autocomplete ui-menu ui-widget ui-widget-content ui-corner-all']/li/a")})
	private List<WebElement> autoSuggesd;

	@FindBy(id = "enstore_icon")
	private WebElement enstoreIcon;

	@FindBy(xpath = "//a[text()='Homeowners']")
	private WebElement gettingStartedChildWindow;

	@FindBy(xpath = "//h2[text()='Choose your region']")
	private WebElement helpSupportChildWindow;

	@FindBy(xpath = "//img[@src='http://store-qa2.enphase.com/storefront/pub/media/logo/default/enphase_store_logo.png']")
	private WebElement enstoreChildWindow;



	@FindBy(xpath = "//h4[contains(text(),'United States')]")
	private WebElement usText;

	@FindBy(xpath = "//h4[contains(text(),'United States')]/following-sibling::p[1]")
	private WebElement usTextSupport;

	@FindBy(xpath = "//h4[contains(text(),'Australia')]")
	private WebElement AustraliaText;

	@FindBy(xpath = " //h4[contains(text(),'Australia')]/following-sibling::p[1]")
	private WebElement AustraliaTextSupport;

	@FindBy(xpath = "//h4[contains(text(),'Netherlands')]")
	private WebElement NetherlandsText;

	@FindBy(xpath = "//h4[contains(text(),'Netherlands')]/following-sibling::p[1]")
	private WebElement NetherlandsSupport;

	@FindBy(xpath = "//h4[contains(text(),'United Kingdom')]")
	private WebElement ukText;

	@FindBy(xpath = "//h4[contains(text(),'United Kingdom')]/following-sibling::p[1]")
	private WebElement ukSupport;

	@FindBy(xpath = "//h4[contains(text(),'Belgium')]")
	private WebElement belgiumText;

	@FindBy(xpath = "//h4[contains(text(),'Belgium')]/following-sibling::p[1]")
	private WebElement belgiumSupport;

	@FindBy(xpath = "//h4[contains(text(),'Germany')]")
	private WebElement GermanyText;

	@FindBy(xpath = "//h4[contains(text(),'Germany')]/following-sibling::p[1]")
	private WebElement GermanySupport;

	@FindBy(xpath = "//h4[contains(text(),'Italy')]")
	private WebElement ItalyText;

	@FindBy(xpath = "//h4[contains(text(),'Italy')]/following-sibling::p[1]")
	private WebElement ItalySupport;

	@FindBy(xpath = "//h4[contains(text(),'Switzerland')]")
	private WebElement SwitzerlandText;

	@FindBy(xpath = "//h4[contains(text(),'Switzerland')]/following-sibling::p[1]")
	private WebElement SwitzerlandSupport;

	@FindBy(xpath = "//h4[contains(text(),'Switzerland')]")
	private WebElement SpainText;

	@FindBy(xpath = "//h4[contains(text(),'Switzerland')]/following-sibling::p[1]")
	private WebElement SpainSupport;

	@FindBy(xpath = "//h4[contains(text(),'France')]")
	private WebElement FranceText;

	@FindBy(xpath = "//h4[contains(text(),'France')]/following-sibling::p[1]")
	private WebElement FranceSupport;

	@FindBy(id = "span-Enphase-Energized-AC-Modules")
	private WebElement Fr;


	private static final String TEXTGETTINGSTARTEDCHILDWINDOW= "Homeowners";
	private static final String TEXTHELPSUPPORTCHILDWINDOW = "Choose your region";

	private static final String TEXTPHONESUPPORT= "Phone Support";
	private static final String TEXTSITEID= "Have your Site ID ready when you call:";
	private static final String TEXTSITENAME= "Site Name:";

	private static final String TEXTUS= "United States";
	private static final String TEXTAUSTRALIA= "Australia";
	private static final String TEXTNETHERLANDS= "Netherlands";
	private static final String TEXTUK= "United Kingdom";
	private static final String TEXTBELGIUM= "Belgium";
	private static final String TEXTGERMANY= "Germany";
	private static final String TEXTITALY= "Italy";
	private static final String TEXTSWITZERLAND= "Switzerland";
	private static final String TEXTSPAIN= "Spain:";
	private static final String TEXTFRANCE= "France";
	private static final String TEXTEMAIL= "Email Support";
	private static final String TEXTSUBJECT= "Subject:";
	private static final String TEXTMSG= "Message:";

	private static final String SUCCESSMSG= "This field is required.";
	private static final String ERRORMSG= "This field is required."; 


	public  static void verifyIfLinkBroken(WebElement element) throws Throwable
	{
		String str = element.getAttribute("href");
		try{
			Assert.assertTrue(isLinkBroken(new URL(str)).equalsIgnoreCase("ok"),"");

		}catch(Exception ex)
		{
			boolean flag=false;
			Assert.assertTrue(flag);
		}
	}

	public static String isLinkBroken(URL url) throws Throwable{
		String response = "";
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		try {
			connection.connect();
			response = connection.getResponseMessage();
			connection.disconnect();
			return response;
		}
		catch(Exception exp) {
			return exp.getMessage();
		}
	}


	public void verifySupportLink() throws Throwable {

		verifyIfLinkBroken(linkHelp1);
		verifyIfLinkBroken(linkHelp2); 
		verifyIfLinkBroken(ownershipTransferbtn);
	}

	public void verifyEnstoreLink() 
	{
		//Not able to capture windowid//   	
		//enstoreIcon.click();
		//browserHelper.SwitchToWindow(1);	
		// assertHelper.assertElementsExistsInPage(Fr); 	
		//browserHelper.switchToParentWindow();   	
	}	

	public void verifyHelplink1() {
		linkHelp1.click();
		browserHelper.SwitchToWindow(1);
		assertHelper.compareTwoValues(helpSupportChildWindow.getText(),TEXTHELPSUPPORTCHILDWINDOW);
		browserHelper.switchToParentWindow();

	}

	public void verifyHelplink2() {
		linkHelp2.click();
		browserHelper.SwitchToWindow(1);
		assertHelper.compareTwoValues(helpSupportChildWindow.getText(),TEXTHELPSUPPORTCHILDWINDOW);
		browserHelper.switchToParentWindow();		
	}

	public void verifyGettingStartedLink() {

		try {
			linkgettingStarted.click();
			browserHelper.SwitchToWindow(1);
			assertHelper.compareTwoValues(gettingStartedChildWindow.getText(),TEXTGETTINGSTARTEDCHILDWINDOW);
			browserHelper.switchToParentWindow();

		} catch (Exception e) {
			logger.info("GettingStartedLink is not displayed");
		}

	}




	public void verifySearchSiteName(String siteName) {
		try {
			assertHelper.compareTwoValues(textSupport.getText(),TEXTPHONESUPPORT);
			assertHelper.compareTwoValues(textSiteId.getText(),TEXTSITEID);
			assertHelper.compareTwoValues(textSiteName.getText(),TEXTSITENAME);
			generalHelper.enterTextInTextField(searchSiteName,siteName);
			for(WebElement we: autoSuggesd) {
				Assert.assertTrue( we.getText().contains(siteName),we.getText()+"Text not found");
			}
			progressHelpers.hardWait(4000);
			autoSuggesd.get(0).click();

		} catch (Exception e) {
			logger.info("SearchTextBox is not displayed");
		}


	}

	public void verifySupportText() {
		assertHelper.compareTwoValues(usText.getText(),TEXTUS);
		assertHelper.compareTwoValues(AustraliaText.getText(),TEXTAUSTRALIA);
		assertHelper.compareTwoValues(NetherlandsText.getText(),TEXTNETHERLANDS);
		assertHelper.compareTwoValues(ukText.getText(),TEXTUK);
		assertHelper.compareTwoValues(belgiumText.getText(),TEXTBELGIUM);
		assertHelper.compareTwoValues(GermanyText.getText(),TEXTGERMANY);
		assertHelper.compareTwoValues(ItalyText.getText(),TEXTITALY);
		assertHelper.compareTwoValues(SwitzerlandText.getText(),TEXTSWITZERLAND);
		assertHelper.compareTwoValues(SpainText.getText(),TEXTSPAIN);
		assertHelper.compareTwoValues(FranceText.getText(),TEXTFRANCE);	
		assertHelper.assertElementsExistsInPage(usTextSupport);
		assertHelper.assertElementsExistsInPage(AustraliaTextSupport);
		assertHelper.assertElementsExistsInPage(NetherlandsSupport);
		assertHelper.assertElementsExistsInPage(ukSupport);
		assertHelper.assertElementsExistsInPage(belgiumSupport);
		assertHelper.assertElementsExistsInPage(GermanySupport);
		assertHelper.assertElementsExistsInPage(ItalySupport);
		assertHelper.assertElementsExistsInPage(SwitzerlandSupport);
		assertHelper.assertElementsExistsInPage(SpainSupport);
		assertHelper.assertElementsExistsInPage(FranceSupport);    	
	}
	public void verifyEmailSupportTetx() {
		assertHelper.compareTwoValues(emailsupportText.getText(),TEXTEMAIL);
		assertHelper.compareTwoValues(subjectText.getText(),TEXTSUBJECT);
		assertHelper.compareTwoValues(msgText.getText(),TEXTMSG);
	}

	public void emailsupportErrorMsg() {

		buttonHelper.click(btnSend);
		assertHelper.compareTwoValues(subjectErrorMsg.getText(),ERRORMSG);
		assertHelper.compareTwoValues(descriptionErrorMsg.getText(),ERRORMSG);

	}
	public void verifySendEmail(String msg,String sub) {
		verifyEmailSupportTetx();
		generalHelper.enterTextInTextField(subjectTextBox, sub);
		generalHelper.enterTextInTextField(msgTextBox, msg);
		buttonHelper.click(btnSend); 	
		assertHelper.compareTwoValues(successMsg.getText(),SUCCESSMSG);
	}
}
