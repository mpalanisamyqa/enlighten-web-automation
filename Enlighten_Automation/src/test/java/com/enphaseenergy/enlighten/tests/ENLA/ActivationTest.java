package com.enphaseenergy.enlighten.tests.ENLA;

import com.enphaseenergy.enlighten.tests.BaseTest;
import com.enphaseenergy.enlighten.utils.Report.ExtentTestManager;
import com.relevantcodes.extentreports.LogStatus;
import org.testng.annotations.Test;

import java.lang.reflect.Method;

/**
 * @author mnpalanisamy
 */
public class ActivationTest extends BaseTest {

    @Test(groups = {"sanity","qa2"})
    public void verifyCreateActivation(Method method){
        ExtentTestManager.startTest(method.getName(),"Verify Create New Activation with Admin Role");
        ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId"+ Thread.currentThread().getId());
        getSignInPageObject().verifySignInPage();
        getSignInPageObject().loginAs("ADMIN_AUT_1");
        getDashboardPageObject().verifyDashboardPage();
        getDashboardPageObject().clickOnMenu("Systems");
        getSystemPageObject().verifySystemsPage();
        getSystemPageObject().clickOnTab("Activations");
        getSystemPageObject().clickOnAddANewSystemLink();
        getActivationPageObject().verifyNewActivationPage();
        getActivationPageObject().createNewSystem("Activation","3");
        getActivationPageObject().verifySuccessfullActivationMessage();
    }

    @Test(groups = {"sanity","qa2"}, dependsOnMethods = {"verifyCreateActivation"})
    public void verifyReadActivation(Method method){
        ExtentTestManager.startTest(method.getName(),"Verify Read New Activation with Admin Role");
        ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId"+ Thread.currentThread().getId());
        getSignInPageObject().verifySignInPage();
        getSignInPageObject().loginAs("ADMIN_AUT_1");
        getDashboardPageObject().verifyDashboardPage();
        getDashboardPageObject().clickOnMenu("Systems");
        getSystemPageObject().verifySystemsPage();
        getSystemPageObject().clickOnTab("Activations");
        getSystemPageObject().verifyActivationTableIsDisplayed();
        getSystemPageObject().searchActivations("Activation","3");
        getActivationPageObject().verifyActivatedSystemDetails("Activation","3");

    }

    @Test(groups = {"sanity","qa2"}, dependsOnMethods = {"verifyReadActivation"})
    public void verifyUpdateActivation(Method method){
        ExtentTestManager.startTest(method.getName(),"Verify Update New Activation with Admin Role");
        ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId"+ Thread.currentThread().getId());
        getSignInPageObject().verifySignInPage();
        getSignInPageObject().loginAs("ADMIN_AUT_1");
        getDashboardPageObject().verifyDashboardPage();
        getDashboardPageObject().clickOnMenu("Systems");
        getSystemPageObject().verifySystemsPage();
        getSystemPageObject().clickOnTab("Activations");
        getSystemPageObject().verifyActivationTableIsDisplayed();
        getSystemPageObject().searchActivations("Activation","3");
        getActivationPageObject().updateActivatedSystem("Activation","4");
        getActivationPageObject().verifySuccessfullUpdateActivationMessage();
        getActivationPageObject().verifyActivatedSystemDetails("Activation","4");
    }

    @Test(groups = {"sanity","qa2"}, dependsOnMethods = {"verifyUpdateActivation"})
    public void verifyDeleteActivation(Method method){
        ExtentTestManager.startTest(method.getName(),"Verify Delete New Activation with Admin Role");
        ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId"+ Thread.currentThread().getId());
        getSignInPageObject().verifySignInPage();
        getSignInPageObject().loginAs("ADMIN_AUT_1");
        getDashboardPageObject().verifyDashboardPage();
        getDashboardPageObject().clickOnMenu("Systems");
        getSystemPageObject().verifySystemsPage();
        getSystemPageObject().clickOnTab("Activations");
        getSystemPageObject().verifyActivationTableIsDisplayed();
        getSystemPageObject().searchActivations("Activation","4");
        getActivationPageObject().deleteActivation();
        getActivationPageObject().verifySuccessfullRemovalMessage();
    }

    @Test(groups = {"sanity","prod_sanity","qa4_sanity","qa2_sanity"})
    public void ENLA1872_verifyAllTheFiltersBehaviourInActivationTable(Method method){
        ExtentTestManager.startTest(method.getName(),"Verify All The Filters in Activation Table with Admin Role");
        ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId"+ Thread.currentThread().getId());
        getSignInPageObject().verifySignInPage();
        getSignInPageObject().loginAs("EEADMIN_ADMIN_AUT_1");
        getDashboardPageObject().verifyDashboardPage();
        getDashboardPageObject().clickOnMenu("Admin");
        getAdminPageObject().verifyAdminPage();
        getAdminPageObject().navigateToLeftMenu("Activations");
        getAdminActivationPageObject().verifyAllTheFiltersInTable("Activations","1");
    }

    @Test(groups = {"sanity","prod_sanity","qa4_sanity","qa2_sanity"})
    public void ENLA1873_verifyDifferentStagesOfActivationInActivationTable(Method method){
        ExtentTestManager.startTest(method.getName(),"Verify Different stages of activation in Activation Table with Admin Role");
        ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId"+ Thread.currentThread().getId());
        getSignInPageObject().verifySignInPage();
        getSignInPageObject().loginAs("EEADMIN_ADMIN_AUT_1");
        getDashboardPageObject().verifyDashboardPage();
        getDashboardPageObject().clickOnMenu("Admin");
        getAdminPageObject().verifyAdminPage();
        getAdminPageObject().navigateToLeftMenu("Activations");
        getAdminActivationPageObject().verifyFilterWithAllTheStages();
    }

    @Test(groups = {"sanity","prod_sanity","qa4_sanity","qa2_sanity"})
    public void ENLA1871_verifyAllFieldsCanBeSortedInActivationTable(Method method) {
        ExtentTestManager.startTest(method.getName(), "Verify All Fields can be sorted in Activation Table with Admin Role");
        ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId" + Thread.currentThread().getId());
        getSignInPageObject().verifySignInPage();
        getSignInPageObject().loginAs("EEADMIN_ADMIN_AUT_1");
        getDashboardPageObject().verifyDashboardPage();
        getDashboardPageObject().clickOnMenu("Admin");
        getAdminPageObject().verifyAdminPage();
        getAdminPageObject().navigateToLeftMenu("Activations");
        getAdminActivationPageObject().verifyActivationTableSortFunctionality();
    }
}
