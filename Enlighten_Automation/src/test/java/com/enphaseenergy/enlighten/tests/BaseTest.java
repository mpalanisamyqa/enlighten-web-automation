package com.enphaseenergy.enlighten.tests;

import com.enphaseenergy.enlighten.Helpers.LoggerHelper;
import com.enphaseenergy.enlighten.pages.*;
import com.enphaseenergy.enlighten.utils.Data.PropertyReader;
import com.enphaseenergy.enlighten.utils.Driver.DriverFactory;
import com.enphaseenergy.enlighten.utils.Driver.DriverManager;
import com.enphaseenergy.enlighten.utils.Report.ExtentManager;
import com.enphaseenergy.enlighten.utils.Report.ExtentTestManager;
import com.relevantcodes.extentreports.LogStatus;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

import java.lang.reflect.Method;

public class BaseTest {
    protected WebDriver driver;
    Logger logger = LoggerHelper.getLogger(BaseTest.class);

    /**
     * Below method shall get the environment variable from system variable
     * Both BrowserName and Environment will be passed from command line
     */
    @BeforeMethod(alwaysRun = true)
    public void startDriver(){
        driver = DriverFactory.configDriver(System.getProperty("BROWSER"));
        DriverManager.setWebDriver(driver);
        logger.info("Browser is set and Launched");
        driver.navigate().to(PropertyReader.getConfigProperties(System.getProperty("ENV"),"url"));
    }

    @AfterMethod(alwaysRun = true)
    public void closeDriver(){
        logger.info("Browser closed");
       // driver.quit();
        driver.close();
    }

    /*Below Two methods are for Extent Reports*/
//    @BeforeMethod
    public void beforeMethod(Method method) {
        ExtentTestManager.startTest(method.getName());
    }

    @AfterMethod
    protected void afterMethod(ITestResult result) {
        if (result.getStatus() == ITestResult.FAILURE) {
            ExtentTestManager.getTest().log(LogStatus.FAIL, result.getThrowable());
        } else if (result.getStatus() == ITestResult.SKIP) {
            ExtentTestManager.getTest().log(LogStatus.SKIP, "Test skipped " + result.getThrowable());
        } else {
            ExtentTestManager.getTest().log(LogStatus.PASS, "Test passed");
        }

        ExtentManager.getReporter().endTest(ExtentTestManager.getTest());
        ExtentManager.getReporter().flush();
    }

    /*Gettes for all the page objects*/

    public ConsumptionPage getConsumptionPageObject(){
        ConsumptionPage consumptionPage = new ConsumptionPage(driver);
        return consumptionPage;

    }

    public DashboardPage getDashboardPageObject(){
        DashboardPage dashboardPage = new DashboardPage(driver);
        return dashboardPage;
    }

    public OverviewPage getOverviewPageObject(){
        OverviewPage overviewPage = new OverviewPage(driver);
        return overviewPage;
    }

    public ProductionPage getProductionPageObject(){
        ProductionPage productionPage = new ProductionPage(driver);
        return productionPage;
    }

    public ReportsPage getReportsPageObject(){
        ReportsPage reportsPage = new ReportsPage(driver);
        return reportsPage;
    }

    public SignInPage getSignInPageObject(){
        SignInPage signInPage = new SignInPage(driver);
        return signInPage;
    }

    public ActivationsPage getActivationPageObject(){
        ActivationsPage activationsPage = new ActivationsPage(driver);
        return activationsPage;
    }

    public SystemsPage getSystemPageObject(){
        SystemsPage systemsPage = new SystemsPage(driver);
        return systemsPage;
    }

    public AccountPage getAccountPageObject(){
        AccountPage accountPage = new AccountPage(driver);
        return accountPage;
    }

    public CompanyInformationPage getCompanyInformationPageObject(){
        CompanyInformationPage companyInformationPage = new CompanyInformationPage(driver);
        return companyInformationPage;
    }

    public BranchOfficesPage getBranchOfficePageObject(){
        BranchOfficesPage branchOfficesPage = new BranchOfficesPage(driver);
        return branchOfficesPage;
    }

    public AdminPage getAdminPageObject(){
        AdminPage adminPage = new AdminPage(driver);
        return adminPage;
    }

    public CompaniesPage getCompaniesPageObject(){
        CompaniesPage companiesPage = new CompaniesPage(driver);
        return companiesPage;
    }
    public OwnershipPage getOwnershipPagePageObject(){
    	OwnershipPage ownershipPage=new OwnershipPage(driver);
         return ownershipPage;
     }
     

    public AdminSystemsPage getAdminSystemPageObject(){
        AdminSystemsPage adminSystemsPage = new AdminSystemsPage(driver);
        return adminSystemsPage;
    }

    public EEAdminPage getEEAdminPageObject(){
        EEAdminPage eeAdminPage = new EEAdminPage(driver);
        return eeAdminPage;
    }

    public AdminEventsPage getAdminEventPageObject(){
        AdminEventsPage adminEventsPage = new AdminEventsPage(driver);
        return adminEventsPage;
    }

    public AdminRollupsPage getAdminRollupsPageObject(){
        AdminRollupsPage adminRollupsPage = new AdminRollupsPage(driver);
        return adminRollupsPage;
    }

    public AdminDevicesPage getAdminDevicesPageObject(){
        AdminDevicesPage adminDevicesPage = new AdminDevicesPage(driver);
        return adminDevicesPage;
    }

    public AdminEnvoyPage getAdminEnvoyPageObject(){
        AdminEnvoyPage adminEnvoyPage = new AdminEnvoyPage(driver);
        return adminEnvoyPage;
    }

    public AdminSystemAccessPage getAdminSystemAccessPageObject(){
        AdminSystemAccessPage adminSystemAccessPage = new AdminSystemAccessPage(driver);
        return adminSystemAccessPage;
    }

    public AdminActivationPage getAdminActivationPageObject(){
        AdminActivationPage adminActivationPage = new AdminActivationPage(driver);
        return adminActivationPage;
    }


}
