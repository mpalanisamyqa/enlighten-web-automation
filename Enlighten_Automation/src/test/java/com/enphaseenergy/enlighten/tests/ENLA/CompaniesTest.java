package com.enphaseenergy.enlighten.tests.ENLA;

import com.enphaseenergy.enlighten.tests.BaseTest;
import com.enphaseenergy.enlighten.utils.Report.ExtentTestManager;
import com.relevantcodes.extentreports.LogStatus;
import org.testng.annotations.Test;

import java.lang.reflect.Method;

/**
 * @author mnpalanisamy
 */
public class CompaniesTest extends BaseTest {

    /*
    Execute the below query in qa2 mysql before running the sanity tests

    Delete from companies where name='EIN Admin testing #12'
    Delete from companies where name='EIN Admin testing #112'
    Delete from companies where name='EIN Admin testing Branch #116'
    */

    @Test(groups = {"sanity","qa2"})
    public void verifyCreateNewCompany(Method method){
        ExtentTestManager.startTest(method.getName(),"verify Create New Company with Admin Role");
        ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId"+ Thread.currentThread().getId());
        getSignInPageObject().verifySignInPage();
        getSignInPageObject().loginAs("ADMIN_AUT_1");
        getDashboardPageObject().verifyDashboardPage();
        getDashboardPageObject().clickOnMenu("Admin");
        getAdminPageObject().navigateToLeftMenu("Companies");
        getCompaniesPageObject().createNewCompany("CompanyInformation","3");
        getCompaniesPageObject().enterCompanyInformation("CompanyInformation","3");
        getCompaniesPageObject().verifyCompanyInformationSavedSuccessfully();
    }

    @Test(groups = {"sanity","qa2"}, dependsOnMethods = "verifyCreateNewCompany")
    public void verifyReadCreatedCompany(Method method){
        ExtentTestManager.startTest(method.getName(),"verify Read New Company with Admin Role");
        ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId"+ Thread.currentThread().getId());
        getSignInPageObject().verifySignInPage();
        getSignInPageObject().loginAs("ADMIN_AUT_1");
        getDashboardPageObject().verifyDashboardPage();
        getDashboardPageObject().clickOnMenu("Admin");
        getAdminPageObject().navigateToLeftMenu("Companies");
        getCompaniesPageObject().searchCompany("CompanyInformation","3");
        getCompaniesPageObject().clickOnEditCompanyButton();
        getCompaniesPageObject().readCompanyInformation("CompanyInformation","3");
    }

    @Test(groups = {"sanity","qa2"}, dependsOnMethods = "verifyReadCreatedCompany")
    public void verifyUpdateCreatedCompany(Method method){
        ExtentTestManager.startTest(method.getName(),"verify Update New Company with Admin Role");
        ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId"+ Thread.currentThread().getId());
        getSignInPageObject().verifySignInPage();
        getSignInPageObject().loginAs("ADMIN_AUT_1");
        getDashboardPageObject().verifyDashboardPage();
        getDashboardPageObject().clickOnMenu("Admin");
        getAdminPageObject().navigateToLeftMenu("Companies");
        getCompaniesPageObject().searchCompany("CompanyInformation","3");
        getCompaniesPageObject().clickOnEditCompanyButton();
        getCompaniesPageObject().updateCompany("CompanyInformation","4");
        getCompaniesPageObject().readCompanyInformation("CompanyInformation","4");
    }

    @Test(groups = {"sanity","qa2"}, dependsOnMethods = "verifyUpdateCreatedCompany")
    public void verifyCreateNewBranchOffice(Method method){
        ExtentTestManager.startTest(method.getName(),"verify Create New Branch Office with Admin Role");
        ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId"+ Thread.currentThread().getId());
        getSignInPageObject().verifySignInPage();
        getSignInPageObject().loginAs("ADMIN_AUT_1");
        getDashboardPageObject().verifyDashboardPage();
        getDashboardPageObject().clickOnMenu("Admin");
        getAdminPageObject().navigateToLeftMenu("Companies");
        getCompaniesPageObject().searchCompany("CompanyInformation","4");
        getCompaniesPageObject().createNewBranchCompany("BranchOffice","3");
        getCompaniesPageObject().enterCompanyInformation("BranchOffice","3");
        getCompaniesPageObject().verifyCompanyInformationSavedSuccessfully();
    }

    @Test(groups = {"sanity","qa2"}, dependsOnMethods = "verifyCreateNewBranchOffice")
    public void verifyReadCreatedBranchOffice(Method method){
        ExtentTestManager.startTest(method.getName(),"verify Read New Branch Office with Admin Role");
        ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId"+ Thread.currentThread().getId());
        getSignInPageObject().verifySignInPage();
        getSignInPageObject().loginAs("ADMIN_AUT_1");
        getDashboardPageObject().verifyDashboardPage();
        getDashboardPageObject().clickOnMenu("Admin");
        getAdminPageObject().navigateToLeftMenu("Companies");
        getCompaniesPageObject().searchCompany("BranchOffice","3");
        getCompaniesPageObject().clickOnEditCompanyButton();
        getCompaniesPageObject().readBranchOfficeInformation("BranchOffice","3");
    }

    @Test(groups = {"sanity","qa2"}, dependsOnMethods = "verifyReadCreatedBranchOffice")
    public void verifyUpdateCreatedBranchOffice(Method method){
        ExtentTestManager.startTest(method.getName(),"verify Update New Branch Office with Admin Role");
        ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId"+ Thread.currentThread().getId());
        getSignInPageObject().verifySignInPage();
        getSignInPageObject().loginAs("ADMIN_AUT_1");
        getDashboardPageObject().verifyDashboardPage();
        getDashboardPageObject().clickOnMenu("Admin");
        getAdminPageObject().navigateToLeftMenu("Companies");
        getCompaniesPageObject().searchCompany("BranchOffice","3");
        getCompaniesPageObject().clickOnEditCompanyButton();
        getCompaniesPageObject().updateCompany("BranchOffice","4");
        getCompaniesPageObject().readCompanyInformation("BranchOffice","4");
    }
}
