package com.enphaseenergy.enlighten.tests.ENLM;

import com.enphaseenergy.enlighten.tests.BaseTest;
import com.enphaseenergy.enlighten.utils.Report.ExtentTestManager;
import com.relevantcodes.extentreports.LogStatus;
import org.testng.annotations.Test;

import java.lang.reflect.Method;

/**
 * @author mnpalanisamy
 */
public class BranchOfficesTest extends BaseTest {

    /*Below scripts are from BOC-2157*/
    @Test(groups = {"sanity","qa2"})
    public void verifyCreateBranchOffice(Method method){
        ExtentTestManager.startTest(method.getName(),"verify Create Branch Office with Installer Role");
        ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId"+ Thread.currentThread().getId());
        getSignInPageObject().verifySignInPage();
        getSignInPageObject().loginAs("INSTALLER_AUT_1");
        getDashboardPageObject().verifyDashboardPage();
        getDashboardPageObject().clickOnMenu("Account");
        getAccountPageObject().selectTheLeftMenuOption("BranchOffices");
        getBranchOfficePageObject().clickOnAddBranchOfficeButton();
        getBranchOfficePageObject().createBranchOffice("BranchOffice","1");
        getBranchOfficePageObject().verifyBranchOfficeSavedSuccessfully();
    }

    @Test(groups = {"sanity","qa2"}/*, dependsOnMethods = "verifyCreateBranchOffice"*/)
    public void verifyReadBranchOffice(Method method){
        ExtentTestManager.startTest(method.getName(),"verify Read Branch Office with Installer Role");
        ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId"+ Thread.currentThread().getId());
        getSignInPageObject().verifySignInPage();
        getSignInPageObject().loginAs("INSTALLER_AUT_1");
        getDashboardPageObject().verifyDashboardPage();
        getDashboardPageObject().clickOnMenu("Account");
        getAccountPageObject().selectTheLeftMenuOption("BranchOffices");
        getBranchOfficePageObject().openCreatedBranchOffice("BranchOffice","1");
        getBranchOfficePageObject().readBranchOfficeInformation("BranchOffice","1");

    }

    @Test(groups = {"sanity","qa2"}, dependsOnMethods = "verifyReadBranchOffice")
    public void verifyUpdateBranchOffice(Method method){
        ExtentTestManager.startTest(method.getName(),"verify Update Branch Office with Installer Role");
        ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId"+ Thread.currentThread().getId());
        getSignInPageObject().verifySignInPage();
        getSignInPageObject().loginAs("INSTALLER_AUT_1");
        getDashboardPageObject().verifyDashboardPage();
        getDashboardPageObject().clickOnMenu("Account");
        getAccountPageObject().selectTheLeftMenuOption("BranchOffices");
        getBranchOfficePageObject().openCreatedBranchOffice("BranchOffice","1");
        getBranchOfficePageObject().updateBranchOffice("BranchOffice","2");
        getBranchOfficePageObject().verifyBranchOfficeSavedSuccessfully();
        getBranchOfficePageObject().readBranchOfficeInformation("BranchOffice","2");

    }

//    @Test(priority = 4, groups = {"sanity","qa2"}, dependsOnMethods = "verifyUpdateBranchOffice")
    public void verifyDeleteCreatedBranchOffice(Method method){
        ExtentTestManager.startTest(method.getName(),"verify Delete Branch Office from MySql");
        ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId"+ Thread.currentThread().getId());
        getBranchOfficePageObject().deleteBranchOffice("BranchOffice","2");
    }


}
