package com.enphaseenergy.enlighten.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class DashboardPage implements BaseObjects{

    public DashboardPage(WebDriver driver){
        PageFactory.initElements(driver,this);
    }

    /*Objects of DashboardPage*/
    @FindBy(id = "left_logo")
    private WebElement enlightenManagerLogo;

    @FindBy(css = "div#header_navbar [href='/dashboard']")
    private WebElement dashboardNavMenu;

    @FindBy(css = "div#header_navbar [href='/systems']")
    private WebElement systemsNavMenu;

    @FindBy(css = "div#header_navbar [href='/account']")
    private WebElement accountNavMenu;

    @FindBy(css = "div#header_navbar [href='/support']")
    private WebElement supportNavMenu;

    @FindBy(css = "div#header_navbar [href='/eeadmin']")
    private WebElement adminNavMenu;

    /*Page Methods*/

    public void verifyDashboardPage(){
        Assert.assertTrue(enlightenManagerLogo.isDisplayed());
    }

    /**
     * @param menuName
     */
    public void clickOnMenu(String menuName){
        switch (menuName.toLowerCase()){
            case "dashboard":
                dashboardNavMenu.click();
                break;
            case "systems":
                systemsNavMenu.click();
                break;
            case "account":
                accountNavMenu.click();
                break;
            case "support":
                supportNavMenu.click();
                break;
            case "admin":
                adminNavMenu.click();
                break;
                default:
                    dashboardNavMenu.click();
                    break;
        }
    }
}
