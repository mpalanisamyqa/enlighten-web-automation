package com.enphaseenergy.enlighten.Helpers;

import com.enphaseenergy.enlighten.utils.Driver.DriverManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ButtonHelper {

    WebDriver driver = DriverManager.getDriver();
    Logger logger = LoggerHelper.getLogger(ButtonHelper.class);


    public void click(By locator) {
        click(driver.findElement(locator));
        logger.info(locator);
    }

    public void click(WebElement element){
        element.click();
        logger.info(element);
    }
}
