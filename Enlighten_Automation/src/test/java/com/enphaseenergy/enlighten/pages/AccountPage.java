package com.enphaseenergy.enlighten.pages;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.enphaseenergy.enlighten.utils.Data.PropertyReader;

/**
 * @author mnpalanisamy
 */
public class AccountPage implements BaseObjects {
	public AccountPage(WebDriver driver){
		PageFactory.initElements(driver,this);
	}

	/*Objects of the Page*/

	@FindBy(id = "nav_overview")
	private WebElement myAccountLeftMenuButton;

	@FindBy(id = "nav_company")
	private WebElement companyInformationLeftMenuButton;

	@FindBy(id = "nav_branches")
	private WebElement branchOfficesLeftMenuButton;

	@FindBy(id = "nav_profiles")
	private WebElement gridProfilesLeftMenuButton;

	@FindBy(id = "nav_users")
	private WebElement usersLeftMenuButton;

	@FindBy(id = "nav_password_reset")
	private WebElement resetPasswordLeftMenuButton;

	@FindBy(id = "nav_logout")
	private WebElement signOutLeftMenuButton;



	/* Account Detail Section*/

	@FindBy(xpath = "//h2[text()='Account Details']")
	private WebElement accountTitle;

	@FindBy(id = "user_email")
	private WebElement emailTextBox;

	@FindBy(xpath = "//label[@for='user_email']")
	private WebElement emaiText;

	@FindBy(id = "user_first_name")
	private WebElement fistNameTextBox;

	@FindBy(xpath = "//label[@for='user_first_name']")
	private WebElement firstNameText;

	@FindBy(id = "user_last_name")
	private WebElement lastNameTextBox;

	@FindBy(xpath = "//label[@for='user_last_name']")
	private WebElement lastNameText;

	@FindBy(id = "user_phone")
	private WebElement phoneTextBox;

	@FindBy(xpath = "//label[@for='user_phone']")
	private WebElement phoneText;

	@FindBy(xpath = "//th[text()='Company name']")
	private WebElement companyNameText;

	@FindBy(xpath = "//td[contains(text(),'Enphase Energy')]")
	private WebElement enphaseEnergyText;

	@FindBy(xpath = "//a[contains(text(),'View Enlighten administrator(s)')]")
	private WebElement viewEnlightenLink;

	@FindBy(xpath = "//p[contains(text(),'These Enlighten users can update your company information and add/modify user records.')]")
	private WebElement userRecordText;

	@FindBys({@FindBy(xpath = "//div[@id='company_admins_list']//a")})
	private List<WebElement> adminAccessRecord;

	@FindBy(id="save")
	private WebElement saveBtn;

	@FindBy(xpath = "//div[text()='Account Details saved successfully.']")
	private WebElement successMsg;

	@FindBy(xpath = "//a[@href='/systems']")
	private WebElement systemTab;

	@FindBy(xpath = "(//a[@href='/account'])[1]")
	private WebElement accountTab;


	/* Account Detail Section*/

	
	/*Constant Strings*/

	private static final String SUCCESSMSG= "Account Details saved successfully.";
	private static final String ACCOUNTTITLE= "Account Details";
	private static final String EMAIL= "Email";
	private static final String FIRSTNAME= "First Name";
	private static final String LASTNAME= "Last Name";
	private static final String PHONE= "Phone";
	private static final String COMPANYNAME= "Company name";
	private static final String ENPHASEENERGY= "Enphase Energy";
	private static final String USERRECORD= "These Enlighten users can update your company information and add/modify user records.";



	/*Page Methods*/

	/**
	 * This method is to choose the option in the left menu in account screen
	 * @param option
	 */
	public void selectTheLeftMenuOption(String option){
		switch (option.toLowerCase()){
		case "myaccount":
			myAccountLeftMenuButton.click();
			break;
		case "companyinformation":
			companyInformationLeftMenuButton.click();
			break;
		case "branchoffices":
			branchOfficesLeftMenuButton.click();
			break;
		case "gridprofiles":
			gridProfilesLeftMenuButton.click();
			break;
		case "users":
			usersLeftMenuButton.click();
			break;
		case "resetpassword":
			resetPasswordLeftMenuButton.click();
			break;
		case "signout":
			signOutLeftMenuButton.click();
			break;
		}
		progressHelpers.hardWait(1000);
	}
	
	  /* Account Detail Section*/
	public void clickOnviewEnlightenLink() {
		buttonHelper.click(viewEnlightenLink);
	}
	public void verifyAccountDetailText() {
		assertHelper.compareTwoValues(accountTitle.getText(),ACCOUNTTITLE);
		assertHelper.compareTwoValues(emaiText.getText(),EMAIL);
		assertHelper.compareTwoValues(firstNameText.getText(),FIRSTNAME);
		assertHelper.compareTwoValues(lastNameText.getText(),LASTNAME);
		assertHelper.compareTwoValues(phoneText.getText(),PHONE);
		assertHelper.compareTwoValues(companyNameText.getText(),COMPANYNAME);
	}
	public void verifyEnlightenadministrator() {
		assertHelper.compareTwoValues(userRecordText.getText(),USERRECORD);
		for(WebElement we:adminAccessRecord) {
			Assert.assertTrue((we.getText().contains("enphaseenergy") || we.getText().contains("gmail.com") || we.getText().contains("riverstonetech.com") || we.getText().contains("mailinator.com")));		
		}	
	}
	public void verifyAccountDetailMail(String userType) {
		String email=PropertyReader.getUserProperties(userType,"email");
		assertHelper.compareTwoValues(emailTextBox.getAttribute("value"),email);	
	}
	public void clickOnSaveBtn() {
		buttonHelper.click(saveBtn);
	}
	public void verifyUpdateAccountDetail(String phNo) {
		generalHelper.enterTextInTextField(phoneTextBox,phNo);
		clickOnSaveBtn();
		assertHelper.compareTwoValues(successMsg.getText(),SUCCESSMSG);
		buttonHelper.click(systemTab);
		buttonHelper.click(accountTab);
		assertHelper.compareTwoValues(phoneTextBox.getAttribute("value"),phNo);			
	}
	/* Account Detail Section*/
}



