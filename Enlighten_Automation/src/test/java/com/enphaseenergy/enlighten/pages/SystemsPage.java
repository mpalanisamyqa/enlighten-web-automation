package com.enphaseenergy.enlighten.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author mnpalanisamy
 */
public class SystemsPage implements BaseObjects{

    public SystemsPage(WebDriver driver){
        PageFactory.initElements(driver,this);
    }

    /*Objects of the Page*/
    @FindBy(css = "li#nav_overview [href='/systems']")
    private WebElement listTab;

    @FindBy(css = "li#nav_reports [href='/systems/reports']")
    private WebElement reportsTab;

    @FindBy(css = "li#nav_activations [href='/activations?stage=inprogress']")
    private WebElement activationsTab;

    @FindBy(css = "a#add_system")
    private WebElement addNewSystemLink;

    @FindBy(id = "sites_datatables")
    private WebElement systemsTable;

    @FindBy(id = "sites_datatables_processing")
    private WebElement systemsTableProgressbar;

    @FindBy(css = "input[name='name']")
    private WebElement activationTableNameSearchTextField;

    @FindBy(css = "table#activations_datatables tr#filter_header th.ref_id input")
    private WebElement activationTableInstallerReferenceSearchTextField;

    @FindBy(css = "table#activations_datatables tr#filter_header th.address1 input")
    private WebElement activationTableAddress1SearchTextField;

    @FindBy(css = "table#activations_datatables tr#filter_header th.address2 input")
    private WebElement activationTableAddress2SearchTextField;

    @FindBy(css = "table#activations_datatables tr#filter_header th.city input")
    private WebElement activationTableCitySearchTextField;

    @FindBy(css = "table#activations_datatables tr#filter_header th.state input")
    private WebElement activationTableStateSearchTextField;

    @FindBy(css = "table#activations_datatables tr#filter_header th.country input")
    private WebElement activationTableCountrySearchTextField;

    @FindBy(css = "table#activations_datatables tr#filter_header th.installer_name input")
    private WebElement activationTableInstallerNameSearchTextField;

    @FindBy(css = "table#activations_datatables tr#filter_header th.stage input")
    private WebElement activationTableStageSearchTextField;

    @FindBy(css = "table#activations_datatables tr#filter_header th.arrays_built input")
    private WebElement activationTableArraysBuiltSearchTextField;

    @FindBy(id = "submit")
    private WebElement activationTableSearchButton;

    @FindBy(xpath = "//table[@id='activations_datatables']/tbody/tr/descendant::a")
    private List<WebElement> activationTableSearchedItemNames;

    @FindBy(id = "activations_datatables")
    private WebElement activationTable;

    @FindBy(xpath = "//div[@id='activations_datatables_info'][contains(text(),'Showing 1 to 1 of 1 activations')]")
    private WebElement activationShowingOneOfOneActivationsText;

    @FindBy(css = "table#activations_datatables tbody tr")
    private List<WebElement> activationTableBodyRows;

    /*Page Methods*/

    public void verifySystemsPage(){
        progressHelpers.waitForElementToInvisible(systemsTableProgressbar);
        progressHelpers.waitForElementToDisplay(systemsTable);
        Assert.assertTrue(systemsTable.isDisplayed());
    }

    /**
     * @param tabName
     */
    public void clickOnTab(String tabName){
        switch (tabName.toLowerCase()){
            case "list":
                listTab.click();
                break;
            case "reports":
                reportsTab.click();
                break;
            case "activations":
                activationsTab.click();
                break;
                default:
                    listTab.click();
                    break;

        }
    }

    public void clickOnAddANewSystemLink(){
        addNewSystemLink.click();
    }

    /**
     * @param sheetName
     * @param rowIndex
     */
    public void searchActivations(String sheetName, String rowIndex){
        String activationName = testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"System Name");
        progressHelpers.waitForElementToDisplay(activationTable);
        progressHelpers.waitForElementToBeClickable(activationTableNameSearchTextField);
        enterActivationName(activationName);
        activationTableSearchButton.click();
        progressHelpers.waitForElementToDisplay(activationTable);
        clickOnActivationSite(activationName);
//        progressHelpers.waitForElementToInvisible(activationTable);
    }


    /**
     * @param activationName
     */
    public void enterActivationName(String activationName){
//        activationTableNameSearchTextField.click();
        activationTableNameSearchTextField.clear();
        activationTableNameSearchTextField.sendKeys(activationName);
    }

    /**
     * @param activationSystemName
     */
    public void clickOnActivationSite(String activationSystemName){
        for(WebElement individualSystemName:activationTableSearchedItemNames){
            if(individualSystemName.getText().equalsIgnoreCase(activationSystemName)){
                progressHelpers.hardWait(2000);
                individualSystemName.click();
                progressHelpers.hardWait(2000);
                break;
            }
        }
    }

    public void verifyActivationTableIsDisplayed(){
        Assert.assertTrue(activationTable.isDisplayed());
    }

    public void verifyEnteredSearchTextInActivationName(String searchedText){
        Assert.assertEquals(activationTableNameSearchTextField.getAttribute("value"),searchedText);
    }

    public void activationTableSearchHandler(){
        while(activationTableBodyRows.size()>1){
            activationTableSearchButton.click();
        }
    }
}
