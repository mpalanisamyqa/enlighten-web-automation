package com.enphaseenergy.enlighten.pages;

import com.enphaseenergy.enlighten.Helpers.*;
import com.enphaseenergy.enlighten.utils.Driver.DriverManager;
import org.openqa.selenium.WebDriver;

public interface BaseObjects {
    WebDriver driver = DriverManager.getDriver();
    AlertHelper alertHelper = new AlertHelper();
    BrowserHelper browserHelper = new BrowserHelper();
    ButtonHelper buttonHelper = new ButtonHelper();
    CheckBoxAndRadioButtonHelper checkBoxAndRadioButtonHelper = new CheckBoxAndRadioButtonHelper();
    DropDownHelper dropDownHelper = new DropDownHelper();
    GeneralHelper generalHelper = new GeneralHelper();
    JavascriptHelper javascriptHelper = new JavascriptHelper();
    LinkHelper linkHelper = new LinkHelper();
    LoggerHelper loggerHelper = new LoggerHelper();
    ProgressHelpers progressHelpers = new ProgressHelpers();
    CalendarHelper calendarHelper = new CalendarHelper();
    TestDataHelper testDataHelper = new TestDataHelper();
    MySqlHelper mysqlHelper = new MySqlHelper();
    AssertHelper assertHelper = new AssertHelper();

}
