package com.enphaseenergy.enlighten.pages;

import com.enphaseenergy.enlighten.utils.Data.PropertyReader;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class OverviewPage implements BaseObjects{

    public OverviewPage(WebDriver driver){
        PageFactory.initElements(driver,this);
    }

    /*Objects of OverviewPage*/

    @FindBy(xpath = "//span[@class='logo brand']")
    private WebElement myEnlightenLogo;

    @FindBy(css = "a#topnav-menu")
    private WebElement myEnlightenUserName;

    @FindBy(css = "a#topnav-support")
    private WebElement myEnlightenSupportMenu;

    @FindBy(css = "ul.dropdown-menu h5")
    private WebElement myEnlightenSupportSectionTitle;

    @FindBy(css = "ul.dropdown-menu p a.button")
    private WebElement myEnlightenSupportHyperLink;

    @FindBy(css = "ul.dropdown-menu p a.button span")
    private WebElement myEnlightenSupportHyperLinkText;

    @FindBy(css = "ul.dropdown-menu p a.button img")
    private WebElement myEnlightenSupportHyperLinkImage;

    @FindBy(css = "ul.dropdown-menu div.info h5")
    private WebElement myEnlightenMaintenanceSectionTitle;

    @FindBy(css = "ul.dropdown-menu div.info a.button")
    private WebElement myEnlightenMaintenanceHyperLink;

    @FindBy(css = "ul.dropdown-menu div.info a.button span")
    private WebElement myEnlightenMaintenanceHyperLinkText;

    @FindBy(css = "ul.dropdown-menu div.info a.button img")
    private WebElement myEnlightenMaintenanceHyperLinkImage;

    @FindBy(css = "ul.dropdown-menu div.info h4")
    private WebElement myEnlightenMaintenerName;

    @FindBy(css = "ul.dropdown-menu div.info p")
    private WebElement myEnlightenMaintenerAddress;

    @FindBy(css = "ul.dropdown-menu div.info p a")
    private WebElement myEnlightenMaintenerEmail;

    @FindBy(css = "div.consumer-loading")
    private WebElement myEnlightenLoadingProgressBar;

    @FindBy(css = "ul#global-nav")
    private WebElement myEnlightenUserNameDropDown;

    @FindBy(css = "title#at-svg-facebook-1 + g path")
    private WebElement facebookButton;

    @FindBy(css = "title#at-svg-twitter-2 + g path")
    private WebElement twitterButton;

    @FindBy(css = "div.title_head h2")
    private WebElement systemName;

    @FindBy(css = "li.system_status .value")
    private WebElement systemStatusText;

    @FindBy(css = "li.system_status  .icon")
    private WebElement systemStatusIcon;

    @FindBy(css = "li.system_last_update")
    private WebElement systemLastUpdatedText;

    @FindBy(css = "li.system_last_update span")
    private WebElement systemLastUpdatedValue;

    @FindBy(css = "span.update-data")
    private WebElement systemRefreshButton;

    @FindBy(css = "span[name='prev']")
    private WebElement calendarPreviousButton;

    @FindBy(css = "span[name='next']")
    private WebElement calendarNextButton;

    @FindBy(css = "input[class='date form-control date-control']")
    private WebElement systemDate;

    @FindBy(css = "div.datepicker-days th.prev")
    private WebElement datePickerPreviousButton;

    @FindBy(css = "div.datepicker-days th.next")
    private WebElement datePickerNextButton;

    @FindBy(css = "div.datepicker-days table tbody")
    private WebElement datePickerTable;

    @FindBy(css = "div.datepicker-days th.datepicker-switch")
    private WebElement datePickerMonthAndYear;

    @FindBy(xpath = "//td[@class='day']")
    private List<WebElement> datePickerEnableDates;

    @FindBy(xpath = "//td[@class='disabled day']")
    private List<WebElement> datePickerDisableDates;

    /*OverviewPage constants*/

    private static final String MYENLIGHTENLOGOTEXT = "MyEnlighten";
    private static final String MYENLIGHTENSUPPORTTITLE = "Enlighten help and Enphase support";
    private static final String MYENLIGHTENSUPPORTHYPERLINKTEXT = "Enphase support";
    private static final String MYENLIGHTENMAINTENANCETITLE = "System support and maintenance";
    private static final String MYENLIGHTENMAINTAINERHYPERLINKTEXT = "Contact maintainer";
    private static final String MYENLIGHTENSUPPORTBUTTON = "Support";
    private static final List<String> USERNAMEDROPDOWNSTATICITEMS = new ArrayList<String>(Arrays.asList("Overview", "Production","Reports","Settings"));
    private static final String MYENLIGHTENSIGNOUTTEXT = "Sign Out";
    private static final String MYENLIGHTENSYSTEMSACCORDIONTEXT = "SYSTEMS";
    private static final String MYENLIGHTENSYSTEMLASTUPDATEDTEXT = "Data Last Updated";



    public void verifyMyEnlightenLandingPage(){
        progressHelpers.waitForElementToDisplay(myEnlightenLogo);
        Assert.assertEquals(myEnlightenLogo.getText(),MYENLIGHTENLOGOTEXT);
    }

    public void verifyTopNavigationBar(String userType, String siteName){
        progressHelpers.waitForElementToInvisible(myEnlightenLoadingProgressBar);
        Assert.assertEquals(myEnlightenLogo.getText(),MYENLIGHTENLOGOTEXT);
        String expectedName = PropertyReader.getUserProperties(userType,"firstname")+" "+PropertyReader.getUserProperties(userType,"lastname");
        Assert.assertEquals(myEnlightenUserName.getText().trim(),expectedName);
        clickOnSupportButton();
        Assert.assertEquals(MYENLIGHTENSUPPORTBUTTON,myEnlightenSupportMenu.getText().trim());
        verifyAllElementsUnderSupportDropDown(userType,siteName);

    }

    public void clickOnSupportButton(){
        myEnlightenSupportMenu.click();
    }

    public void verifyAllElementsUnderSupportDropDown(String userType, String siteName){

        /*Support Section*/
        Assert.assertEquals(MYENLIGHTENSUPPORTTITLE,myEnlightenSupportSectionTitle.getText().trim());
        Assert.assertEquals(PropertyReader.getSiteProperties(siteName,"supporturl"),myEnlightenSupportHyperLink.getAttribute("href"));
        Assert.assertEquals(MYENLIGHTENSUPPORTHYPERLINKTEXT,myEnlightenSupportHyperLinkText.getText());
        Assert.assertEquals(PropertyReader.getSiteProperties(siteName,"supporturlimage"),myEnlightenSupportHyperLinkImage.getAttribute("src"));

        /*Maintainer Section*/
        Assert.assertEquals(MYENLIGHTENMAINTENANCETITLE,myEnlightenMaintenanceSectionTitle.getText().trim());
        Assert.assertEquals(PropertyReader.getSiteProperties(siteName,"maintainerurl"),myEnlightenMaintenanceHyperLink.getAttribute("href"));
        Assert.assertEquals(MYENLIGHTENMAINTAINERHYPERLINKTEXT,myEnlightenMaintenanceHyperLinkText.getText());
        Assert.assertEquals(PropertyReader.getSiteProperties(siteName,"maintainerurlimage"),myEnlightenMaintenanceHyperLinkImage.getAttribute("src"));

        Assert.assertEquals(PropertyReader.getSiteProperties(siteName,"maintainer"),myEnlightenMaintenerName.getText().trim());
        Assert.assertEquals(PropertyReader.getSiteProperties(siteName,"maintaineraddress"),myEnlightenMaintenerAddress.getText());
        Assert.assertEquals(PropertyReader.getSiteProperties(siteName,"maintaineremail"),myEnlightenMaintenerEmail.getText());
    }

    public void clickOnUserNameButton(){
        progressHelpers.waitForElementToInvisible(myEnlightenLoadingProgressBar);
        myEnlightenUserName.click();
    }

    public void verifyAllElementsUnderUserNameDropDown(String userType){
        progressHelpers.waitForElementToDisplay(myEnlightenUserNameDropDown);
        List<String> allItems = dropDownHelper.getAllDropDownValuesFromList(myEnlightenUserNameDropDown);
        String userSystems = PropertyReader.getUserProperties(userType,"systems");
        List<String> systemList = Arrays.asList(userSystems.split(";"));
        List<String> allExpectedItems = new ArrayList<String>();
        allExpectedItems.addAll(USERNAMEDROPDOWNSTATICITEMS);
        allExpectedItems.addAll(systemList);

        if(allItems.size()>5){
            for(int i=0;i<allItems.size();i++){
                if(i==allItems.size()-1){
                    Assert.assertEquals(allItems.get(i), MYENLIGHTENSIGNOUTTEXT);
                }else {
                    Assert.assertEquals(allItems.get(i), allExpectedItems.get(i));
                }
            }
        }
    }

    public void verifySocialLinks(){
        progressHelpers.waitForElementToInvisible(myEnlightenLoadingProgressBar);
        Assert.assertTrue(facebookButton.isDisplayed());
        Assert.assertTrue(twitterButton.isDisplayed());
    }

    public void verifySystemName(String userType){
        String userSystems = PropertyReader.getUserProperties(userType,"systems");
        List<String> systemList = Arrays.asList(userSystems.split(";"));
        String expectedSystemName = systemList.get(0);
        Assert.assertEquals(expectedSystemName,systemName.getText().trim());

    }

    public void verifySystemStatus(String siteName){
        progressHelpers.waitForElementToInvisible(myEnlightenLoadingProgressBar);
        String expectedStatus = PropertyReader.getSiteProperties(siteName,"status");
        Assert.assertEquals(expectedStatus,systemStatusText.getText().trim());
        Assert.assertTrue(systemStatusIcon.isDisplayed());
    }

    public void verifySystemLastUpdatedDetails(){
        progressHelpers.waitForElementToInvisible(myEnlightenLoadingProgressBar);
        Assert.assertTrue(systemLastUpdatedValue.isDisplayed());
        Assert.assertTrue(systemLastUpdatedText.getText().trim().contains(MYENLIGHTENSYSTEMLASTUPDATEDTEXT));
    }

    public void verifyRefreshButton(){
        progressHelpers.waitForElementToInvisible(myEnlightenLoadingProgressBar);
        Assert.assertTrue(systemRefreshButton.isDisplayed());
        clickOnRefreshButton();
        progressHelpers.setImplicitWait(10, TimeUnit.SECONDS);
        Assert.assertTrue(myEnlightenLoadingProgressBar.isDisplayed());
        progressHelpers.waitForElementToInvisible(myEnlightenLoadingProgressBar);
        verifyMyEnlightenLandingPage();
    }

    public void clickOnRefreshButton(){
        systemRefreshButton.click();
    }


    public void verifySelecteDateIsDisplayed(String date){
        progressHelpers.waitForElementToInvisible(myEnlightenLoadingProgressBar);
        progressHelpers.hardWait(3000);
        clickOnDateDisplayed();
        selectDate(date);
        System.out.println(systemDate.getAttribute("value"));
        Assert.assertEquals(systemDate.getAttribute("value"),date);
    }

    public void selectDate(String date){
        LocalDate selectDate = calendarHelper.getDateFromString(date);
        int month = selectDate.getMonthValue();
        int year = selectDate.getYear();
        int day = selectDate.getDayOfMonth();

        int currentMonth = calendarHelper.getIntegerValueOfMonth(datePickerMonthAndYear.getText());

        for(int expectedMonth=month; expectedMonth<=currentMonth; expectedMonth++){
            if(expectedMonth==currentMonth){
                clickOnDateInDatePicker(String.valueOf(day));
                break;
            }else{
                datePickerPreviousButton.click();
            }
        }
    }

    public void clickOnDateInDatePicker(String date){
        int size = datePickerEnableDates.size();
        for(int i=0;i<=size;i++){
            if(datePickerEnableDates.get(i).getText().equals(date)){
                datePickerEnableDates.get(i).click();
                break;
            }
        }

//        driver.findElement(By.xpath("//td[@class='day' and text()='"+date+"']")).click();
    }

    public void verifyFutureDatesAreBlocked(String siteName){
        progressHelpers.waitForElementToDisplay(datePickerTable);
        List<String> listOfAllDisabledDates = calendarHelper.getAllDisabledDatesFromCalendar(datePickerDisableDates);
        List<String> listOfAllDisabledDatesAsPerSiteDate = calendarHelper.getAllDisabledDatesFromCalendar(siteName);
        Assert.assertEquals(listOfAllDisabledDatesAsPerSiteDate,listOfAllDisabledDates);

    }

    public void clickOnDateDisplayed(){
        progressHelpers.waitForElementToInvisible(myEnlightenLoadingProgressBar);
        progressHelpers.waitForElementToDisplay(systemDate);
        systemDate.click();
    }




}
