package com.enphaseenergy.enlighten.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.Arrays;
import java.util.List;

/**
 * @author mnpalanisamy
 */
public class AdminDevicesPage implements BaseObjects {
    public AdminDevicesPage(WebDriver driver){
        PageFactory.initElements(driver,this);
    }

    /*Page Objects*/
    @FindBy(css = "div#emu tr#emu_t_tr_0 a")
    private List<WebElement> envoyTableLinks;

    @FindBy(id = "emu_t")
    private WebElement envoyTable;

    @FindBy(id = "eim")
    private WebElement enphaseIntegratedMeterTable;

    @FindBy(id = "pcu_devices_datatables")
    private WebElement microinvertersTable;

    @FindBy(id = "empty_acbs")
    private WebElement emptyACBTable;

    @FindBy(id = "empty_encharge")
    private WebElement emptyEnchargeTable;

    @FindBy(id = "empty_enpower")
    private WebElement emptyEnpowerTable;

    @FindBy(id = "cellular_modems")
    private WebElement cellularModemTable;

    @FindBy(id = "zigbee_devices_datatables")
    private WebElement zigbeeDevicesTable;

    @FindBy(id = "table#eim td div:nth-child(2) a")
    private List<WebElement> enphaseIntegratedMeterLinks;

    @FindBy(id = "table#pcu_devices_datatables td div:nth-child(2) a")
    private List<WebElement> microinverterLinks;

    @FindBy(css = "table#emu_t td")
    private List<WebElement> envoyTableDatas;

    @FindBy(css = "table#eim td")
    private List<WebElement> enphaseIntegratedMeterDatas;

    @FindBy(css = "table#pcu_devices_datatables td")
    private List<WebElement> microinvertersTableDatas;


    /*Page Methods*/

    /**
     * @param text
     */
    public void clickOnEnvoyLink(String text){
        for (WebElement item: envoyTableLinks){
            if(item.getText().equalsIgnoreCase(text)){
                item.click();
                break;
            }
        }
    }

    public void verifyAdminDevicesPage(){
        List<WebElement> listOfElements = Arrays.asList(envoyTable,enphaseIntegratedMeterTable,microinvertersTable,emptyACBTable,emptyEnpowerTable,emptyEnchargeTable);
        assertHelper.assertListOfElementsExistsInPage(listOfElements);
        verifyAllDevicesTableData();
    }

    public void verifyAllDevicesTableData(){
        assertHelper.assertListOfElementsIsNotEmpty(envoyTableDatas);
        assertHelper.assertListOfElementsIsNotEmpty(enphaseIntegratedMeterDatas);
        assertHelper.assertListOfElementsIsNotEmpty(microinvertersTableDatas);
    }

}
