package com.enphaseenergy.enlighten.tests;

import org.apache.log4j.Logger;
import org.testng.annotations.Test;
import java.lang.reflect.Method;

import com.enphaseenergy.enlighten.Helpers.CalendarHelper;
import com.enphaseenergy.enlighten.Helpers.LoggerHelper;
import com.enphaseenergy.enlighten.utils.Report.ExtentTestManager;
import com.relevantcodes.extentreports.LogStatus;

public class SupportTest extends BaseTest{

	/**@author kulanjirajanp
	 * @throws Throwable 
	 * 	
	 */

	@Test(groups = {"sanity","prod_sanity","qa4_sanity","qa2_sanity"})
	public void ENL1321_verifyCustomerSupport(Method method) throws Throwable {
		ExtentTestManager.startTest(method.getName(),"verifyCustomerSupport");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId"+ Thread.currentThread().getId());
		getSignInPageObject().loginAs("INSTALLER_AUT_1");
		getDashboardPageObject().clickOnMenu("support");
		getSupportPageObject().verifySupportText();


	}
	@Test(groups = {"sanity","prod_sanity","qa4_sanity","qa2_sanity"})
	public void ENLM1326ENLM1327_verifyValidPhoneSupport(Method method) throws Throwable {
		ExtentTestManager.startTest(method.getName(),"verifyValidPhoneSupport");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId"+ Thread.currentThread().getId());
		getSignInPageObject().loginAs("INSTALLER_AUT_1");
		getDashboardPageObject().clickOnMenu("support");
		getSupportPageObject().verifySendEmail("Test","Testing mail");


	}	
	@Test(groups = {"sanity","prod_sanity","qa4_sanity","qa2_sanity"})
	public void ENLM1328_verifyInValidPhoneSupport(Method method) throws Throwable {
		ExtentTestManager.startTest(method.getName(),"verifyInValidPhoneSupport");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId"+ Thread.currentThread().getId());
		getSignInPageObject().loginAs("INSTALLER_AUT_1");
		getDashboardPageObject().clickOnMenu("support");
		getSupportPageObject().emailsupportErrorMsg();

	}	
	@Test(groups = {"sanity","prod_sanity","qa4_sanity","qa2_sanity"})
	public void ENLM1337_verifySearchSiteName(Method method) throws Throwable {
		ExtentTestManager.startTest(method.getName(),"verifySearchSiteName");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId"+ Thread.currentThread().getId());
		getSignInPageObject().loginAs("INSTALLER_AUT_1");
		getDashboardPageObject().clickOnMenu("support");
		getSupportPageObject().verifySearchSiteName("Test@12");

	}	

	@Test(groups = {"sanity","prod_sanity","qa4_sanity","qa2_sanity"})
	public void ENLM1320ENLM1322_verifySupportLink(Method method) throws Throwable {
		ExtentTestManager.startTest(method.getName(),"verifySupportLink");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId"+ Thread.currentThread().getId());
		getSignInPageObject().loginAs("INSTALLER_AUT_1");
		getDashboardPageObject().clickOnMenu("support");
		getSupportPageObject().verifySupportLink();
		getSupportPageObject().verifyHelplink1();
		getSupportPageObject().verifyHelplink2();
		getSupportPageObject().verifyGettingStartedLink();
		getSupportPageObject().verifyEnstoreLink() ;

	}	
}
