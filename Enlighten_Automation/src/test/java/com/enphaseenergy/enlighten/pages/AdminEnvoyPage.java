package com.enphaseenergy.enlighten.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.util.List;

/**
 * @author mnpalanisamy
 */
public class AdminEnvoyPage implements BaseObjects{
    public AdminEnvoyPage(WebDriver driver){
        PageFactory.initElements(driver,this);
    }

    /*Page Objects*/
    @FindBy(css = "table#emu_reports_datatables tr:nth-child(1) td:nth-child(9)")
    private WebElement latestReportCreatedDate;

    @FindBy(css = "table#emu_reports_datatables tr:nth-child(1) td")
    private List<WebElement> latestReportTableDatas;

    @FindBy(id = "emu_reports_datatables")
    private WebElement reportsTable;

    @FindBy(id = "tasks_table_datatables")
    private WebElement tasksTable;

    @FindBy(css = "table#tasks_table_datatables td")
    private List<WebElement> tasksTableDatas;

    @FindBy(id = "audit_log_entries")
    private WebElement envoyLogTable;

    @FindBy(css = "table#audit_log_entries td")
    private List<WebElement> envoyLogTableDatas;


    /*Page Methods*/
    public void verifyLatestEnvoyReport(){
        progressHelpers.waitForElementToDisplay(reportsTable);
        // verify latest report record is not empty
        for(WebElement item:latestReportTableDatas){
            Assert.assertTrue((item.getText()!=""&&item.getText()!=null));
        }
    }

    public void verifyTasksTableData(){
        progressHelpers.waitForElementToDisplay(tasksTable);
        // verify latest report record is not empty
        for(WebElement item:tasksTableDatas){
            Assert.assertTrue((item.getText()!=""&&item.getText()!=null));
        }
    }

    public void verifyEnvoyLogTableData(){
        progressHelpers.waitForElementToDisplay(envoyLogTable);
        // verify latest report record is not empty
        for(WebElement item:envoyLogTableDatas){
            Assert.assertTrue((item.getText()!=""&&item.getText()!=null));
        }
    }
}
