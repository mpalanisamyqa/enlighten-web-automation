package com.enphaseenergy.enlighten.pages;

import com.enphaseenergy.enlighten.Helpers.LoggerHelper;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.util.*;

/**
 * @author mnpalanisamy
 */
public class EEAdminPage implements BaseObjects {

    public EEAdminPage(WebDriver driver){
        PageFactory.initElements(driver,this);
    }

    private Logger logger = LoggerHelper.getLogger(EEAdminPage.class);

    /*Page Objects*/

    /// Below Objects are for Site Form
    @FindBy(css = "form.edit_site")
    private WebElement eeAdminForm;

    @FindBy(id = "address_form")
    private WebElement eeAdminAddressForm;

    @FindBy(xpath = "//form[@class='edit_site']/descendant-or-self::label[text()='Name']")
    private WebElement siteFormNameLabel;

    @FindBy(id = "site_name")
    private WebElement siteNameInForm;

    @FindBy(xpath = "//form[@class='edit_site']/descendant-or-self::label[text()='First Interval End Date']")
    private WebElement siteFormFirstIntervalEndDateLabel;

    @FindBy(xpath = "//form[@class='edit_site']/descendant-or-self::label[text()='First Interval End Date']/following-sibling::br")
    private WebElement siteFormFirstIntervalEndDateValue;

    @FindBy(xpath = "//form[@class='edit_site']/descendant-or-self::label[text()='Most recent Envoy/EMU Report']")
    private WebElement siteFormMostRecentEnvoyReportLabel;

    @FindBy(xpath = "//form[@class='edit_site']/descendant-or-self::label[text()='Most recent Envoy/EMU Report']/following-sibling::br")
    private WebElement siteFormMostRecentEnvoyReportValue;

    @FindBy(xpath = "//form[@class='edit_site']/descendant-or-self::label[text()='Data caught up to']")
    private WebElement siteFormDataCaughtUptoLabel;

    @FindBy(xpath = "//form[@class='edit_site']/descendant-or-self::label[text()='Data caught up to']/following-sibling::br")
    private WebElement siteFormDataCaughtUptoValue;

    @FindBy(xpath = "//form[@class='edit_site']/descendant-or-self::label[text()='System Installer ']")
    private WebElement siteFormSystemInstallerLabel;

    @FindBy(id = "installer_company_display")
    private WebElement siteFormSystemInstallerValue;

    @FindBy(xpath = "//form[@class='edit_site']/descendant-or-self::b[text()='Disallow RMA']")
    private WebElement siteFormDisallowRMALabel;

    @FindBy(id = "site_disallow_rma")
    private WebElement disallowRMACheckbox;

    @FindBy(xpath = "//form[@class='edit_site']/descendant-or-self::b[text()='Disallow EMU Upgrades']")
    private WebElement siteFormDisallowEMUUpgradesLabel;

    @FindBy(id = "site_disallow_emu_upgrades")
    private WebElement disallowEMUUpgradeCheckbox;

    @FindBy(xpath = "//form[@class='edit_site']/descendant-or-self::b[text()='Disallow PCU Upgrades']")
    private WebElement siteFormDisallowPCUUpgradesLabel;

    @FindBy(id = "site_disallow_pcu_upgrades")
    private WebElement disallowPCUUpgradeCheckbox;

    @FindBy(xpath = "//form[@class='edit_site']/descendant-or-self::b[text()='Disallow Grid Profiles']")
    private WebElement siteFormDisallowGridProfilesLabel;

    @FindBy(id = "site_disallow_tpm_profiles")
    private WebElement disallowGridProfileCheckbox;

    @FindBy(xpath = "//form[@class='edit_site']/descendant-or-self::b[text()='Reason(s):']")
    private WebElement siteFormreasonsLabel;

    @FindBy(id = "site_disallow_reason_ids")
    private WebElement siteFormDisallowReasons;

    @FindBy(xpath = "//form[@class='edit_site']/descendant-or-self::b[text()='Archive EMU Reports']")
    private WebElement siteFormArchiveEMUReportsLabel;

    @FindBy(id = "site_archive_reports")
    private WebElement archiveEMUReportsCheckbox;

    @FindBy(xpath = "//form[@class='edit_site']/descendant-or-self::label[text()='Activation']")
    private WebElement siteFormActivationLabel;

    @FindBy(xpath = "//form[@class='edit_site']/descendant-or-self::label[text()='Installation Type']")
    private WebElement siteFormInstallationTypeLabel;

    @FindBy(id = "site_site_type")
    private WebElement siteFormInstallationTypeDropDown;

    @FindBy(xpath = "//form[@class='edit_site']/descendant-or-self::b[text()='Allow public access?:']")
    private WebElement siteFormAllowPublicAccessLabel;

    @FindBy(xpath = "//form[@class='edit_site']/descendant-or-self::b[text()='Public Access URL:']")
    private WebElement siteFormPublicAccessURLLabel;

    @FindBy(css = "form.edit_site input[name=commit][value=Save]")
    private WebElement siteFormSaveButton;

    @FindBy(id = "reset_btn")
    private WebElement siteFormResetButton;

    ///Below Objects are for Site Address Form
    @FindBy(xpath = "//h2[text()='Site Address']")
    private WebElement siteAddressFormTitleLabel;

    @FindBy(xpath = "//form[@id='address_form']/descendant-or-self::label[text()='Country']")
    private WebElement siteAddressFormCountryLabel;

    @FindBy(id = "address_country")
    private WebElement siteAddressFormCountryDropdown;

    @FindBy(xpath = "//form[@id='address_form']/descendant-or-self::label[text()='Street Address']")
    private WebElement siteAddressFormStreetAddressLabel;

    @FindBy(id = "address_address1")
    private WebElement siteAddressFormStreetAddressTextField;

    @FindBy(xpath = "//form[@id='address_form']/descendant-or-self::label[text()='Street Address 2']")
    private WebElement siteAddressFormStreetAddress2Label;

    @FindBy(id = "address_address2")
    private WebElement siteAddressFormStreetAddress2TextField;

    @FindBy(xpath = "//form[@id='address_form']/descendant-or-self::label[text()='City']")
    private WebElement siteAddressFormCityLabel;

    @FindBy(id = "address_city")
    private WebElement siteAddressFormCityTextField;

    @FindBy(xpath = "//form[@id='address_form']/descendant-or-self::label[text()='State/Province']")
    private WebElement siteAddressFormStateLabel;

    @FindBy(id = "address_state")
    private WebElement siteAddressFormStateDropdown;

    @FindBy(xpath = "//form[@id='address_form']/descendant-or-self::label[text()='Zip/Postal Code']")
    private WebElement siteAddressFormZipcodeLabel;

    @FindBy(id = "address_zip")
    private WebElement siteAddressFormZipcodeTextField;

    @FindBy(xpath = "//form[@id='address_form']/descendant-or-self::td[text()='Override Geocode?']")
    private WebElement siteAddressFormOverrideGeocodeLabel;

    @FindBy(id = "address_override_geocode")
    private WebElement siteAddressFormOverrideGeocodeCheckbox;

    @FindBy(xpath = "//form[@id='address_form']/descendant-or-self::img[@alt='Help small']")
    private WebElement siteAddressFormHelpIcon;

    @FindBy(xpath = "//form[@id='address_form']/descendant-or-self::label[text()='Latitude']")
    private WebElement siteAddressFormLatitudeLabel;

    @FindBy(id = "address_latitude")
    private WebElement siteAddressFormLatitudeTextField;

    @FindBy(xpath = "//form[@id='address_form']/descendant-or-self::label[text()='Longitude']")
    private WebElement siteAddressFormLongitudeLabel;

    @FindBy(id = "address_longitude")
    private WebElement siteAddressFormLongitudeTextField;

    @FindBy(xpath = "//form[@id='address_form']/descendant-or-self::label[text()='Timezone']")
    private WebElement siteAddressFormTimezoneLabel;

    @FindBy(id = "address_timezone")
    private WebElement siteAddressFormTimezoneDropdown;

    @FindBy(xpath = "//form[@id='address_form']/descendant-or-self::a[text()='Show on Google Maps']")
    private WebElement siteAddressFormShowOnGoogleMapsLink;

    @FindBy(css = "form#address_form input[name=commit][value=Save]")
    private WebElement siteAddressFormSaveButton;

    @FindBy(xpath = "//div[@id='page_head']/following-sibling::a")
    private List<WebElement> systemEEAdminMenuItems;


    /*Page Constants*/

    private static final ArrayList<String> disallowReasons = new ArrayList<String>();
    private static final List<String> SYSTEMEEADMINMENUITEMS = Arrays.asList("Enlighten Manager","MyEnlighten","Enlighten Mobile","Admin","Envoy/EMU Reports","Access","Settings","Events","Tasks","Command","Devices","Analytics","Site Rollup");


    /**
     * @param menuItems
     */
    /*Page Methods*/
    public void verifyEEAdminMenuItems(String menuItems[]){
        Set<String> acutalMenuItems = new HashSet<>(SYSTEMEEADMINMENUITEMS);
        acutalMenuItems.addAll(SYSTEMEEADMINMENUITEMS);
        Set<String> expectedMenuItems = new HashSet<>();
        for(WebElement menu:systemEEAdminMenuItems){
            expectedMenuItems.add(menu.getText());
        }
        Assert.assertEquals(acutalMenuItems,expectedMenuItems);
    }

    /**
     * @param sheetName
     * @param rowIndex
     */
    public void verifyEEAdminPageElements(String sheetName, String rowIndex){
        progressHelpers.waitForElementToDisplay(siteAddressFormTitleLabel);
        progressHelpers.waitForElementToDisplay(eeAdminAddressForm);
        ///Verify the site form datas
        Assert.assertTrue(siteFormNameLabel.isDisplayed());
        verifySystemName(testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"System Name"));
        Assert.assertTrue(siteFormFirstIntervalEndDateLabel.isDisplayed());
        Assert.assertTrue(siteFormMostRecentEnvoyReportLabel.isDisplayed());
        Assert.assertTrue(siteFormDataCaughtUptoLabel.isDisplayed());
        Assert.assertTrue(siteFormSystemInstallerLabel.isDisplayed());
        verifySystemInstaller(testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Installer Name"));
        Assert.assertTrue(siteFormDisallowRMALabel.isDisplayed());
        Assert.assertTrue(siteFormDisallowEMUUpgradesLabel.isDisplayed());
        Assert.assertTrue(siteFormDisallowPCUUpgradesLabel.isDisplayed());
        Assert.assertTrue(siteFormDisallowGridProfilesLabel.isDisplayed());
        Assert.assertTrue(siteFormreasonsLabel.isDisplayed());
        Assert.assertTrue(siteFormArchiveEMUReportsLabel.isDisplayed());
        Assert.assertTrue(siteFormActivationLabel.isDisplayed());
        Assert.assertTrue(siteFormInstallationTypeLabel.isDisplayed());
        Assert.assertEquals(dropDownHelper.getSelectedValue(siteFormInstallationTypeDropDown),testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Installation Type"));
        Assert.assertTrue(siteFormAllowPublicAccessLabel.isDisplayed());
        Assert.assertTrue(siteFormPublicAccessURLLabel.isDisplayed());
        Assert.assertTrue(siteFormSaveButton.isDisplayed());
        Assert.assertTrue(siteFormResetButton.isDisplayed());

        ///Verify the address form datas
        Assert.assertTrue(siteAddressFormTitleLabel.isDisplayed());
        Assert.assertTrue(siteAddressFormCountryLabel.isDisplayed());
        Assert.assertEquals(dropDownHelper.getSelectedValue(siteAddressFormCountryDropdown),testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Country"));
        Assert.assertTrue(siteAddressFormStreetAddressLabel.isDisplayed());
        Assert.assertEquals(siteAddressFormStreetAddressTextField.getAttribute("value"),testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Street Address"));
        Assert.assertTrue(siteAddressFormStreetAddress2Label.isDisplayed());
        Assert.assertTrue(siteAddressFormCityLabel.isDisplayed());
        Assert.assertEquals(siteAddressFormCityTextField.getAttribute("value"),testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"City"));
        Assert.assertTrue(siteAddressFormStateLabel.isDisplayed());
        Assert.assertEquals(dropDownHelper.getSelectedValue(siteAddressFormStateDropdown),testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"State"));
        Assert.assertTrue(siteAddressFormZipcodeLabel.isDisplayed());
        Assert.assertEquals(siteAddressFormZipcodeTextField.getAttribute("value"),testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Zipcode"));
        Assert.assertTrue(siteAddressFormOverrideGeocodeLabel.isDisplayed());
        Assert.assertTrue(siteAddressFormLatitudeLabel.isDisplayed());
        Assert.assertEquals(siteAddressFormLatitudeTextField.getAttribute("value"),testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Latitude"));
        Assert.assertTrue(siteAddressFormLongitudeLabel.isDisplayed());
        Assert.assertEquals(siteAddressFormLongitudeTextField.getAttribute("value"),testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Longitude"));
        Assert.assertTrue(siteAddressFormTimezoneLabel.isDisplayed());
        Assert.assertEquals(dropDownHelper.getSelectedValue(siteAddressFormTimezoneDropdown),testDataHelper.getDataFromDataSheet(sheetName,rowIndex,"Timezone"));
        Assert.assertTrue(siteAddressFormShowOnGoogleMapsLink.isDisplayed());
        Assert.assertTrue(siteAddressFormSaveButton.isDisplayed());

    }

    /**
     * @param name
     */
    public void verifySystemName(String name){
        Assert.assertEquals(siteNameInForm.getAttribute("value"),name);
    }

    /**
     * @param installerName
     */
    public void verifySystemInstaller(String installerName){
        Assert.assertEquals(siteFormSystemInstallerValue.getAttribute("value"),installerName);
    }
}
