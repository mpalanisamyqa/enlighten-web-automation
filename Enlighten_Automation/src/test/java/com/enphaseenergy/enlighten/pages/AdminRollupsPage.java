package com.enphaseenergy.enlighten.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.time.LocalDate;
import java.util.List;

/**
 * @author mnpalanisamy
 */
public class AdminRollupsPage implements BaseObjects {
    public AdminRollupsPage(WebDriver driver){
        PageFactory.initElements(driver,this);
    }

    /*Page Objects*/
    @FindBy(css = "div#data_table tr:nth-child(3) td")
    private List<WebElement> rollupTableFirstRowDatas;

    @FindBy(css = "div#data_table tr:nth-child(3) td:nth-child(1)")
    private WebElement latestStartDate;



    /*Page Constants*/

    /*Page Methods*/
    public void verifyYesterdayRollupData(){
        String yesterdayDate = latestStartDate.getText().split(" ")[0];
        String rollupDatePattern = "yyyy/MM/dd";
        LocalDate actualDate = calendarHelper.getDateFromString(yesterdayDate,rollupDatePattern);
        LocalDate expectedDate = calendarHelper.getTodayDate(rollupDatePattern);
        Assert.assertTrue(expectedDate.isAfter(actualDate));
        //Also verify all the table data are not empty
        for (WebElement item:rollupTableFirstRowDatas){
            Assert.assertTrue(!(item.getText().isEmpty()));
        }

    }

}
