package com.enphaseenergy.enlighten.tests;

import java.lang.reflect.Method;

import org.testng.annotations.Test;
import com.enphaseenergy.enlighten.utils.Report.ExtentTestManager;
import com.relevantcodes.extentreports.LogStatus;


public class OwnershipTest extends BaseTest{
	@Test(priority = 1)
	public void ENLM2159_verifyFetchOwnershipUsingEnvoySerialNo(Method method) throws Throwable{
		ExtentTestManager.startTest(method.getName(),"verifyFetchOwnerShipUsingEnvoySerialNo");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId"+ Thread.currentThread().getId());
		getSignInPageObject().verifySignInPage();
		getSignInPageObject().loginAs("ADMIN");
		getOwnershipPagePageObject().clickOnSupportTab();
		getOwnershipPagePageObject().clickOnOwnershipTransferButton();
		getOwnershipPagePageObject().verifyFetchOwnerDetailAllText();
		getOwnershipPagePageObject().FetchOwnerShipDetailUsingEnvoySerialNo("site");

	}
 
	@Test(priority = 2)
	public void ENLM2159_verifyFetchOwnershipUsingEnlightenSystemId(Method method) 
	{
		ExtentTestManager.startTest(method.getName(),"verifyFetchOwnerShipUsingEnlightenSystemId");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId"+ Thread.currentThread().getId());
		getSignInPageObject().verifySignInPage();
		getSignInPageObject().loginAs("ADMIN");
		getOwnershipPagePageObject().clickOnSupportTab();
		getOwnershipPagePageObject().clickOnOwnershipTransferButton(); 
		getOwnershipPagePageObject().verifyFetchOwnerDetailAllText();
		getOwnershipPagePageObject().FetchOwnershipDetailUsingEnlightenSystemId("site");

	}

	@Test(priority = 3)
	public void ENLM2159_verifyInvalidOwnershipDetail(Method method) {

		ExtentTestManager.startTest(method.getName(),"verifyInvalidOwnerShipDetai");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId"+ Thread.currentThread().getId());
		getSignInPageObject().verifySignInPage();
		getSignInPageObject().loginAs("ADMIN");
		getOwnershipPagePageObject().clickOnSupportTab();
		getOwnershipPagePageObject().clickOnOwnershipTransferButton();         
		getOwnershipPagePageObject().VerifyErrorMsgFetchOwnerDetail();

	}
	  
	@Test(priority = 4)
	public void ENLM2159_verifyValidPreviousOwnerDetail(Method method) throws Throwable  
	{	 
		ExtentTestManager.startTest(method.getName(),"verifyValidPreviousOwnerDetail");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId"+ Thread.currentThread().getId());
		getSignInPageObject().verifySignInPage();
		getSignInPageObject().loginAs("ADMIN");
		getOwnershipPagePageObject().clickOnSupportTab();
		getOwnershipPagePageObject().clickOnOwnershipTransferButton();      
		getOwnershipPagePageObject().FetchOwnershipDetailUsingEnlightenSystemId("site");
		getOwnershipPagePageObject().verifyValidPreviousOwnerDetail();        
	}
	
	 
	@Test(priority = 5)
	public void ENLM2159_verifyInValidPreviousOwnerDetail(Method method) throws Throwable 

	{
		ExtentTestManager.startTest(method.getName(),"verifyInValidPreviousOwnerDetail");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId"+ Thread.currentThread().getId());
		getSignInPageObject().verifySignInPage();
		getSignInPageObject().loginAs("ADMIN");
		getOwnershipPagePageObject().clickOnSupportTab();
		getOwnershipPagePageObject().clickOnOwnershipTransferButton(); 
		getOwnershipPagePageObject().FetchOwnershipDetailUsingEnlightenSystemId("site");
		getOwnershipPagePageObject().verifyInValidPreviousOwnerDetail();

	} 
	 
	@Test(priority = 6)
	public void ENLM2159_verifyValidAddressInformation(Method method) throws Throwable 
	{
		ExtentTestManager.startTest(method.getName(),"verifyValidAddressInformation");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId"+ Thread.currentThread().getId());
		getSignInPageObject().verifySignInPage();
		getSignInPageObject().loginAs("ADMIN");
		getOwnershipPagePageObject().clickOnSupportTab();
		getOwnershipPagePageObject().clickOnOwnershipTransferButton(); 
		getOwnershipPagePageObject().FetchOwnershipDetailUsingEnlightenSystemId("site");
		getOwnershipPagePageObject().verifyValidPreviousOwnerDetail();
		getOwnershipPagePageObject().verifyValidDefaultAddressInformation();
	} 
	 
    @Test(priority = 7)
	public void ENLM2159_verifyInValidAddressInformation(Method method) throws Throwable 
	{
		ExtentTestManager.startTest(method.getName(),"verifyInValidAddressInformation");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId"+ Thread.currentThread().getId());
		getSignInPageObject().verifySignInPage();
		getSignInPageObject().loginAs("ADMIN");
		getOwnershipPagePageObject().clickOnSupportTab();
		getOwnershipPagePageObject().clickOnOwnershipTransferButton(); 
		getOwnershipPagePageObject().FetchOwnershipDetailUsingEnlightenSystemId("site");
		getOwnershipPagePageObject().verifyValidPreviousOwnerDetail();
		getOwnershipPagePageObject().verifyInValidAddressInformation();
	} 

	
	@Test(priority = 8)
	public void ENLM2159_verifyInValidPaymentDetail(Method method) throws Throwable 

	{
		ExtentTestManager.startTest(method.getName(),"verifyInValidPaymentDetail");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId"+ Thread.currentThread().getId());
		getSignInPageObject().verifySignInPage();
		getSignInPageObject().loginAs("ADMIN");
		getOwnershipPagePageObject().clickOnSupportTab();
		getOwnershipPagePageObject().clickOnOwnershipTransferButton(); 
		getOwnershipPagePageObject().FetchOwnershipDetailUsingEnlightenSystemId("site");
		getOwnershipPagePageObject().verifyValidPreviousOwnerDetail();
		getOwnershipPagePageObject().verifyValidDefaultAddressInformation();
		getOwnershipPagePageObject().verifyInValidPaymentDetail();
	} 
	
	
	@Test(priority = 9)
		public void ENLM2159_verifyValidPaymentDetailUsingSystemId(Method method) throws Throwable 
		{
			ExtentTestManager.startTest(method.getName(),"verifyValidPaymentDetailUsingSystemId");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId"+ Thread.currentThread().getId());
			getSignInPageObject().verifySignInPage();
			getSignInPageObject().loginAs("ADMIN");
			getOwnershipPagePageObject().clickOnSupportTab();
			getOwnershipPagePageObject().clickOnOwnershipTransferButton(); 
			getOwnershipPagePageObject().FetchOwnershipDetailUsingEnlightenSystemId("site");
			getOwnershipPagePageObject().verifyValidPreviousOwnerDetail();
			getOwnershipPagePageObject().verifyValidDefaultAddressInformation();
			getOwnershipPagePageObject().verifyValidPaymentDetail();

		} 
	
	//@Test
	public void ENLM2159_verifyValidPaymentDetailUsingSerialNo(Method method) throws Throwable 
	{
		ExtentTestManager.startTest(method.getName(),"verifyValidPaymentDetailUsingSerialNo");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId"+ Thread.currentThread().getId());
		getSignInPageObject().verifySignInPage();
		getSignInPageObject().loginAs("ADMIN");
		getOwnershipPagePageObject().clickOnSupportTab();
		getOwnershipPagePageObject().clickOnOwnershipTransferButton(); 
		getOwnershipPagePageObject().FetchOwnerShipDetailUsingEnvoySerialNo("site");
		getOwnershipPagePageObject().verifyValidPreviousOwnerDetail();
		getOwnershipPagePageObject().verifyValidDefaultAddressInformation();
		getOwnershipPagePageObject().verifyValidPaymentDetail();
	} 

	@Test(priority = 10)
	public void ENLM2159_verifyAlreadyDonePaymentUsingSystemId(Method method) throws Throwable 
	{
		ExtentTestManager.startTest(method.getName(),"verifyAlreadyDonePaymentUsingSystemId");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Log from threadId"+ Thread.currentThread().getId());
		getSignInPageObject().verifySignInPage();
		getSignInPageObject().loginAs("ADMIN");
		getOwnershipPagePageObject().clickOnSupportTab();
		getOwnershipPagePageObject().clickOnOwnershipTransferButton(); 
		getOwnershipPagePageObject().verifyAlreadyDonePayment("site");
		
	} 
	
}
