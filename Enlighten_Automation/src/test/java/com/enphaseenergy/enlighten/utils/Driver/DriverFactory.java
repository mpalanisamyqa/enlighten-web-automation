package com.enphaseenergy.enlighten.utils.Driver;

import com.enphaseenergy.enlighten.enums.DriverType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

import java.io.File;

public class DriverFactory {
    private static WebDriver driver;

    private static final String FIREFOX_DRIVER_PROPERTY = "webdriver.gecko.driver";
    private static final String CHROME_DRIVER_PROPERTY = "webdriver.chrome.driver";

    public static WebDriver configDriver(String driverName){
        switch (DriverType.valueOf(driverName.toUpperCase())){
            case FIREFOX:
                System.setProperty(FIREFOX_DRIVER_PROPERTY,System.getProperty("user.dir") + File.separator +"/drivers/geckodriver_V_0_24_0");
                driver = new FirefoxDriver();
                break;
            case CHROME:
                System.setProperty(CHROME_DRIVER_PROPERTY,System.getProperty("user.dir") + File.separator +"drivers/chromedriver_V_74_0");
                driver = new ChromeDriver();
                break;
            case SAFARI:
//                To be added
                break;
            case IE:
//                To be added
                break;
            case HEADLESS:
                FirefoxBinary firefoxBinary = new FirefoxBinary();
                firefoxBinary.addCommandLineOptions("--headless");
                System.setProperty(FIREFOX_DRIVER_PROPERTY,System.getProperty("user.dir") + File.separator +"/drivers/geckodriver_V_0_24_0");
                FirefoxOptions firefoxOptions = new FirefoxOptions();
                firefoxOptions.setBinary(firefoxBinary);
                driver = new FirefoxDriver(firefoxOptions);
                break;
            default:
                System.setProperty(FIREFOX_DRIVER_PROPERTY,System.getProperty("user.dir") + File.separator +"/drivers/geckodriver_V_0_24_0");
                driver = new FirefoxDriver();
                break;
        }
        driver.manage().window().maximize();
        return driver;
    }

}
